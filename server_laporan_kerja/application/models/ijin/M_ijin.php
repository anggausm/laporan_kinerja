<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_ijin extends CI_Model
{
//construktor
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }
    
    public function get_data()
    {
        // $data = json_decode(file_get_contents("php://input"));
        // $token = $this->tkn;
        // $user_key = $token['user_key'];

        $sql = "SELECT a.*, b.nama_jenis_ijin, c.nama_doskar
                FROM absensi_ijin a
                    JOIN absensi_ijin_jenis b ON a.jenis_ijin = b.id_jenis_ijin
                    JOIN doskar_usm c ON a.user_key = c.user_key
                WHERE a.status = ? 
                ORDER BY a.id DESC";
        $query = $this->db->query($sql, 'Menunggu');

        if ($query->num_rows() > 0) 
        {
            $result = $query->result_array();
            $query->free_result();
            $result;
        } 

        $rs = array('rs' => $result);
        return $this->app->respons_data($rs, 'Data berhasil diload', 200);
    }

    public function doskar_usm($data = array())
    {
        $sql = "SELECT user_key, nama_doskar FROM doskar_usm ORDER BY nama_doskar";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function jenis_ijin($data = array())
    {
        $sql = "SELECT id_jenis_ijin, nama_jenis_ijin FROM absensi_ijin_jenis WHERE status = ?";
        $query = $this->db->query($sql, 'Aktif');
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function insert()
    {
        $dataqu     = file_get_contents("php://input");
        $data       = json_decode($dataqu, true);
        $token      = $this->tkn;
        $user_key   = $token['user_key'];

        // return print_r($data);
        // $this->form_validation->set_data($data);
        // $this->form_validation->set_rules('user_key', 'Dosen / Karyawan', 'trim|required');
        // $this->form_validation->set_rules('tanggal_awal', 'Tanggal Mulai', 'trim|required');
        // $this->form_validation->set_rules('tanggal_akhir', 'Tanggal Selesai', '');
        // $this->form_validation->set_rules('jenis_ijin', 'Jenis Ijin', 'trim|required');
        // $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');
        // $this->form_validation->set_rules('status', 'Status Ijin', 'trim|required');
        // $this->form_validation->set_error_delimiters('', '');

        // if ($this->form_validation->run() == false) {
        //     return $this->app->respons_data(false, validation_errors(), 200);
        // }

        // $struktural_sub = $this->db->get_where('tes_struktural_sub', 
        //                     array('id_sub_struktural' => $data['id_sub_struktural']))->row_array();
        // $datas                       = array();
        // $datas['user_key']           = $data['user_key'];
        // $datas['tanggal_awal']       = $data['tanggal_mulai'];
        // $datas['tanggal_selesai']    = $data['tanggal_selesai'];
        // $datas['jenis_ijin']         = $data['jenis_ijin'];
        // $datas['keterangan']         = $data['keterangan'];
        // $datas['status']             = $data['status'];
        // $datas['c']                  = $user_key;

        $this->db->insert('absensi_ijin', $data);

        if ($this->db->affected_rows() > 0) 
        {
            $this->log_app->log_data($user_key, 'absensi_ijin', 'user_key', $data['user_key'], 'C', $dataqu, ' ', 'log_absensi_ijin');
            return $this->app->respons_data(true, 'Data berhasil disimpan', 200);
        } 
        else 
        {
            return $this->app->respons_data(false, 'Data gagal disimpan', 200);
        }
    }

    public function edit() {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT id, user_key, tanggal_awal, tanggal_akhir, jenis_ijin, keterangan, status
                FROM absensi_ijin
                WHERE id = ?";
        $query = $this->db->query($sql, $data['id']);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            $result;
        }
        $rs = array('rs' => $result);
        return $this->app->respons_data($rs, 'Data berhasil diload', 200);
    }

    public function update()
    {
        $dataqu     = file_get_contents("php://input");
        $data       = json_decode($dataqu, true);
        $token      = $this->tkn;
        $user_key   = $token['user_key'];

        // $this->form_validation->set_data($data);
        // $this->form_validation->set_rules('user_key', 'Dosen / Karyawan', 'trim|required');
        // $this->form_validation->set_rules('tanggal_awal', 'Tanggal Mulai', 'trim|required');
        // $this->form_validation->set_rules('tanggal_akhir', 'Tanggal Selesai', 'trim|required');
        // $this->form_validation->set_rules('jenis_ijin', 'Jenis Ijin', 'trim|required');
        // $this->form_validation->set_rules('keterangan', 'Keterangan', '');
        // $this->form_validation->set_rules('status', 'Status', 'trim|required');
        // $this->form_validation->set_error_delimiters('', '');

        // if ($this->form_validation->run() == false) {
        //     return $this->app->respons_data(false, validation_errors(), 200);
        // }

        $datas['user_key']      = $data['user_key'];
        $datas['tanggal_awal']  = $data['tanggal_awal'];
        $datas['tanggal_akhir'] = $data['tanggal_akhir'];
        $datas['jenis_ijin']    = $data['jenis_ijin'];
        $datas['keterangan']    = $data['keterangan'];
        $datas['status']        = $data['status'];

        $this->db->where('id', $data['id']);
        $this->db->update('absensi_ijin', $datas);
        if ($this->db->affected_rows() >= 0) {

            if ($data['status'] == 'Acc') 
            {
                $this->buat_absen($datas);
            }

            $this->log_app->log_data($user_key, 'absensi_ijin', 'user_key', $data['user_key'], 'U', $dataqu, ' ', 'log_absensi_ijin');
            return $this->app->respons_data(true, 'Data berhasil diupdate', 200);
        } else {
            return $this->app->respons_data(false, 'Data gagal diupdate', 200);
        }
    }

    public function buat_absen($data)
    {
        $jam = $this->get_jam($data['user_key'], $data['jenis_ijin']);
\
        if (strtotime($data['tanggal_awal']) == strtotime($data['tanggal_akhir'])) 
        {
            $cek_absen = $this->cek_absen($data);
            if ($cek_absen > 0) 
            {
                # update\
                if ($data['jenis_ijin'] = '1') 
                {
                    
                }
                elseif ($data['jenis_ijin'] = '2') 
                {
                    $jam_masuk  = '';
                    $jam_pulang = $jam['jam_pulang'];
                }
                else
                {
                    $data['jam_masuk']  = $jam['jam_masuk'];
                    $data['jam_pulang'] = $jam['jam_pulang'];
                }
            }
            else
            {
                # insert
            }
        }
        elseif (strtotime($data['tanggal_awal']) <= strtotime($data['tanggal_akhir'])) 
        {
            while ( strtotime($data['tanggal_awal']) <= strtotime($data['tanggal_akhir']))
            {
                $cek_absen = $this->cek_absen($data);
                if ($cek_absen > 0) 
                {
                    # update
                }
                else
                {
                    # insert
                }
            }
        }
    }

    public function cek_insert($tanggal = '', $user_key = '')
    {
        $sql    = "SELECT id_absen FROM wfh_absensi WHERE tanggal = ? and user_key  = ? ";
        $query  = $this->db->query($sql, array($tanggal, $user_key));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_jam($user_key = '', $jenis_ijin = '')
    {
        $shitf  = $this->get_doskar($user_key);
        $sql    = "SELECT jam_masuk, jam_pulang, toleransi_keterlambatan 
                    FROM absesni_setingan_jam 
                    WHERE status = ? AND shift = ?";
        $query  = $this->db->query($sql, array('1', $shitf['shift_finjer']));
        $result = $query->row_array();
        $query->free_result();

        return $result;
    }

    public function get_doskar($user_key = '')
    {
        $sql = "SELECT user_key, shift_finjer FROM doskar_usm WHERE user_key = '$user_key'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function insert_absen($data)
    {

    }

    public function update_absen($data)
    {

    }
}