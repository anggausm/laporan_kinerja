<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_user extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }
    ///ACT INsert
    public function act_insert()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $user_route = $data->user_route;
        $level_roure = $data->level_roure;

        $c = $token['user_key'];
        if ($this->cek_insert($user_route->user_key, $user_route->id_aplikasi)) {
            return print '0';
        }
        if (empty($level_roure->level_key)) {
            return print '0';
        }

        if ($user_route->id_aplikasi != $token['id_app']) {
            return print '0';
        }

        if ($this->insert_user_route($user_route->id_grub, $user_route->user_key, $user_route->id_aplikasi, $user_route->kode_khusus, $c)) {
            if ($this->insert_level_route($level_roure->id_aplikasi, $level_roure->level_key, $level_roure->user_key, $user_route->kode_khusus, $c)) {
                //insert Log

                $this->log_app->log_data($token['user_key'], 'sso_level_route', 'id_aplikasi, level_key, user_key', $user_route->id_aplikasi . ', ' . $level_roure->level_key . ', ' . $data->user_key, 'C', file_get_contents("php://input"), 'Miroring SSo', 'log_sso');
                return print '1';
            }
        } else {
            return print '0';

        }
    }

    public function cek_insert($user_key = '', $id_aplikasi = '')
    {

        $sql = "SELECT id_aplikasi FROM `sso_user_router` WHERE user_key ='$user_key' and id_aplikasi = '$id_aplikasi'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    // insert into sso_user_router
    public function insert_user_route($id_grub = '', $user_key = '', $id_aplikasi = '', $kode_khusus = '', $c = '')
    {
        $sql = "INSERT INTO  `sso_user_router` (id_grub, user_key, id_aplikasi, kode_khusus, c ) VALUE (?, ?, ?, ?, ?)";
        if ($this->db->query($sql, array($id_grub, $user_key, $id_aplikasi, $kode_khusus, $c))) {
            return true;
        } else {
            return false;
        }
    }

    //insert sso_level_route
    public function insert_level_route($id_aplikasi = '', $level_key = '', $user_key = '', $kode_khusus = '', $c = '')
    {
        $sql = "INSERT INTO sso_level_route (id_aplikasi, level_key, user_key, kode_khusus , c) VALUE (?, ?, ?, ?, ?) ";
        if ($this->db->query($sql, array($id_aplikasi, $level_key, $user_key, $kode_khusus, $c))) {
            return true;
        } else {
            return false;
        }
    }

    //delete data
    public function act_delete()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;

        $user_route = $data->delete_user_route;
        $level_roure = $data->delete_level_route;
        $user_key = $user_route->user_key;

        if ($user_route->id_aplikasi != $token['id_app']) {
            return print '1';
        }

        if ($this->delete_user_route($user_key, $user_route->id_aplikasi)) {
            if ($this->delete_level_route($user_route->id_aplikasi, $user_key)) {

                $this->log_app->log_data($token['user_key'], 'sso_level_route', 'id_aplikasi, level_key, user_key', $user_route->id_aplikasi . ', ' . $user_route->level_key . ', ' . $user_route->user_key, 'D', file_get_contents("php://input"), 'Miroring SSO', 'log_sso');

                return print '1';

            }
        } else {
            return print '0';
        }
    }

    // delete into sso_user_router

    public function delete_user_route($user_key = '', $id_aplikasi = '')
    {
        $sql = "DELETE FROM  `sso_user_router` WHERE user_key =  ? AND id_aplikasi = ?";
        if ($this->db->query($sql, array($user_key, $id_aplikasi))) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_level_route($id_aplikasi = '', $user_key = '')
    {
        $sql = "DELETE FROM sso_level_route WHERE id_aplikasi = ? AND user_key = ? ";
        if ($this->db->query($sql, array($id_aplikasi, $user_key))) {
            return true;
        } else {
            return false;
        }
    }

}
