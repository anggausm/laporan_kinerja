<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_menu extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }
    public function cek_insert($tabel = '', $id_aplikasi = '', $nm_menu = '')
    {

        $sql = "SELECT id_menu FROM $tabel WHERE id_aplikasi ='$id_aplikasi' and nm_menu ='$nm_menu'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function insert_data()
    {
        $token = $this->tkn;
        $data = json_decode(file_get_contents("php://input"));

        if ($this->id_app != $token['id_aplikasi']) {
            return print '0';
        }
        if ($token['user_key'] != $data->c) {
            return print '0';
        }

        if ($this->cek_insert('sso_menu', $data->id_aplikasi, $data->nm_menu)) {
            return print '0';
        }

        $data_insert = array(
            'id_menu' => $data->id_menu,
            'id_aplikasi' => $data->id_aplikasi,
            'icon' => str_replace('**', ' ', $data->icon),
            'nm_menu' => str_replace('**', ' ', $data->nm_menu),
            'url' => $data->url,
            'id_grub_user' => $data->id_grub_user,
            'menu_key' => $data->menu_key,
            'order' => $data->order,
            'st' => $data->st,
            'c' => $data->c,
        );
        if ($this->db->insert('sso_menu', $data_insert)) {
            $this->log_app->log_data($token['user_key'], 'sso_menu', 'menu_key', $data->menu_key, 'C', json_encode($data), 'Miroring SSO', 'log_sso');
            return print '1';
        } else {
            return print '0';
        }
    }

    public function update_data($value = '')
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        if ($this->id_app != $token['id_aplikasi']) {
            return print '0';
        }

        if ($token['user_key'] != $data->c) {
            return print '0';
        }

        //id_menu, id_aplikasi, icon, nm_menu, url, menu_key, `order`, st ,id_aplikasi
        $data_update = array(
            'icon' => str_replace('**', ' ', $data->icon),
            'nm_menu' => str_replace('**', ' ', $data->nm_menu),
            'url' => $data->url,
            'order' => $data->order,
            'st' => $data->st,
            'id_grub_user' => $data->id_grub_user,
            'u' => $data->u,
        );
        $where = array('menu_key' => $data->menu_key);
        if ($this->db->update('sso_menu', $data_update, $where)) {

            $this->log_app->log_data($token['user_key'], 'sso_menu', 'menu_key', $data->menu_key, 'U', json_encode($data), 'Miroring SSO', 'log_sso');
            return print '1';
        } else {
            return print '0';
        }
    }

    /*================== DELETE DATA ================*/

    public function delete_data()
    {
        $token = $this->tkn;
        $data = json_decode(file_get_contents("php://input"));
        if ($this->id_app != $token['id_aplikasi']) {
            return print '0';
        }
        if ($token['user_key'] != $data->c) {
            return print '0';
        }

        $data_log = $this->log_app->get_data('sso_menu', 'menu_key', $data->menu_key);
        $sql = "DELETE FROM sso_menu WHERE menu_key = ? ";
        if ($this->db->query($sql, array($data->menu_key))) {
            $this->log_app->log_data($token['user_key'], 'sso_menu', 'menu_key', $data->menu_key, 'D', json_encode($data), 'Miroring SSO', 'log_sso');
            return print '1';
        } else {
            return print '0';
        }
    }

    /*======================== SUB Menu =============================*/

    public function insert_data_menu_sub()
    {
        $token = $this->tkn;
        $data = json_decode(file_get_contents("php://input"));
        if ($this->id_app != $token['id_aplikasi']) {
            return print '0';
        }

        if ($token['user_key'] != $data->c) {
            return print '0';
        }
        $data_insert = array(
            'id_menu' => $data->id_menu,
            'id_aplikasi' => $data->id_aplikasi,
            'nm_menu' => str_replace('**', ' ', $data->nm_menu),
            'menu_key' => $data->menu_key,
            'url' => $data->url,
            'order' => $data->order,
            'st' => $data->st,
            'c' => $data->c,
        );
        if ($this->db->insert('sso_menu_sub', $data_insert)) {
            $rs_data = $this->log_app->get_data('sso_menu_sub', 'menu_key', $data->menu_key);
            $this->log_app->log_data($token['user_key'], 'sso_menu_sub', 'menu_key', $data->menu_key, 'C', file_get_contents("php://input"), 'Miroring SSO', 'log_sso');
            return print '1';
        } else {
            return print '0';
        }
    }

    /*================== UPDATE DATA ================*/

    public function update_data_menu_sub()
    {
        $token = $this->tkn;
        $data = json_decode(file_get_contents("php://input"));
        if ($this->id_app != $token['id_aplikasi']) {
            return print '0';
        }
        $data_update = array(
            'id_menu' => $data->id_menu,
            'nm_menu' => str_replace('**', ' ', $data->nm_menu),
            'url' => $data->url,
            'order' => $data->order,
            'st' => $data->st,
            'u' => $token['user_key'],
        );
        $where = array('menu_key' => $data->menu_key);
        if ($this->db->update('sso_menu_sub', $data_update, $where)) {
            $this->log_app->log_data($token['user_key'], 'sso_menu_sub', 'menu_key', $data->menu_key, 'U', file_get_contents("php://input"), 'Miroring SSO', 'log_sso');
            return print '1';
        } else {
            return print '0';
        }
    }

    /*================== DELETE DATA ================*/

    public function delete_data_menu_sub()
    {
        $token = $this->tkn;
        $data = json_decode(file_get_contents("php://input"));
        if ($this->id_app != $token['id_aplikasi']) {
            return print '0';
        }
        if ($token['user_key'] != $data->c) {
            return print '0';
        }

        $sql = "DELETE FROM sso_menu_sub WHERE menu_key = ? ";
        if ($this->db->query($sql, array($data->menu_key))) {
            $this->log_app->log_data($token['user_key'], 'sso_menu_sub', 'menu_key', $data->menu_key, 'D', file_get_contents("php://input"), 'Miroring SSO', 'log_sso');
            return print '1';
        } else {
            return print '0';
        }
    }

}
