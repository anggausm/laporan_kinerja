<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_base_token extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }

    public function cek_akses($user_key = '', $id_app = '')
    {
        $sql = "SELECT a.user_key, a.id_aplikasi, b.level_key FROM (SELECT user_key, id_aplikasi FROM `sso_user_router`
				WHERE user_key = ?  and id_aplikasi = ? )a
				JOIN sso_level_route b on a.id_aplikasi = b.id_aplikasi and a.user_key = b.user_key";
        $query = $this->db->query($sql, array($user_key, $id_app));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
}
