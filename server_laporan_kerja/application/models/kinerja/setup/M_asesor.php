<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_asesor extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->library('form_validation');
    }

    public function get_data()
    {
        $token = $this->tkn;
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $this->db->from('kinerja_asesor');
        $query = $this->db->get();
        $result = $query->result_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    // public function get_data()
    // {
    //     $token = $this->tkn;
    //     $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
    //     $this->db->select('*');
    //     $this->db->from('kinerja_asesor');
    //     $this->db->where('periode_penilaian', $data->id_periode);
    //     $query = $this->db->get();
    //     $result = $query->result_array();
    //     // var_dump($result); die();
    //     $query->free_result();
    //     return $this->app->respons_data($result, 'Data berhasil diload', 200);
    // }

    public function insert()
    {
        $token = $this->tkn;
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();

        $data['kategori'] = empty($data['kategori']) ? 'teman' : $data['kategori'];
        if ($data['dinilai_id_doskar'] == $data['penilai_id_doskar']) {
            return $this->app->respons_data(false, 'Data gagal disimpan, yang dinilai dan penilai tidak boleh sama', 200);
        }
        $check_exist = $this->db->get_where('kinerja_asesor', array('dinilai_id_doskar' => $data['dinilai_id_doskar'], 'penilai_id_doskar' => $data['penilai_id_doskar']))->row_array();
        if (!empty($check_exist)) {
            return $this->app->respons_data(false, 'Data gagal disimpan, data sudah ada', 200);
        }
        $jm_asesor = $this->db->get_where('kinerja_asesor', array('dinilai_id_doskar' => $data['dinilai_id_doskar'], 'kategori' => 'teman'))->num_rows();
        if ($jm_asesor >= 3 && $data['kategori'] == 'teman') {
            return $this->app->respons_data(false, 'Data gagal disimpan, data asesor rekan kerja maksimal 3', 200);
        }
        $this->db->trans_start();
        $datas = array(
            'dinilai_id_doskar' => $data['dinilai_id_doskar'],
            'dinilai_user_key' => $data['dinilai_user_key'],
            'dinilai_nama' => $data['dinilai_nama'],
            'dinilai_fak_unit' => $data['dinilai_fak_unit'],
            'dinilai_nama_unit' => $data['dinilai_nama_unit'],
            'penilai_id_doskar' => $data['penilai_id_doskar'],
            'dinilai_jenis_pekerjaan' => $data['dinilai_jenis_pekerjaan'],
            'dinilai_status_kerja' => $data['dinilai_status_kerja'],
            'penilai_user_key' => $data['penilai_user_key'],
            'penilai_nama' => $data['penilai_nama'],
            'penilai_fak_unit' => $data['penilai_fak_unit'],
            'penilai_nama_unit' => $data['penilai_nama_unit'],
            'penilai_jenis_pekerjaan' => $data['penilai_jenis_pekerjaan'],
            'penilai_status_kerja' => $data['penilai_status_kerja'],
            'kategori' => $data['kategori'],
            'periode_penilaian' => $data['periode_penilaian'],
            'c' => $token['user_key'],
            'dc' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('kinerja_asesor', $datas);
        $id_asesor = $this->db->insert_id();
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal disimpan', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'kinerja_asesor', 'id_asesor', $id_asesor, 'C', json_encode($datas), '', 'log_sso');
            return $this->app->respons_data(true, 'Data berhasil disimpan', 200);
        }
    }

    public function delete()
    {
        $token = $this->tkn;
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();

        $data_log = $this->db->get_where('kinerja_asesor', array('id_asesor' => $data['id_asesor']))->row_array();
        $this->db->trans_start();
        $this->db->delete('kinerja_asesor', array('id_asesor' => $data['id_asesor']));
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal dihapus', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'kinerja_asesor', 'id_asesor', $data['id_asesor'], 'D', json_encode($data_log), '', 'log_kinerja');
            return $this->app->respons_data(true, 'Data berhasil dihapus', 200);
        }
    }
}
