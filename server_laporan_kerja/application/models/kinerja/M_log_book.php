<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_log_book extends CI_Model
{
//construktor
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }
    
    public function get_data()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $user_key = $token['user_key'];
        $tanggal = $data->tahun . '-' . $data->bulan;

        // $sql = "SELECT  kinerja_log_book.id_log_book,
        //                 kinerja_log_book.user_key,
        //                 kinerja_log_book.tanggal_log_book,
        //                 kinerja_log_book.tugas,
        //                 kinerja_log_book.status_pekerjaan,
        //                 kinerja_log_book.ket,
        //                 kinerja_log_book.cd,
        //                 kinerja_log_book.c,
        //                 YEAR(tanggal_log_book) as tahun,
        //                 MONTH(tanggal_log_book) as bulan
        //                 FROM
        //                     kinerja_log_book
        //                 WHERE
        //                  tanggal_log_book LIKE '$tanggal%'
        //                  AND user_key = '$user_key'
        //                 ORDER BY
        //                     tanggal_log_book ASC";

        $sql = "SELECT  kinerja_log_book.id_log_book,
                        kinerja_log_book.user_key,
                        kinerja_log_book.tanggal_log_book,
                        kinerja_log_book.tugas,
                        kinerja_log_book.status_pekerjaan,
                        kinerja_log_book.ket,
                        kinerja_period_log.tgl_awal,
                        kinerja_period_log.tgl_akhir,
                        kinerja_periode.pengisian_mulai,
                        kinerja_periode.pengisian_selesai,
                        kinerja_periode.nama_periode
                        FROM
                        kinerja_log_book
                        JOIN kinerja_period_log ON kinerja_log_book.id_periode_log = kinerja_period_log.id
                        JOIN kinerja_periode ON kinerja_period_log.periode_kinerja = kinerja_periode.id_periode
                        WHERE user_key = '$user_key' and kinerja_period_log.periode_kinerja = '$data->id_periode'
                        ORDER BY
                        tanggal_log_book ASC";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function cek_insert($id = '')
    {
         $token = $this->tkn;
         $userkey = $token['user_key'];
        $sql = "SELECT id_log_book, user_key FROM kinerja_log_book WHERE id_periode_log = '$id' AND user_key = '$userkey'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function insert_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
       
        $data_insert = array(
            "user_key" => $token['user_key'],
            "id_periode_log" => $data['tanggal_log_book'],
            "tugas" => $data['tugas'],
            "status_pekerjaan" => $data['status_pekerjaan'],
            "ket" => $data['ket'],
            "cd" => date("Y-m-d h:i:s"),
            "c" => $token['user_key']
        );

        if ($this->cek_insert($data['tanggal_log_book']) == false) {
            return $this->app->respons_data(false, 'Log sudah ada', 200);
        } else {
            if ($this->db->insert('kinerja_log_book', $data_insert)) {
                $id = $this->db->insert_id();
                $this->log_app->log_data($token['user_key'], 'kinerja_log_book', 'id', $id, 'C', json_encode($data_insert), 'Insert Data Log Book', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil simpan', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal disimpan', 200);
            }
        }
        
    }

    public function edit() {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT  kinerja_log_book.id_log_book,
                        kinerja_log_book.id_periode_log,
                        kinerja_log_book.user_key,
                        kinerja_log_book.tanggal_log_book,
                        kinerja_log_book.tugas,
                        kinerja_log_book.status_pekerjaan,
                        kinerja_log_book.skor,
                        kinerja_log_book.ket,
                        kinerja_log_book.stat,
                        kinerja_log_book.file_foto,
                        kinerja_log_book.cd,
                        kinerja_log_book.c,
                        kinerja_log_book.ud,
                        kinerja_log_book.u,
                        kinerja_period_log.tgl_awal,
                        kinerja_period_log.tgl_akhir
                        FROM
                        kinerja_log_book
                        LEFT JOIN kinerja_period_log ON kinerja_log_book.id_periode_log = kinerja_period_log.id
                        WHERE id_log_book = ?";
        $query = $this->db->query($sql, $data['id']);

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function update_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
                
        $data_update = array(
            "tugas" => $data['tugas'],
            // "id_periode_log" => $data['tanggal_log_book'],
            "status_pekerjaan" => $data['status_pekerjaan'],
            "ket" => $data['ket'],
            "u" => $token['user_key'],
            "ud" => date("Y-m-d h:i:s")
        );

            if ($this->db->update('kinerja_log_book', $data_update, array('id_log_book' => $data['id_log_book']))) {
                $this->log_app->log_data($token['username'], 'kinerja_log_book', 'id_log_book', $data['id_log_book'], 'U', json_encode($data_update), 'Update Data Log Book', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil update', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal update', 200);
            }
    }

    public function get_data_edit($id = '')
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT * FROM kinerja_log_book WHERE id_log_book = '$id'";
        
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return true;
        } else {
            return false;
        }
    }

    public function delete_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $token = $this->tkn;
        $data_test = $this->get_data_edit($data['id']);
        
        if (empty($data)) {
            return $this->app->respons_data(array(), 'Data kosong', 200);
        }
        if ($this->db->delete('kinerja_log_book', array('id_log_book' => $data['id']))) {
            $this->log_app->log_data($token['username'], 'kinerja_log_book', 'id_log_book', $data['id'], 'D', json_encode($data_test), 'Delete Data Log Book', 'log_sia');
            return $this->app->respons_data(array(), 'Data berhasil dihapus', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal dihapus', 200);
        }
    }

    public function get_periode_log_aktif()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $user_key = $token['user_key'];
        $tanggal = $data->tahun . '-' . $data->bulan;

        $sql = "SELECT  kinerja_period_log.id,
                        kinerja_period_log.nama_log,
                        kinerja_period_log.tgl_awal,
                        kinerja_period_log.tgl_akhir,
                        kinerja_period_log.periode_kinerja,
                        kinerja_periode.pengisian_mulai,
                        kinerja_periode.pengisian_selesai,
                        kinerja_periode.nama_periode
                        FROM
                        kinerja_period_log
                        LEFT JOIN kinerja_periode ON kinerja_period_log.periode_kinerja = kinerja_periode.id_periode
                        WHERE (CURDATE() BETWEEN kinerja_periode.pengisian_mulai AND kinerja_periode.pengisian_selesai)
                        AND kinerja_period_log.status = '1'
                        ORDER BY kinerja_period_log.tgl_awal ASC  ";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_semua_periode()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT  * from kinerja_periode where kategori = 'Kependidikan' order by penilaian_mulai DESC";

        $query = $this->db->query($sql, array());

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }

    }

    public function get_periode_log_pk()
    {
        $data = json_decode(file_get_contents("php://input"));
        $sql = "SELECT  kinerja_period_log.id,
                        kinerja_period_log.nama_log,
                        kinerja_period_log.tgl_awal,
                        kinerja_period_log.tgl_akhir,
                        kinerja_period_log.periode_kinerja,
                        kinerja_periode.pengisian_mulai,
                        kinerja_periode.pengisian_selesai,
                        kinerja_periode.nama_periode
                        FROM
                        kinerja_period_log
                        LEFT JOIN kinerja_periode ON kinerja_period_log.periode_kinerja = kinerja_periode.id_periode
                        WHERE kinerja_periode.id_periode = '$data->id_periode'
                        AND kinerja_period_log.status = '1'
                        ORDER BY kinerja_period_log.tgl_awal ASC  ";

        $query = $this->db->query($sql, array());

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_periode_aktif()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT * FROM kinerja_periode
                        WHERE (CURDATE() BETWEEN pengisian_mulai AND pengisian_selesai) 
                        AND kategori = 'Kependidikan'";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
}