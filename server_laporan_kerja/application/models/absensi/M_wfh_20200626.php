<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_wfh extends CI_Model
{
//construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }
    public function index()
    {
    }

    public function get_doskar($user_key = '')
    {
        $sql = "SELECT user_key, shift_finjer FROM doskar_usm WHERE user_key = '$user_key'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    public function insert_a_berangkat()
    {
        $token = $this->tkn;
        $id_absen = $this->get_id();
        $data = json_decode(file_get_contents("php://input"));
        $tanggal = date('Y-m-d');
        $hari = $this->namahari();
        $jam_masuk = date("H:i:s");
        $kordinat_masuk = $data->latitude . ',' . $data->longitude;
        $z_kordinat_masuk = $data->latitude2 . ',' . $data->longitude2;
        $keterangan = $data->keterangan;
        $cek = $this->cek_insert($tanggal, $token['user_key']);
        $shitf = $this->get_doskar($token['user_key']);
        if (!empty($cek['id_absen'])) {
            return $this->app->respons_data($cek, 'Anda sudah melakukan presensi masuk hari ini..', 200);
        }
        // akses_masuk, akses_pulang
        //z_kordinat_masuk , longitude2
        $sql = "INSERT INTO wfh_absensi (id_absen, user_key, tanggal, hari, jam_masuk, kordinat_masuk, akses_masuk, z_kordinat_masuk, shitf) VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?) ";
        if ($this->db->query($sql, array($id_absen, $token['user_key'], $tanggal, $hari, $jam_masuk, $kordinat_masuk, $keterangan, $z_kordinat_masuk, $shitf['shift_finjer']))) {
            $this->get_radius_berangkat($id_absen);
            $this->log_app->log_data($token['user_key'], 'wfh_absensi', 'id_absen', $id_absen, 'C', json_encode(array('id_absen' => $id_absen, 'user_key' => $token['user_key'], 'tanggal' => $tanggal, 'hari' => $hari, 'jam_masuk' => $jam_masuk, 'kordinat_masuk' => $kordinat_masuk)), ' ', 'log_kinerja');
            return $this->app->respons_data(array('id_absen' => $id_absen), 'Data berhasil simpan', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal disimpan diload', 200);
        }
    }

    public function get_radius_berangkat($id_absen = '')
    {
        $central_latitude = "-6.9817454";
        $central_longitude = "110.452002";
        $sql = "SELECT a.*, round(a.distance * 1000,1) jarak_dari_titik_pusat FROM  (SELECT
                  id_absen, user_key ,(
                    6371 * acos (
                      cos ( radians($central_latitude) )
                      * cos( radians( lat_berangkat ) )
                      * cos( radians( long_berangkat ) - radians($central_longitude) )
                      + sin ( radians($central_latitude) )
                      * sin( radians( lat_berangkat ) )
                    )
                  ) AS distance
                FROM (
                SELECT id_absen, user_key,kordinat_masuk,
                SUBSTRING_INDEX(kordinat_masuk,',','1') lat_berangkat,
                SUBSTRING_INDEX(kordinat_masuk,',','-1') long_berangkat
                FROM `wfh_absensi` WHERE id_absen = '$id_absen' )a )a";
        $query = $this->db->query($sql, array($id_absen));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();

            //update jarak
            $sql_update = "UPDATE  wfh_absensi SET radius_berangkat = '$result[jarak_dari_titik_pusat]'  WHERE id_absen = '$id_absen'";
            $this->db->query($sql_update, array());
        } else {
            return array();
        }
    }

    public function get_radius_pulang($id_absen = '')
    {
        //-6.9829042,110.4516949
        //-6.9817454,110.452002
        $central_latitude = "-6.9817454";
        $central_longitude = "110.452002";
        $sql = "SELECT a.*, round(a.distance * 1000,1) jarak_dari_titik_pusat FROM  (SELECT
                  id_absen, user_key ,(
                    6371 * acos (
                      cos ( radians($central_latitude) )
                      * cos( radians(lat_pulang) )
                      * cos( radians(long_pulang) - radians($central_longitude) )
                      + sin ( radians($central_latitude) )
                      * sin( radians(lat_pulang) )
                    )
                  ) AS distance
                FROM (
                SELECT id_absen, user_key,kordinat_masuk,
                SUBSTRING_INDEX(kordinat_pulang,',','1') lat_pulang,
                SUBSTRING_INDEX(kordinat_pulang,',','-1') long_pulang
                FROM `wfh_absensi` WHERE id_absen = '$id_absen' )a )a";
        $query = $this->db->query($sql, array($id_absen));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            //update jarak
            $sql_update = "UPDATE  wfh_absensi SET radius_pulang = '$result[jarak_dari_titik_pusat]'  WHERE id_absen = '$id_absen'";
            $this->db->query($sql_update, array());
        } else {
            return array();
        }
    }

    public function get_radius_lokasi($id_absen = '')
    {

        $sql = "SELECT id_absen, hari, tanggal, kordinat_masuk, kordinat_pulang,
                if(a.c_lat_b = '1' and a.c_long_b ='1', '1', '0') lokasi_berangkat,
                if(a.c_lat_p = '1' and a.c_long_p ='1', '1', '0') lokasi_pulang  FROM (SELECT a.*,
                 if(lat_berangkat = null ,'0', if(lat_berangkat >= '-6.980480' and  lat_berangkat <= '-6.983081', '1','0')) c_lat_b,
                 if(long_berangkat = null ,'0',if(long_berangkat >= '110.451872' and  long_berangkat <= '110.453386', '1','0')) c_long_b,
                 if(lat_pulang = null ,'0',if(lat_pulang >= '-6.980480' and  lat_pulang <= '-6.983081', '1','0')) c_lat_p,
                 if(long_pulang = null ,'0',if(long_pulang >= '110.451872' and  long_pulang <= '110.453386', '1','0')) c_long_p
                 FROM (SELECT id_absen, hari, tanggal, kordinat_masuk, kordinat_pulang,
                substr(SUBSTRING_INDEX(kordinat_masuk,',', 1 ),1,9) lat_berangkat,
                substr(SUBSTRING_INDEX(kordinat_masuk,',', -1 ),1,10) long_berangkat,
                substr(SUBSTRING_INDEX(kordinat_pulang,',', 1 ),1,9) lat_pulang,
                substr(SUBSTRING_INDEX(kordinat_pulang,',', -1 ),1,10) long_pulang
                FROM wfh_absensi WHERE id_absen = ? )a)a
                 ";
        $query = $this->db->query($sql, array($id_absen));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }

    }

    public function selesai()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $tanggal = date('Y-m-d');
        $user_key = $token['user_key'];
        $jam_selesai = date("H:i:s");
        $kordinat_pulang = $data->latitude . ',' . $data->longitude;
        $presensi = $this->cek_insert($tanggal, $user_key);
        $sql = "UPDATE wfh_absensi SET  jam_pulang  = ?, kordinat_pulang = ? ,akses_pulang = ?,  z_kordinat_pulang = ?   WHERE id_absen = ?  and user_key = ? ";
        if ($this->db->query($sql, array($jam_selesai, $kordinat_pulang, $data->keterangan, $data->latitude2 . ',' . $data->longitude2, $presensi['id_absen'], $user_key))) {
            $this->get_radius_pulang($presensi['id_absen']);
            //insert rekap
            $lokasi_presensi = $this->get_radius_lokasi($presensi['id_absen']);
            $this->insert_rekap($user_key, $tanggal, $tarif = '', $lokasi_presensi['lokasi_berangkat'], $lokasi_presensi['lokasi_pulang']);

            $this->log_app->log_data($token['user_key'], 'wfh_absensi', 'id_absen', $id_absen, 'U', json_encode(array('id_absen' => $presensi['id_absen'], 'user_key' => $token['user_key'], 'status' => "Selesai Kerja Pulang", 'jam_selesai' => $jam_selesai)), ' ', 'log_kinerja');
            return $this->app->respons_data(array('id_absen' => $presensi['id_absen']), 'Terimakasih, Selamat istirahat.....', 200);
        } else {
            return $this->app->respons_data(array('id_absen' => $presensi['id_absen']), 'Terimakasih, Selamat istirahat.....', 200);
        }

    }
    public function insert_rekap($user_key = '', $tanggal = '', $tarif = '', $area_masuk = '', $area_pulang = '')
    {

        $get_status_user = $this->get_status_user($user_key, $tanggal);
//         SELECT
        // id_absensi, tanggal, telat_masuk, telat_pulang, bolos, hadir, status_transport, tarif, area_masuk, area_pulang
        // FROM `absensi_histori`;
        if ($get_status_user['tm'] == "1" or $get_status_user['tp'] == "0") {
            $hadir = '1';
        } elseif ($get_status_user['tm'] == "0" or $get_status_user['tp'] == "1") {
            $hadir = '1';
        } else {
            $hadir = '0';
        }

        if ($get_status_user['tm'] == '0' and $get_status_user['tp'] == '0') {
            $status_transport = '1';
        } else {
            $status_transport = '0';
        }

        $data_insert = array(
            "user_key" => $user_key,
            "id_absensi" => $get_status_user['id_absen'],
            "tanggal" => $get_status_user['tanggal'],
            "telat_masuk" => $get_status_user['tm'],
            "telat_pulang" => $get_status_user['tp'],
            "bolos" => "0",
            "hadir" => $hadir,
            "status_transport" => $status_transport,
            "tarif" => $tarif,
            "area_masuk" => $area_masuk,
            "area_pulang" => $area_pulang,
        );
        $this->db->insert('absensi_histori', $data_insert);
    }

    public function get_jam($user_key = '')
    {
        $shitf = $this->get_doskar($user_key);

        $sql = "SELECT jam_masuk, jam_pulang, toleransi_keterlambatan FROM absesni_setingan_jam WHERE `status` ='1' and shift ='$shitf[shift_finjer]'";
        $query = $this->db->query($sql, array());
        $result = $query->row_array();
        $query->free_result();
        return $result;

    }

    public function get_status_user($user_key = '', $tanggal = '')
    {
        $get_jam = $this->get_jam($user_key);
        $sql = "SELECT a.*, if(a.selisih_masuk < '00:00:00', '1', if(a.selisih_masuk < '00:30:00', '0', '1')) tm ,
                if(a.selisih_pumang < '00:00:00', '1', if(a.selisih_pumang < '00:30:00', '0', '1')) tp
                 FROM (SELECT id_absen, user_key, tanggal, hari, jam_masuk,  timediff( jam_masuk, '$get_jam[jam_masuk]') selisih_masuk, jam_pulang,
                  timediff( jam_pulang, '$get_jam[jam_pulang]')  selisih_pumang
                FROM wfh_absensi WHERE user_key  = '$user_key' and tanggal ='$tanggal')a ";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function cek_insert($tanggal = '', $user_key = '')
    {
        $sql = "SELECT id_absen FROM wfh_absensi WHERE tanggal = ? and user_key  = ? ";
        $query = $this->db->query($sql, array($tanggal, $user_key));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    public function detail_wfh($id_absen = '', $user_key = '')
    {
        $sql = "SELECT id_absen,  hari, tanggal, date_format(tanggal, '%d-%m-%Y') tgl, jam_masuk, kordinat_masuk, jam_pulang FROM wfh_absensi WHERE id_absen = ?  and user_key = ? ";
        $query = $this->db->query($sql, array($id_absen, $user_key));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    public function get_task_day($id_absen = '')
    {
        $sql = "SELECT  id_absen, id_task, judul_kerjaan, keterangan, type_riport, dokumen, cek, keterangan_cek,jam_task, jam_riport, date_c, date_u, link
            FROM `wfh_my_task` WHERE id_absen = '$id_absen'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    public function get_wfh()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $id_absen = $data->id_absen;
        $detail_wfh = $this->detail_wfh($id_absen, $token['user_key']);
        $get_task_day = $this->get_task_day($id_absen);
        $result = array('detail_wfh' => $detail_wfh, 'get_task_day' => $get_task_day);
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }
//insert TASK
    public function insert_task()
    {
        $token = $this->tkn;
        $data = json_decode(file_get_contents("php://input"));
        $id_task = $this->get_id();
        $tanggal = date('Y-m-d');
        $user_key = $token['user_key'];
        $jam_riport = date("H:i:s");
        $presensi = $this->cek_insert($tanggal, $user_key);
        $sql = "INSERT INTO `wfh_my_task` (id_task, id_absen, user_key, jam_task,  judul_kerjaan) VALUE (?, ?, ?, ?, ?)";
        if ($this->db->query($sql, array($id_task, $presensi['id_absen'], $user_key, $jam_riport, $data->judul_kerjaan))) {
            $this->log_app->log_data($token['user_key'], 'wfh_my_task', 'id_task', $id_task, 'C', json_encode(array('id_task' => $id_task, 'id_absen' => $presensi['id_absen'], 'user_key' => $user_key, 'judul_kerjaan' => $data->judul_kerjaan)), ' ', 'log_kinerja');
            return $this->app->respons_data(array('id_absen' => $presensi['id_absen']), 'Data berhasil simpan', 200);
        } else {
            return $this->app->respons_data(array('id_absen' => $presensi['id_absen']), 'Data gagal disimpan diload', 200);
        }
    }
//RIPORT
    public function riport_wfh()
    {
        $token = $this->tkn;
        $jam_riport = date("H:i:s");
        $tanggal = date('Y-m-d');
        $user_key = $token['user_key'];
        $presensi = $this->cek_insert($tanggal, $user_key);
        $data = json_decode(file_get_contents("php://input"));
        if (empty($data->dokumen)) {
            $sql = "UPDATE `wfh_my_task` SET  judul_kerjaan = ? ,  keterangan = ? , type_riport = ? , jam_riport = ? , link = ?    WHERE id_task = ?  and id_absen = ? ";
            $update = array($data->judul_kerjaan, $data->keterangan, $data->type_riport, $jam_riport, $data->link, $data->id_task, $presensi['id_absen']);
        } else {
            $sql = "UPDATE `wfh_my_task` SET judul_kerjaan = ?,   keterangan = ? , type_riport = ? , jam_riport = ? , dokumen = ? , link = ?    WHERE id_task = ?  and id_absen = ? ";
            $update = array($data->judul_kerjaan, $data->keterangan, $data->type_riport, $jam_riport, $data->dokumen, $data->link, $data->id_task, $presensi['id_absen']);
        }
        if ($this->db->query($sql, $update)) {
            $this->log_app->log_data($token['user_key'], 'wfh_my_task', 'id_task', $data->id_task, 'U', json_encode(array('id_task' => $id_task, 'id_absen' => $presensi['id_absen'], 'user_key' => $user_key, 'type_riport' => $data->judul_kerjaan, 'dokumen' => $data->dokumen, 'jam_riport' => $jam_riport)), ' ', 'log_kinerja');
            return $this->app->respons_data(array('id_absen' => $presensi['id_absen']), 'Data berhasil simpan', 200);
        } else {
            return $this->app->respons_data(array('id_absen' => $presensi['id_absen']), 'Data gagal disimpan diload', 200);
        }
    }

    public function cek_delete($id_task = '')
    {
        $sql = "SELECT date_u FROM `wfh_my_task` WHERE jam_riport is not null  and id_task = ? ";
        $query = $this->db->query($sql, array($id_task));
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_task()
    {
        $token = $this->tkn;
        $data = json_decode(file_get_contents("php://input"));
        $tanggal = date('Y-m-d');
        $presensi = $this->cek_insert($tanggal, $token['user_key']);
        if ($this->cek_delete($data->id_task)) {
            return $this->app->respons_data(array('id_absen' => $presensi['id_absen']), 'Maaf data tidak bisa di hapus.', 200);
        }

        $sql = "DELETE FROM `wfh_my_task` WHERE user_key = ? and id_task = ?";
        if ($this->db->query($sql, array($token['user_key'], $data->id_task))) {
            $this->log_app->log_data($token['user_key'], 'wfh_my_task', 'id_task', $data->id_task, 'D', json_encode(array('id_task' => $data->id_task, 'id_absen' => $presensi['id_absen'], 'user_key' => $user_key)), ' ', 'log_kinerja');
            return $this->app->respons_data(array('id_absen' => $presensi['id_absen']), 'Data berhasil hapus', 200);
        } else {
            return $this->app->respons_data(array('id_absen' => $presensi['id_absen']), 'Data gagal hapus', 200);
        }
    }

    public function get_task()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $tanggal = date('Y-m-d');
        $presensi = $this->cek_insert($tanggal, $token['user_key']);
        $sql = "SELECT id_task, id_absen, judul_kerjaan, keterangan, type_riport, dokumen, jam_task, jam_riport, cek, keterangan_cek
                FROM `wfh_my_task` WHERE user_key = ? and id_task = ? and id_absen = ? ";
        $query = $this->db->query($sql, array($token['user_key'], $data->id_task, $presensi['id_absen']));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    //matrial

    public function get_id()
    {
        $sql = "SELECT UUID() id_absen";
        $query = $this->db->query($sql, array());
        $result = $query->row_array();
        $query->free_result();
        return $result['id_absen'];
    }
    public function namahari()
    {
        $tgl = date('d');
        $bln = date('m');
        $thn = date('Y');
        $info = date('w', mktime(0, 0, 0, $bln, $tgl, $thn));
        if ($info == '0') {
            $hari = "Minggu";
        } elseif ($info == '1') {
            $hari = "Senin";
        } elseif ($info == '2') {
            $hari = "Selasa";
        } elseif ($info == '3') {
            $hari = "Rabu";
        } elseif ($info == '4') {
            $hari = "Kamis";
        } elseif ($info == '5') {
            $hari = "Jumat";
        } elseif ($info == '6') {
            $hari = "Sabtu";
        } elseif ($info == '7') {
        }
        return $hari;
    }

    public function download_file()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $id_task = $data->id_task;
        $sql = "SELECT id_task, REPLACE(id_task,'-','')jdl_file, judul_kerjaan, dokumen FROM wfh_my_task WHERE id_task = ? ";
        $query = $this->db->query($sql, array($id_task));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function get_detail_presensi($id_absen = '')
    {
        $sql = "SELECT jam_masuk, jam_pulang, kordinat_masuk, kordinat_pulang, tanggal, hari FROM wfh_absensi WHERE id_absen = ? ";
        $query = $this->db->query($sql, array($id_absen));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
}

/*
Rumus Get Radius
SELECT a.*, round(a.distance * 1000,1) jarak_dari_titik_pusat FROM  (SELECT
id_absen, user_key, kordinat_masuk, kordinat_pulang , (
6371 * acos (
cos ( radians(-6.981992) )
 * cos( radians( lat_berangkat ) )
 * cos( radians( long_berangkat ) - radians(110.4504783) )
+ sin ( radians(-6.981992) )
 * sin( radians( lat_berangkat ) )
)
) AS distance
FROM (
SELECT id_absen, user_key,kordinat_masuk,
SUBSTRING_INDEX(kordinat_masuk,',','1') lat_berangkat,
SUBSTRING_INDEX(kordinat_masuk,',','-1') long_berangkat
, kordinat_pulang, jam_masuk, jam_pulang
FROM `wfh_absensi` WHERE tanggal ='2020-05-11')a
HAVING distance <= 10)a

 */
