<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_riwayat extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index()
    {

    }

    public function default_history()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $tanggal = date('Y-m');
        $bulan = $bulan = $this->nama_bulan(date('m')) . ' ' . date('Y');
        $sql = "SELECT a.*, b.id_absen,
				if(a.hari ='Sabtu', 'btn-danger',
				if(a.hari = 'Minggu', 'btn-danger',
				if(b.id_absen is null ,'btn-dark', 'btn-primary')))
				st,  if(b.jm_task is null, 0, b.jm_task )jm_task, jam_masuk, jam_pulang, kordinat_masuk, kordinat_pulang  FROM (SELECT tanggal, hari, DATE_FORMAT(tanggal,'%d %M %Y') tgl FROM wfh_kalender WHERE tanggal like '$tanggal%')a
				LEFT JOIN (
				SELECT a.tanggal,  a.id_absen, if(b.jm_task is null, 0, b.jm_task) jm_task, jam_masuk, jam_pulang, kordinat_masuk, kordinat_pulang  FROM
				(SELECT tanggal, id_absen , jam_masuk, jam_pulang, kordinat_masuk, kordinat_pulang  FROM absensi_piket WHERE tanggal like '$tanggal%' and user_key = ? )a
				LEFT JOIN (SELECT id_absen, COUNT(*) jm_task FROM absensi_task_piket GROUP BY id_absen)b on a.id_absen = b.id_absen
				)b on a.tanggal = b.tanggal";
        $query = $this->db->query($sql, array($token['user_key']));
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data(array('bulan' => $bulan, 'rs' => $result), 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array('bulan' => $bulan, 'rs' => ''), 'data gagal diload', 200);
        }
    }

    public function histori_bulanan()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $tanggal = $data->tahun . '-' . $data->bulan;
        $bulan = $this->nama_bulan($data->bulan) . ' ' . $data->tahun;

        $sql = "SELECT a.*, b.id_absen,
				if(a.hari ='Sabtu', 'btn-danger',
				if(a.hari = 'Minggu', 'btn-danger',
				if(b.id_absen is null ,'btn-dark', 'btn-primary')))
				st,  if(b.jm_task is null, 0, b.jm_task )jm_task, jam_masuk, jam_pulang, kordinat_masuk, kordinat_pulang  FROM (SELECT tanggal, hari, DATE_FORMAT(tanggal,'%d %M %Y') tgl FROM wfh_kalender WHERE tanggal like '$tanggal%')a
				LEFT JOIN (
				SELECT a.tanggal,  a.id_absen, if(b.jm_task is null, 0, b.jm_task) jm_task , jam_masuk, jam_pulang, kordinat_masuk, kordinat_pulang  FROM
				(SELECT tanggal, id_absen,jam_masuk, jam_pulang, kordinat_masuk, kordinat_pulang FROM absensi_piket WHERE tanggal like '$tanggal%' and user_key = ? )a
				LEFT JOIN (SELECT id_absen, COUNT(*) jm_task FROM absensi_task_piket GROUP BY id_absen)b on a.id_absen = b.id_absen
				)b on a.tanggal = b.tanggal";
        $query = $this->db->query($sql, array($token['user_key']));
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data(array('bulan' => $bulan, 'rs' => $result), 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array('bulan' => $bulan, 'rs' => ''), 'data gagal diload', 200);
        }
    }

    public function nama_bulan($bulan = '')
    {
        if ($bulan == '01') {
            $nm_bulan = "Januari";
        } elseif ($bulan == '02') {
            $nm_bulan = "Februari";
        } elseif ($bulan == '03') {
            $nm_bulan = "Maret";
        } elseif ($bulan == '04') {
            $nm_bulan = "April";
        } elseif ($bulan == '05') {
            $nm_bulan = "Mei";
        } elseif ($bulan == '06') {
            $nm_bulan = "Juni";
        } elseif ($bulan == '07') {
            $nm_bulan = "Juli";
        } elseif ($bulan == '08') {
            $nm_bulan = "Agustus";
        } elseif ($bulan == '09') {
            $nm_bulan = "September";
        } elseif ($bulan == '10') {
            $nm_bulan = "Oktober";
        } elseif ($bulan == '11') {
            $nm_bulan = "November";
        } else {
            $nm_bulan = "Desember";

        }
        return $nm_bulan;

    }

}
