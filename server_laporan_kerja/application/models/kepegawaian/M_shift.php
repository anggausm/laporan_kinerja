<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_shift extends CI_Model
{
//construktor
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }

    public function get_list()
    {
        // $data = json_decode(file_get_contents("php://input"));
        $sql = "SELECT a.user_key, nama_doskar, nis, shift_finjer, b.keterangan 
				FROM `doskar_usm` a
				JOIN struktural_anggota b ON a.user_key = b.user_key
                ORDER BY nama_doskar ASC;";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            $result;
        } 
        $rs = array('rs_data' => $result);
        return $this->app->respons_data($rs, 'Data berhasil diload', 200);
    }

    public function edit($user_key = '') {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $sql = "SELECT a.user_key, nama_doskar, nis, shift_finjer, b.keterangan 
				FROM `doskar_usm` a
				JOIN struktural_anggota b ON a.user_key = b.user_key
				WHERE a.user_key = ?";
        $query = $this->db->query($sql, $data->user_key);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            $result;
        }
        $rs = array('rs' => $result);
        return $this->app->respons_data($rs, 'Data berhasil diload', 200);
    }

    public function update($user_key = '') {
        $data = json_decode(file_get_contents("php://input"));
    	$token = $this->tkn;
        $user_update = $token['user_key'];
        $data_update = json_encode($this->edit());
        $sql = "UPDATE doskar_usm
                SET shift_finjer = ?
                WHERE user_key = ?";
        $update = array($data->shift_finjer,
                        $data->user_key
                    );
        $this->db->query($sql, $update, $data->user_key);
        $affectedRows = $this->db->affected_rows();
        if ($affectedRows >= 0) {
            $this->log_app->log_data($user_key, 'doskar_usm', 'user_key', $user_update, 'C', json_encode(array('user_key' => $data->user_key, 'shift_finjer' => $data->shift_finjer)), ' ', 'log_kinerja');
            return $this->app->respons_data($user_update, 'Data berhasil update', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal update diload', 200);
        }
    }
}