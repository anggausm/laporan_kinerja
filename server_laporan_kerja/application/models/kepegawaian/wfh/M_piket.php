<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_piket extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }
    public function get_periode()
    {
        $sql = "SELECT id_piket_wfh, periode_wfh, keterangan, tgl_mulai, tgl_selesai FROM `piket_wfh_periode`";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function get_level()
    {
        $arr = array(
            array(
                "level" => "Tendik Fakultas", "key" => "tendik_f",
            ), array(
                "level" => "Tendik UPT & Lembaga", "key" => "tendik_upt_lembaga",
            ), array(
                "level" => "BAAK dan BAUK", "key" => "baak_bauk",
            ), array(
                "level" => "Dosen", "key" => "dosen",
            ),
        );
        return $this->app->respons_data($arr, 'Data berhasil diload', 200);
    }

    public function get_level_list()
    {
        $data = json_decode(file_get_contents("php://input"));
        if ($data->key == "tendik_f") {
            return $this->get_unit_fakultsa();
        } elseif ($data->key == "tendik_upt_lembaga") {
            return $this->get_unit_tendik_upt_lemb();
        } elseif ($data->key == "baak_bauk") {
            return $this->get_unit_baak_bauk();
        } elseif ($data->key == "dosen") {
            return $this->get_unit_fakultsa();
        } else {

        }
    }

    public function get_anggota()
    {
        $data = json_decode(file_get_contents("php://input"));
        if ($data->key == "tendik_f") {
            return $this->get_list_anggota_tendik_f($data->id_struktural, $data->id_piket_wfh);
        } elseif ($data->key == "tendik_upt_lembaga") {
            return $this->get_list_anggota_tendik_upt_lemb($data->id_struktural, $data->id_piket_wfh);
        } elseif ($data->key == "baak_bauk") {
            return $this->get_list_anggota_baak_bauk($data->id_struktural, $data->id_piket_wfh);
        } elseif ($data->key == "dosen") {
            return $this->get_list_anggota_dosen($data->id_struktural, $data->id_piket_wfh);
        } else {

        }
    }

    public function get_presensi($user_key = '', $id_piket_wfh = '')
    {

        $sql = "SELECT a.*, b.hari FROM (SELECT user_key, tanggal,    DATE_FORMAT(tanggal,'%d %M %Y') piket FROM `piket_wfh_jadwal`
WHERE user_key ='$user_key' and id_piket_wfh ='$id_piket_wfh')a
JOIN wfh_kalender b on a.tanggal = b.tanggal";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function detail_unit($id_struktural = '')
    {
        $sql = "SELECT  unit_bagian FROM struktural_sub WHERE kode_sto = ? ";
        $query = $this->db->query($sql, array($id_struktural));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    public function detail_fakultas($kode_fakultas = '')
    {
        $sql = "SELECT kode_fakultas, kode_khusus, kode_prodi, fakultas ,  fakultas unit_bagian  FROM program_studi WHERE kode_fakultas = ?  GROUP BY kode_fakultas ";
        $query = $this->db->query($sql, array($kode_fakultas));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_unit_fakultsa()
    {
        $sql = "SELECT  kode_fakultas id_struktural , kode_fakultas, kode_khusus, kode_prodi, fakultas, fakultas unit_bagian  FROM program_studi GROUP BY kode_fakultas ORDER BY kode_khusus";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function get_list_anggota_dosen($kode_fakultas = '', $id_piket_wfh = '')
    {

        $detail_fak = $this->detail_fakultas($kode_fakultas);
        $periode_wfh = $this->periode_wfh($id_piket_wfh);
        $sql = "SELECT a.*, b.user_key ,c.nis,  c.nidn, c.nama_doskar FROM (
        SELECT id_sub_struktural, nama_sub_struktural, kode_sto, `level`
                FROM struktural_sub WHERE id_struktural ='4' and kode_sto not like '4.2.%'
UNION
SELECT id_sub_struktural, nama_sub_struktural, kode_sto, `level`
                FROM struktural_sub WHERE id_struktural ='5' and kode_sto not like '5.2%'
                )a
                JOIN (SELECT id_sub_struktural, user_key, keterangan FROM struktural_anggota WHERE  fakultas ='$kode_fakultas')b
                on a.id_sub_struktural = b.id_sub_struktural
                JOIN doskar_usm c on b.user_key =c.user_key ORDER BY a.`level`, a.kode_sto";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            foreach ($result as $value) {
                $piket = $this->get_presensi($value['user_key'], $id_piket_wfh);
                $rs_arr[] = array("id_sub_struktural" => $value[''],
                    "nama_sub_struktural" => $value['nama_sub_struktural'],
                    "kode_sto" => $value['kode_sto'],
                    "level" => $value['level'],
                    "user_key" => $value['user_key'],
                    "nis" => $value['nis'],
                    "nama_doskar" => $value['nama_doskar'], "piket" => $piket);
            }
            return $this->app->respons_data(array("unit_bagian" => $detail_fak, "rs" => $rs_arr, "periode_wfh" => $periode_wfh), 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }
    public function periode_wfh($id_piket_wfh = '')
    {
        $sql = "SELECT id_piket_wfh, periode_wfh, keterangan, tgl_mulai, tgl_selesai FROM `piket_wfh_periode` WHERE id_piket_wfh = ? ";
        $query = $this->db->query($sql, array($id_piket_wfh));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    public function get_list_anggota_tendik_f($kode_fakultas = '', $id_piket_wfh = '')
    {

        $detail_fak = $this->detail_fakultas($kode_fakultas);
        $periode_wfh = $this->periode_wfh($id_piket_wfh);
        $sql = "SELECT a.*, b.user_key , c.nis, c.nama_doskar FROM (
        SELECT id_sub_struktural, nama_sub_struktural, kode_sto, `level`
                FROM struktural_sub WHERE id_struktural ='4' and kode_sto like '4.2.%'
UNION
SELECT id_sub_struktural, nama_sub_struktural, kode_sto, `level`
                FROM struktural_sub WHERE id_struktural ='5' and kode_sto like '5.2%'
                )a
				JOIN (SELECT id_sub_struktural, user_key, keterangan FROM struktural_anggota WHERE keterangan !='dosen' and fakultas ='$kode_fakultas')b
				on a.id_sub_struktural = b.id_sub_struktural
				JOIN doskar_usm c on b.user_key =c.user_key ORDER BY a.kode_sto";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            foreach ($result as $value) {
                $piket = $this->get_presensi($value['user_key'], $id_piket_wfh);
                $rs_arr[] = array("id_sub_struktural" => $value[''],
                    "nama_sub_struktural" => $value['nama_sub_struktural'],
                    "kode_sto" => $value['kode_sto'],
                    "level" => $value['level'],
                    "user_key" => $value['user_key'],
                    "nis" => $value['nis'],
                    "nama_doskar" => $value['nama_doskar'], "piket" => $piket);
            }

            return $this->app->respons_data(array("unit_bagian" => $detail_fak, "rs" => $rs_arr, "periode_wfh" => $periode_wfh), 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }
    public function get_unit_baak_bauk()
    {
        $sql = "SELECT * FROM (
                SELECT kode_sto id_struktural, unit_bagian, kode_sto FROM (
                SELECT id_struktural, unit_bagian, kode_sto FROM struktural_sub WHERE id_struktural ='2'  and `level` <=2  GROUP BY unit_bagian)a
                UNION
                SELECT kode_sto id_struktural, unit_bagian, kode_sto FROM (
                SELECT id_struktural, unit_bagian, kode_sto FROM struktural_sub WHERE id_struktural ='3' and `level` <=2    GROUP BY unit_bagian)a
                )a ORDER BY a.kode_sto ";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function get_list_anggota_baak_bauk($id_struktural = '', $id_piket_wfh = '')
    {

        $detail_struktural = $this->detail_unit($id_struktural);
        $periode_wfh = $this->periode_wfh($id_piket_wfh);
        $sql = "SELECT a.*, b.user_key , c.nis, c.nama_doskar FROM (
                SELECT  id_sub_struktural, nama_sub_struktural, kode_sto, `level` FROM struktural_sub WHERE id_struktural ='2' or id_struktural ='3'
                 )a
                JOIN (SELECT id_angota_struktural, id_sub_struktural, user_key, keterangan FROM struktural_anggota WHERE keterangan !='dosen' )b
                on a.id_sub_struktural = b.id_sub_struktural
                JOIN doskar_usm c on b.user_key =c.user_key
               WHERE   a.kode_sto like '$id_struktural%'  ORDER BY b.id_angota_struktural";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();

            foreach ($result as $value) {
                $piket = $this->get_presensi($value['user_key'], $id_piket_wfh);
                $rs_arr[] = array("id_sub_struktural" => $value[''],
                    "nama_sub_struktural" => $value['nama_sub_struktural'],
                    "kode_sto" => $value['kode_sto'],
                    "level" => $value['level'],
                    "user_key" => $value['user_key'],
                    "nis" => $value['nis'],
                    "nama_doskar" => $value['nama_doskar'], "piket" => $piket);
            }

            return $this->app->respons_data(array("unit_bagian" => $detail_struktural, "rs" => $rs_arr, "periode_wfh" => $periode_wfh), 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function get_unit_tendik_upt_lemb()
    {
        $sql = "SELECT '6.1' id_struktural, id_sub_struktural, unit_bagian, kode_sto, `level` FROM struktural_sub WHERE kode_sto = '6.1'
                UNION
                SELECT '7.1' id_struktural, id_sub_struktural, unit_bagian, kode_sto, `level` FROM struktural_sub WHERE kode_sto = '7.1'
                UNION
                SELECT '8.1' id_struktural, id_sub_struktural, unit_bagian, kode_sto, `level` FROM struktural_sub WHERE kode_sto = '8.1'
                UNION
                SELECT '8.2' id_struktural, id_sub_struktural, unit_bagian, kode_sto, `level` FROM struktural_sub WHERE kode_sto = '8.2'
                UNION
                SELECT '8.3' id_struktural, id_sub_struktural, unit_bagian, kode_sto, `level` FROM struktural_sub WHERE kode_sto = '8.3'
                UNION
                SELECT '8.4' id_struktural, id_sub_struktural, unit_bagian, kode_sto, `level` FROM struktural_sub WHERE kode_sto = '8.4'
                UNION
                SELECT '8.5' id_struktural, id_sub_struktural, unit_bagian, kode_sto, `level` FROM struktural_sub WHERE kode_sto = '8.5'
                UNION
                SELECT '8.6' id_struktural, id_sub_struktural, unit_bagian, kode_sto, `level` FROM struktural_sub WHERE kode_sto = '8.6'
                UNION
                SELECT '8.7' id_struktural, id_sub_struktural, unit_bagian, kode_sto, `level` FROM struktural_sub WHERE kode_sto = '8.7'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function get_list_anggota_tendik_upt_lemb($id_struktural = '', $id_piket_wfh = '')
    {

        $detail_struktural = $this->detail_unit($id_struktural);
        $periode_wfh = $this->periode_wfh($id_piket_wfh);
        $sql = "SELECT a.*, b.user_key , c.nis, c.nama_doskar FROM (
                SELECT '6.1' id_struktural, id_sub_struktural, nama_sub_struktural, kode_sto, `level` FROM struktural_sub WHERE kode_sto like '6.1.%'
                UNION
                SELECT '7.1' id_struktural, id_sub_struktural, nama_sub_struktural, kode_sto, `level` FROM struktural_sub WHERE kode_sto like '7.1.%'
                UNION
                SELECT '8.1' id_struktural, id_sub_struktural, nama_sub_struktural, kode_sto, `level` FROM struktural_sub WHERE kode_sto like '8.1.%'
                UNION
                SELECT '8.2' id_struktural, id_sub_struktural, nama_sub_struktural, kode_sto, `level` FROM struktural_sub WHERE kode_sto like '8.2.%'
                UNION
                SELECT '8.3' id_struktural, id_sub_struktural, nama_sub_struktural, kode_sto, `level` FROM struktural_sub WHERE kode_sto like '8.3.%'
                UNION
                SELECT '8.4' id_struktural, id_sub_struktural, nama_sub_struktural, kode_sto, `level` FROM struktural_sub WHERE kode_sto like '8.4.%'
                UNION
                SELECT '8.5' id_struktural, id_sub_struktural, nama_sub_struktural, kode_sto, `level` FROM struktural_sub WHERE kode_sto like '8.5.%'
                UNION
                SELECT '8.6' id_struktural, id_sub_struktural, nama_sub_struktural, kode_sto, `level` FROM struktural_sub WHERE kode_sto like '8.6.%'
                UNION
                SELECT '8.7' id_struktural, id_sub_struktural, nama_sub_struktural, kode_sto, `level` FROM struktural_sub WHERE kode_sto like '8.7.%'
                )a
				JOIN (SELECT id_angota_struktural, id_sub_struktural, user_key, keterangan FROM struktural_anggota WHERE keterangan !='dosen' )b
				on a.id_sub_struktural = b.id_sub_struktural
				JOIN doskar_usm c on b.user_key =c.user_key
               WHERE   a.id_struktural ='$id_struktural'  ORDER BY b.id_angota_struktural";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            foreach ($result as $value) {
                $piket = $this->get_presensi($value['user_key'], $id_piket_wfh);
                $rs_arr[] = array("id_sub_struktural" => $value[''],
                    "nama_sub_struktural" => $value['nama_sub_struktural'],
                    "kode_sto" => $value['kode_sto'],
                    "level" => $value['level'],
                    "user_key" => $value['user_key'],
                    "nis" => $value['nis'],
                    "nama_doskar" => $value['nama_doskar'], "piket" => $piket);
            }

            return $this->app->respons_data(array("unit_bagian" => $detail_struktural, "rs" => $rs_arr, "periode_wfh" => $periode_wfh), 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function detail_doskar($user_key = '')
    {
        $sql = "SELECT a.*, c.kode_sto, c.nama_sub_struktural FROM (
		        SELECT user_key, nama_doskar, nis FROM doskar_usm WHERE user_key = '$user_key')a
				JOIN struktural_anggota b on a.user_key = b.user_key
				JOIN struktural_sub c on b.id_sub_struktural = c.id_sub_struktural
				WHERE b.keterangan not like '%dosen%'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }

    }
    public function jadwal_piket()
    {
        $data = json_decode(file_get_contents("php://input"));
        $id_piket_wfh = $data->id_piket_wfh;
        $user_key = $data->user_key;
        $detail_doskar = $this->detail_doskar($user_key);
        $sql = "SELECT periode_wfh, keterangan, date_format(tgl_mulai, '%d %M %Y') mulai, date_format(tgl_selesai, '%d %M %Y')selesai , tgl_mulai, tgl_selesai, id_piket_wfh FROM `piket_wfh_periode` WHERE id_piket_wfh = ? ";
        $query = $this->db->query($sql, array($id_piket_wfh));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();

            $sql_tgl = "SELECT a.*, b.id, b.id_piket_wfh, b.user_key FROM (
			            SELECT tanggal, hari, date_format(tanggal, '%d %M %Y') tgl FROM wfh_kalender WHERE tanggal BETWEEN '$result[tgl_mulai]' and '$result[tgl_selesai]')a
						LEFT JOIN (SELECT id, id_piket_wfh, tanggal, user_key FROM piket_wfh_jadwal
						WHERE user_key = '$user_key')b on a.tanggal = b.tanggal ORDER BY a.tanggal";
            $query_tgl = $this->db->query($sql_tgl, array());
            $result_tgl = $query_tgl->result_array();
            $query->free_result();
            $arr = array("detail_doskar" => $detail_doskar, "wfh_periode" => $result, "jadwal_piket" => $result_tgl);
            return $this->app->respons_data($arr, 'data piket', 200);
        } else {
            $arr = array("detail_doskar" => $detail_doskar, "wfh_periode" => array(), "jadwal_piket" => array());
            return $this->app->respons_data($arr, 'data gagal diload', 200);
        }
    }

    public function simpan_jadwal_piket()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        //SELECT id_piket_wfh, tanggal, user_key, `status`, c FROM `piket_wfh_jadwal`;
        $data_insert = array(
            "id_piket_wfh" => $data->id_piket_wfh,
            "tanggal" => $data->tanggal,
            "user_key" => $data->user_key,
            "status" => '0',
            "c" => $token['user_key'],
        );
        if ($this->db->insert('piket_wfh_jadwal', $data_insert)) {
            $this->log_app->log_data($token['user_key'], 'piket_wfh_jadwal', 'tanggal, user_key', $data->tanggal . ',' . $data->user_key, 'C', json_encode($data_insert), ' ', 'log_kinerja');
            return $this->app->respons_data(array(), 'Data berhasil simpan', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal disimpan diload', 200);
        }

    }

    public function delete_jadwal_piket()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;

        $sql = "delete from `piket_wfh_jadwal` WHERE   user_key = '$data->user_key'  and tanggal = '$data->tanggal' and id_piket_wfh = '$data->id_piket_wfh'";
        if ($this->db->query($sql, array())) {
            $this->log_app->log_data($token['user_key'], 'piket_wfh_jadwal', 'tanggal, user_key', $data->tanggal . ',' . $data->user_key, 'D', json_encode($data_insert), 'Hapus Jadwal Piket', 'log_kinerja');
            return $this->app->respons_data(array(), 'Data berhasil hapus', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal hapus', 200);
        }
    }

}
