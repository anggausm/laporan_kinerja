<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_tendik extends CI_Model
{
//construktor
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
    }
    public function get_biodata($user_key = '')
    {
        // $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $user_key = $token['user_key'];
        $sql = "SELECT nama_doskar, nis, nidn, no_ktp, tgl_lhr, DATE_FORMAT(tgl_lhr,'%d/%m/%Y') tgl_lhr_format, tmpt_lhr, agama, jns_kelamin, gol_darah, handphone, email, alamat, kota, kode_pos
				FROM `doskar_usm`
				WHERE user_key = ?";
        $query = $this->db->query($sql, array($user_key));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            $result;
        } 
        $rs = array('rs' => $result);
        return $this->app->respons_data($rs, 'Data berhasil diload', 200);
    }

    public function edit($user_key = '') {
        // $data = json_decode(file_get_contents("php://input"));
    	$token = $this->tkn;
        $user_key = $token['user_key'];
        $sql = "SELECT nama_doskar, nis, nidn, no_ktp, tgl_lhr, tmpt_lhr, agama, jns_kelamin, gol_darah, handphone, email, alamat, kota, 
                kode_pos
                FROM `doskar_usm`
                WHERE user_key = ?";
        $query = $this->db->query($sql, array($user_key));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            $result;
        } 
        $rs = array('rs' => $result);
        return $this->app->respons_data($rs, 'Data berhasil diload', 200);
    }

    public function update($user_key = '') {
        $data = json_decode(file_get_contents("php://input"));
    	$token = $this->tkn;
        $user_key = $token['user_key'];
        $data_update = json_encode($this->edit());
        $sql = "UPDATE doskar_usm 
                SET nama_doskar = ?, nidn = ?, no_ktp = ?, tgl_lhr = ?, tmpt_lhr = ?, agama = ?, jns_kelamin = ?, gol_darah = ?, handphone = ?, email = ?, alamat = ?, kota = ?, kode_pos = ?
                WHERE user_key = ?";
        $update = array($data->nama_doskar,
                        $data->nidn,
                        $data->no_ktp,
                        $data->tgl_lhr,
                        $data->tmpt_lhr,
                        $data->agama,
                        $data->jns_kelamin,
                        $data->gol_darah,
                        $data->handphone,
                        $data->email,
                        $data->alamat,
                        $data->kota,
                        $data->kode_pos,
                        $user_key
                    );
        $this->db->query($sql, $update);
        $affectedRows = $this->db->affected_rows();
        if ($affectedRows >= 0) { 
            $this->log_app->log_data($user_key, 'doskar_usm', 'user_key', $user_key, 'C', json_encode(array('user_key' => $user_key, 'nama_doskar' => $nama_doskar, 'nidn' => $nidn, 'no_ktp' => $no_ktp, 'tgl_lhr' => $tgl_lhr, 'tmpt_lhr' => $tmpt_lhr, 'agama' => $agama, 'jns_kelamin' => $jns_kelamin, 'gol_darah' => $gol_darah, 'handphone' => $handphone, 'email' => $email, 'alamat' => $alamat, 'kota' => $kota, 'kode_pos' => $kode_pos)), ' ', 'log_kinerja');
            return $this->app->respons_data(array('user_key' => $user_key), 'Data berhasil update', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal update diload', 200);
        }
    }
}