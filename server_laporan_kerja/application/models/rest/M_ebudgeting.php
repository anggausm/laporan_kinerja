<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_ebudgeting extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index()
    {

    }

    public function akses_struktural()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
        $sql = "SELECT id_struktural, nama_struktural, id_sub_struktural, nama_sub_struktural, id_angota_struktural, unit_bagian, keterangan, kode_sto, `level`, level_pimpinan, `status`, fakultas, kode_khusus FROM (SELECT a.*, b.nama_struktural FROM(SELECT id_angota_struktural, a.id_sub_struktural, a.user_key, fakultas, kode_khusus, keterangan, b.id_struktural, b.unit_bagian, b.nama_sub_struktural, b.kode_sto, b.`level`, b.level_pimpinan, b.`status` FROM `struktural_anggota` a
        JOIN struktural_sub b ON a.id_sub_struktural = b.id_sub_struktural
        WHERE a.user_key = ?) a
        JOIN struktural b ON a.id_struktural = b.id_struktural) a";
        $query = $this->db->query($sql, array($token['user_key']));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }
}
