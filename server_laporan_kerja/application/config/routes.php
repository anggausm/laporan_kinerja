<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = false;

//versoning app android
$route['api/versi']['PUT'] = 'versi/get_data';
/*
REST LIB
 */

$route['api/akademik/struktural']['PUT'] = 'rest/akademik/akses_fakultas_tu';
$route['api/ebudgeting/struktural']['PUT'] = 'rest/ebudgeting/akses_struktural';

/*
Re New Token
 */
$route['api/new/token']['PATCH'] = 'base_token/get_playload';
$route['api/cek_token']['PATCH'] = 'base_token/cek_base_token';

/*
======================== ROOTS AKSES APPS ======================
 */
//Menu
$route['api/root_app/menu/inserts']['PATCH'] = 'root/acl/menu/insert_data';
$route['api/root_app/menu/updatess']['PATCH'] = 'root/acl/menu/update_data';
$route['api/root_app/menu/deletos']['DELETE'] = 'root/acl/menu/delete_data';
//Sub Menu
$route['api/root_app/menu/sub_insert']['PATCH'] = 'root/acl/menu/sub_insert';
$route['api/root_app/menu/sub_update']['PATCH'] = 'root/acl/menu/sub_update';
$route['api/root_app/menu/sub_deletos']['DELETE'] = 'root/acl/menu/sub_deletos';

//LEVEL
$route['api/root_app/level/inserts']['PATCH'] = 'root/acl/level/insert_data';
$route['api/root_app/level/inserts_root']['PATCH'] = 'root/acl/level/insert_data_roots';
$route['api/root_app/level/updatess']['PATCH'] = 'root/acl/level/update_data';
$route['api/root_app/level/deletos']['DELETE'] = 'root/acl/level/delete_data';
$route['api/root_app/level/kelola_akses']['PATCH'] = 'root/acl/level/act_simpan';
//USER
$route['api/root_app/user/deletos']['DELETE'] = 'root/acl/user/delete_data';
$route['api/root_app/user/inserts']['PATCH'] = 'root/acl/user/insert_data';

/*
Laporan Kinerja WFH
 */
$route['api/wfh/presensi']['PATCH'] = 'absensi/wfh/insert_a_berangkat';
$route['api/wfh/selesai']['PATCH'] = 'absensi/wfh/selesai';
$route['api/wfh/cek_presensi']['PATCH'] = 'absensi/wfh/cek_absen';
$route['api/wfh/cek_presensi_mobile']['PATCH'] = 'absensi/wfh/cek_absen_mobile';
$route['api/wfh/get_wfh']['PATCH'] = 'absensi/wfh/get_wfh';
$route['api/wfh/insert_task']['PATCH'] = 'absensi/wfh/insert_task';
$route['api/wfh/delete_task']['DELETE'] = 'absensi/wfh/delete_task';
$route['api/wfh/get_task']['PATCH'] = 'absensi/wfh/get_task';
$route['api/wfh/task_riport']['PATCH'] = 'absensi/wfh/riport_wfh';
$route['api/wfh/download']['PUT'] = 'absensi/wfh/download_file';
/*
Histori
 */
$route['api/histori/default']['PUT'] = 'absensi/histori/default_history';
$route['api/histori/histori_laporan']['PUT'] = 'absensi/histori/histori_bulanan';

/*
PIKET
 */
$route['api/piket/presensi']['PATCH'] = 'absensi/piket/presensi/insert_a_berangkat';
$route['api/piket/selesai']['PATCH'] = 'absensi/piket/presensi/selesai';
$route['api/piket/cek_presensi']['PATCH'] = 'absensi/piket/presensi/cek_absen';
$route['api/piket/cek_presensi_mobile']['PATCH'] = 'absensi/piket/presensi/cek_absen_mobile';
$route['api/piket/get_wfh']['PATCH'] = 'absensi/piket/presensi/get_wfh';
$route['api/piket/insert_task']['PATCH'] = 'absensi/piket/presensi/insert_task';
$route['api/piket/delete_task']['DELETE'] = 'absensi/piket/presensi/delete_task';
$route['api/piket/get_task']['PATCH'] = 'absensi/piket/presensi/get_task';
$route['api/piket/task_riport']['PATCH'] = 'absensi/piket/presensi/riport_wfh';
$route['api/piket/download']['PUT'] = 'absensi/piket/presensi/download_file';
/*
Histori PIKET
 */
$route['api/piket/histori/default']['PUT'] = 'absensi/piket/riwayat/default_history';
$route['api/piket/histori/histori_laporan']['PUT'] = 'absensi/piket/riwayat/histori_bulanan';

/*
Angota
 */
$route['api/angota/dosen']['PUT'] = 'anggota/dosen/get_angggota';
$route['api/angota/tendik']['PUT'] = 'anggota/tendik/get_angggota';
//Laporan
$route['api/laporan/presensi/anggota']['PUT'] = 'laporan/presensi/get_angota';
$route['api/laporan/presensi/data_presensi']['PUT'] = 'laporan/presensi/get_data_presensi_default';
$route['api/laporan/presensi/detail_task']['PUT'] = 'laporan/presensi/detail_task';
$route['api/laporan/presensi/download_file']['PUT'] = 'laporan/presensi/download_file';

/*
Re Struktur
 */
$route['api/restruktur/tendik']['PUT'] = 'restruktur/tendik/get_data';
$route['api/restruktur/tendik/simpan']['POST'] = 'restruktur/tendik/simpan';
$route['api/restruktur/tendik/edit']['PUT'] = 'restruktur/tendik/edit';
$route['api/restruktur/tendik/update']['PATCH'] = 'restruktur/tendik/update';
$route['api/restruktur/tendik/delete']['DELETE'] = 'restruktur/tendik/delete';
$route['api/restruktur/tendik/doskar_usm']['PUT'] = 'restruktur/tendik/doskar_usm';

$route['api/restruktur/unit_lembaga']['PUT'] = 'restruktur/unit_lembaga/get_data';
$route['api/restruktur/unit_lembaga/simpan']['POST'] = 'restruktur/unit_lembaga/simpan';
$route['api/restruktur/unit_lembaga/edit']['PUT'] = 'restruktur/unit_lembaga/edit';
$route['api/restruktur/unit_lembaga/update']['PATCH'] = 'restruktur/unit_lembaga/update';
$route['api/restruktur/unit_lembaga/delete']['DELETE'] = 'restruktur/unit_lembaga/delete';
$route['api/restruktur/unit_lembaga/doskar_usm']['PUT'] = 'restruktur/unit_lembaga/doskar_usm';

$route['api/restruktur/pimpinan_fakultas']['PUT'] = 'restruktur/pimpinan_fakultas/get_data';
$route['api/restruktur/pimpinan_fakultas/simpan']['POST'] = 'restruktur/pimpinan_fakultas/simpan';
$route['api/restruktur/pimpinan_fakultas/edit']['PUT'] = 'restruktur/pimpinan_fakultas/edit';
$route['api/restruktur/pimpinan_fakultas/update']['PATCH'] = 'restruktur/pimpinan_fakultas/update';
$route['api/restruktur/pimpinan_fakultas/delete']['DELETE'] = 'restruktur/pimpinan_fakultas/delete';
$route['api/restruktur/pimpinan_fakultas/doskar_usm']['PUT'] = 'restruktur/pimpinan_fakultas/doskar_usm';
$route['api/restruktur/pimpinan_fakultas/fakultas']['PUT'] = 'restruktur/pimpinan_fakultas/fakultas';
$route['api/restruktur/pimpinan_fakultas/program_studi']['PUT'] = 'restruktur/pimpinan_fakultas/program_studi';

/*
Biodata
 */
$route['api/biodata/tendik']['PUT'] = 'biodata/tendik/get_biodata';
$route['api/biodata/tendik/edit']['PUT'] = 'biodata/tendik/edit';
$route['api/biodata/tendik/update']['PATCH'] = 'biodata/tendik/update';

/*
KEPEGAWAIAN
 */
//fakultas
$route['api/kpg/lap/tendik_fakultas/fak']['PUT'] = 'kepegawaian/rekap/laporan_tendik_fakultas/get_fakultsa';
$route['api/kpg/lap/tendik_fakultas/list_anggota']['PUT'] = 'kepegawaian/rekap/laporan_tendik_fakultas/get_list_anggota';
$route['api/kpg/lap/tendik_fakultas/presensi']['PUT'] = 'kepegawaian/rekap/laporan_tendik_fakultas/detail_presensi';
$route['api/kpg/lap/tendik_fakultas/download']['PUT'] = 'kepegawaian/rekap/laporan_tendik_fakultas/download_file';
//upt
$route['api/kpg/lap/upt_lembaga/unit_lembaga']['PUT'] = 'kepegawaian/rekap/Laporan_upt_lembaga/get_unit_lembaga';
$route['api/kpg/lap/upt_lembaga/list_anggota']['PUT'] = 'kepegawaian/rekap/Laporan_upt_lembaga/get_list_anggota';
$route['api/kpg/lap/upt_lembaga/presensi']['PUT'] = 'kepegawaian/rekap/Laporan_upt_lembaga/detail_presensi';
$route['api/kpg/lap/upt_lembaga/download']['PUT'] = 'kepegawaian/rekap/Laporan_upt_lembaga/download_file';
//dosen
$route['api/kpg/lap/dosen/fak']['PUT'] = 'kepegawaian/rekap/Laporan_dosen/get_fakultsa';
$route['api/kpg/lap/dosen/list_anggota']['PUT'] = 'kepegawaian/rekap/Laporan_dosen/get_list_anggota';
$route['api/kpg/lap/dosen/presensi']['PUT'] = 'kepegawaian/rekap/Laporan_dosen/detail_presensi';
$route['api/kpg/lap/dosen/download']['PUT'] = 'kepegawaian/rekap/Laporan_dosen/download_file';
//Bauk
$route['api/kpg/lap/baak_bauk/unit_lembaga']['PUT'] = 'kepegawaian/rekap/Laporan_baak_bauk/get_unit_lembaga';
$route['api/kpg/lap/baak_bauk/list_anggota']['PUT'] = 'kepegawaian/rekap/Laporan_baak_bauk/get_list_anggota';
$route['api/kpg/lap/baak_bauk/presensi']['PUT'] = 'kepegawaian/rekap/Laporan_baak_bauk/detail_presensi';
$route['api/kpg/lap/baak_bauk/download']['PUT'] = 'kepegawaian/rekap/Laporan_baak_bauk/download_file';
//WFH
$route['api/kpg/wfh/periode/get']['PUT'] = 'kepegawaian/wfh/periode/get_data';
$route['api/kpg/wfh/periode/add']['POST'] = 'kepegawaian/wfh/periode/add_data';
$route['api/kpg/wfh/periode/edit']['PUT'] = 'kepegawaian/wfh/periode/get_edit';
$route['api/kpg/wfh/periode/update']['PATCH'] = 'kepegawaian/wfh/periode/update';
$route['api/kpg/wfh/periode/delete']['DELETE'] = 'kepegawaian/wfh/periode/delete_data';
//Piket
$route['api/kpg/wfh/piket/periode']['PUT'] = 'kepegawaian/wfh/piket/get_periode';
$route['api/kpg/wfh/piket/level']['PUT'] = 'kepegawaian/wfh/piket/get_level';
$route['api/kpg/wfh/piket/level_list']['PUT'] = 'kepegawaian/wfh/piket/get_level_list';
$route['api/kpg/wfh/piket/anggota']['PUT'] = 'kepegawaian/wfh/piket/get_anggota';
$route['api/kpg/wfh/piket/jadwal_piket']['PUT'] = 'kepegawaian/wfh/piket/jadwal_piket';
$route['api/kpg/wfh/piket/simpan_jadwal']['PATCH'] = 'kepegawaian/wfh/piket/simpan_jadwal_piket';
$route['api/kpg/wfh/piket/delete_jadwal']['DELETE'] = 'kepegawaian/wfh/piket/delete_jadwal_piket';

/*
Shift
 */
$route['api/kpg/shift/list']['PUT'] = 'kepegawaian/shift/get_list';
$route['api/kpg/shift/edit']['PUT'] = 'kepegawaian/shift/edit';
$route['api/kpg/shift/update']['PATCH'] = 'kepegawaian/shift/update';




// =================================== Manajemen Ijin ============================================
// Ijin
$route['api/ijin/tambah/default']['PUT'] 	= 'ijin/tambah_ijin/get_data';
$route['api/ijin/tambah/simpan']['PATCH'] 	= 'ijin/tambah_ijin/insert';
$route['api/ijin/tambah/edit']['PUT'] 		= 'ijin/tambah_ijin/edit';
$route['api/ijin/tambah/update']['PATCH'] 	= 'ijin/tambah_ijin/update';
$route['api/ijin/tambah/get_doskar']['PUT'] = 'ijin/tambah_ijin/doskar_usm';
$route['api/ijin/tambah/get_jenis']['PUT'] 	= 'ijin/tambah_ijin/jenis_ijin';

//histori ijin
$route['api/ijin/ijin_histori/default']['PUT'] 	= 'ijin/ijin_histori/get_data';


// Cuti
$route['api/ijin/cuti_data/default']['PUT'] 	= 'ijin/cuti/get_data';
$route['api/ijin/cuti_data/simpan']['PATCH'] 	= 'ijin/cuti/insert';
$route['api/ijin/cuti_data/edit']['PUT'] 		= 'ijin/cuti/edit';
$route['api/ijin/cuti_data/update']['PATCH'] 	= 'ijin/cuti/update';
$route['api/ijin/cuti_data/get_jenis']['PUT'] 	= 'ijin/cuti/jenis_cuti';

// histori cuti
$route['api/ijin/cuti_histori/default']['PUT'] 	= 'ijin/cuti_histori/get_data';

// ijin studi
$route['api/ijin/study/default']['PUT'] 		= 'ijin/studi/get_data';

// histori ijin studi

// absen khusus

// =================================== Rekap Transport ============================================
// posting
$route['api/transport/posting/default']['PUT'] = 'transport/all_posting/get_data';
$route['api/transport/posting/datacek']['POST'] = 'transport/all_posting/cek_data';
$route['api/transport/posting/simpan']['POST'] = 'transport/all_posting/insert';

// histori posting

// =================================== Kinerja Pegawai ============================================
// log book
$route['api/kinerja/log_book/default']['PUT'] 	= 'kinerja/log_book/get_data';
$route['api/kinerja/log_book/simpan']['PATCH'] 	= 'kinerja/log_book/insert';
$route['api/kinerja/log_book/edit']['PUT'] 		= 'kinerja/log_book/edit';
$route['api/kinerja/log_book/update']['PATCH'] 	= 'kinerja/log_book/update';
$route['api/kinerja/log_book/hapus']['DELETE'] 	= 'kinerja/log_book/delete';
$route['api/kinerja/log_book/p_log']['PUT'] 	= 'kinerja/log_book/periode_log';

// kinerja pegawai
$route['api/kinerja/pegawai/default']['PUT'] 	= 'kinerja/pegawai/get_data';
$route['api/kinerja/pegawai/simpan']['PATCH'] 	= 'kinerja/pegawai/insert';
$route['api/kinerja/pegawai/edit']['PUT'] 		= 'kinerja/pegawai/edit';
$route['api/kinerja/pegawai/update']['PATCH'] 	= 'kinerja/pegawai/update';
$route['api/kinerja/pegawai/hapus']['DELETE'] 	= 'kinerja/pegawai/delete';
$route['api/kinerja/pegawai/periode']['PUT'] 	= 'kinerja/pegawai/get_periode';
$route['api/kinerja/pegawai/skp_peg']['PUT'] 	= 'kinerja/pegawai/get_skp_peg';
$route['api/kinerja/pegawai/rincian']['PUT'] 	= 'kinerja/pegawai/get_rincian_ind';
$route['api/kinerja/pegawai/data_periode']['PUT'] 	= 'kinerja/pegawai/get_data_periode';
$route['api/kinerja/pegawai/get_log_book']['PUT'] 	= 'kinerja/pegawai/get_log_book';
$route['api/kinerja/pegawai/get_wd']['PUT'] 	= 'kinerja/pegawai/get_wd';
$route['api/kinerja/pegawai/cek_data_pegawai']['PUT'] 	= 'kinerja/pegawai/cek_data_pegawai';
$route['api/kinerja/pegawai/cek_jenis_pegawai']['PUT'] 	= 'kinerja/pegawai/cek_jenis_pegawai';
$route['api/kinerja/pegawai/get_lpp']['PUT'] 	= 'kinerja/pegawai/data_lpp';
$route['api/kinerja/pegawai/list_rekan']['GET'] 	= 'kinerja/pegawai/get_list_rekan';
$route['api/kinerja/pegawai/list_pertanyaan']['GET'] 	= 'kinerja/pegawai/get_list_tanya';
$route['api/kinerja/pegawai/simpan_kuesioner']['PATCH'] 	= 'kinerja/pegawai/simpan_kuesioner';
$route['api/kinerja/pegawai/nilai_kuesioner']['PUT'] 	= 'kinerja/pegawai/get_nilai_kuesioner';

$route['api/kinerja/pegawai/edit_event_peserta']['PUT'] 	= 'kinerja/pegawai/get_event_pst';
$route['api/kinerja/pegawai/simpan_ajuan']['PATCH'] 	= 'kinerja/pegawai/insert_ajuan';
$route['api/kinerja/pegawai/cek_skp_pegawai']['PUT'] 	= 'kinerja/pegawai/cek_skp_pegawai';

//====================ijazah
$route['api/kinerja/pegawai/simpan_ijazah']['PATCH'] 	= 'kinerja/pegawai/insert_ijazah';
$route['api/kinerja/pegawai/get_ijazah']['PUT'] 		= 'kinerja/pegawai/data_ijazah';
$route['api/kinerja/pegawai/ijazah_edit']['PUT'] 		= 'kinerja/pegawai/edit_ijazah';
$route['api/kinerja/pegawai/update_ijazah']['PATCH'] 	= 'kinerja/pegawai/update_ijazah';
$route['api/kinerja/pegawai/hapus_ijazah']['DELETE'] 	= 'kinerja/pegawai/delete_ijazah';
//====================sertifikat
$route['api/kinerja/pegawai/get_sertifikat']['PUT'] 		= 'kinerja/pegawai/data_sertifikat';
$route['api/kinerja/pegawai/simpan_sertifikat']['PATCH'] 	= 'kinerja/pegawai/insert_sertifikat';
$route['api/kinerja/pegawai/edit_sert']['PUT'] 				= 'kinerja/pegawai/edit_data_sertifikat';
$route['api/kinerja/pegawai/update_sertifikat']['PATCH'] 	= 'kinerja/pegawai/update_sertifikat';
$route['api/kinerja/pegawai/hapus_sertifikat']['DELETE'] 	= 'kinerja/pegawai/delete_sertifikat';
//=====================sk
$route['api/kinerja/pegawai/get_unit']['PUT'] 		= 'kinerja/pegawai/get_unit';
$route['api/kinerja/pegawai/get_prodi']['PUT'] 		= 'kinerja/pegawai/get_prodi';
$route['api/kinerja/pegawai/simpan_sk']['PATCH'] 	= 'kinerja/pegawai/insert_sk';
$route['api/kinerja/pegawai/get_sk']['PUT'] 		= 'kinerja/pegawai/data_sk';
$route['api/kinerja/pegawai/simpan_sk_pegawai']['PATCH'] 	= 'kinerja/pegawai/insert_sk_pegawai';
$route['api/kinerja/pegawai/edit_sk']['PUT'] 		= 'kinerja/pegawai/edit_sk';
$route['api/kinerja/pegawai/update_sk_pegawai']['PATCH'] 	= 'kinerja/pegawai/update_sk_peg';
$route['api/kinerja/pegawai/hapus_sk']['DELETE'] 	= 'kinerja/pegawai/delete_sk';

//====================event
$route['api/kinerja/pegawai/get_event']['PUT'] 		= 'kinerja/pegawai/data_event';
$route['api/kinerja/pegawai/simpan_foto']['PATCH'] 	= 'kinerja/pegawai/insert_foto';

$route['api/kinerja/pegawai/edit_foto']['PUT'] 		= 'kinerja/pegawai/edit_ijazah';
$route['api/kinerja/pegawai/update_foto']['PATCH'] 	= 'kinerja/pegawai/update_ijazah';
$route['api/kinerja/pegawai/hapus_foto']['DELETE'] 	= 'kinerja/pegawai/delete_foto';


// SKP Satuan Kinerja Pegawai
$route['api/kinerja/skp/default']['PUT'] 	= 'kinerja/skp/get_data';
$route['api/kinerja/skp/simpan']['PATCH'] 	= 'kinerja/skp/insert';
$route['api/kinerja/skp/edit']['PUT'] 		= 'kinerja/skp/edit';
$route['api/kinerja/skp/update']['PATCH'] 	= 'kinerja/skp/update';
$route['api/kinerja/skp/hapus']['DELETE'] 	= 'kinerja/skp/delete';
$route['api/kinerja/skp/periode']['PUT'] 	= 'kinerja/skp/get_periode';
$route['api/kinerja/skp/ind_utama']['PUT'] 	= 'kinerja/skp/get_ind_utama';
$route['api/kinerja/skp/sub_ind']['PUT'] 	= 'kinerja/skp/get_sub_ind';
$route['api/kinerja/skp/ind_nilai']['PUT'] 	= 'kinerja/skp/get_ind_nilai';
$route['api/kinerja/skp/jenis_peg']['PUT'] 	= 'kinerja/skp/get_jenis_peg';
$route['api/kinerja/skp/get_data_skp']['PUT'] 	= 'kinerja/skp/get_skp_data';


// Penilaian Kependidikan
$route['api/kinerja/kependidikan/pertanyaan']['GET'] 	= 'kinerja/kependidikan/penilaian_diri/get_data';
$route['api/kinerja/kependidikan/pertanyaan/periode']['GET'] 	= 'kinerja/kependidikan/penilaian_diri/get_periode';
$route['api/kinerja/kependidikan/pertanyaan/simpan']['GET'] 	= 'kinerja/kependidikan/penilaian_diri/get_data';




// Validasi Log
$route['api/kinerja/val_log/p_aktif']['PUT'] 	= 'kinerja/validasi_log/periode_aktif';
$route['api/kinerja/val_log/p_log']['PUT'] 		= 'kinerja/validasi_log/periode_log';
$route['api/kinerja/val_log/log_pegawai']['PUT']= 'kinerja/validasi_log/log_pegawai';
$route['api/kinerja/val_log/data_pl']['PUT'] 	= 'kinerja/validasi_log/data_periode_log';
$route['api/kinerja/val_log/simpan']['PATCH'] 	= 'kinerja/validasi_log/insert';
$route['api/kinerja/val_log/hapus']['DELETE'] 	= 'kinerja/validasi_log/delete';

// Validasi SKP
$route['api/kinerja/val_skp/p_aktif']['PUT'] 	= 'kinerja/validasi_skp/periode_aktif';
$route['api/kinerja/val_skp/p_log']['PUT'] 		= 'kinerja/validasi_skp/periode_log';
$route['api/kinerja/val_skp/log_pegawai']['PUT']= 'kinerja/validasi_skp/log_pegawai';
$route['api/kinerja/val_skp/data_pl']['PUT'] 	= 'kinerja/validasi_skp/data_periode_log';
$route['api/kinerja/val_skp/simpan']['PATCH'] 	= 'kinerja/validasi_skp/insert';
$route['api/kinerja/val_skp/hapus']['DELETE'] 	= 'kinerja/validasi_skp/delete';
$route['api/kinerja/val_skp/list']['GET'] 		= 'kinerja/validasi_skp/get_list';
$route['api/kinerja/val_skp/skp_pegawai']['GET']= 'kinerja/validasi_skp/get_skp';
$route['api/kinerja/val_skp/update_validasi']['PATCH'] 	= 'kinerja/validasi_skp/update';
$route['api/kinerja/val_skp/update_revisi']['DELETE'] 	= 'kinerja/validasi_skp/revisi';
$route['api/kinerja/val_skp/get_skp_valid']['GET'] 	= 'kinerja/validasi_skp/get_skp_valid';

// LPP
$route['api/kinerja/lpp/default']['PUT'] 	= 'kinerja/lpp/get_data';
$route['api/kinerja/lpp/simpan']['PATCH'] 	= 'kinerja/lpp/insert';
$route['api/kinerja/lpp/edit']['PUT'] 		= 'kinerja/lpp/edit';
$route['api/kinerja/lpp/update']['PATCH'] 	= 'kinerja/lpp/update';
$route['api/kinerja/lpp/hapus']['DELETE'] 	= 'kinerja/lpp/delete';

// Setup event
$route['api/kinerja/setup/event/default']['PUT'] 	= 'kinerja/setup/event/get_data';
$route['api/kinerja/setup/event/simpan']['PATCH'] 	= 'kinerja/setup/event/insert';
$route['api/kinerja/setup/event/edit']['PUT'] 		= 'kinerja/setup/event/edit';
$route['api/kinerja/setup/event/update']['PATCH'] 	= 'kinerja/setup/event/update';
$route['api/kinerja/setup/event/hapus']['DELETE'] 	= 'kinerja/setup/event/delete';

// Setup event
$route['api/kinerja/setup/rekap_tendik/default']['PUT'] = 'kinerja/setup/rekap_tendik/get_data';

// perorangan

// =================================== SISTEM LLDIKTI ============================================
// ajuan jafa
$route['api/lldikti/jafa/default']['PUT'] 	= 'dikti/jafa/get_data';
$route['api/lldikti/jafa/get_edit']['PUT'] 	= 'dikti/jafa/edit';
$route['api/lldikti/jafa/get_dosen']['PUT'] = 'dikti/jafa/dosen_usm';
$route['api/lldikti/jafa/get_jafa']['PUT'] 	= 'dikti/jafa/jabfung';
$route['api/lldikti/jafa/get_jenis']['PUT'] = 'dikti/jafa/jenis';
$route['api/lldikti/jafa/get_status']['PUT'] = 'dikti/jafa/status';
$route['api/lldikti/jafa/update']['PATCH'] 	= 'dikti/jafa/update';
$route['api/lldikti/jafa/detail']['PUT'] 	= 'dikti/jafa/detail';
$route['api/lldikti/jafa/simpan']['PUT'] 	= 'dikti/jafa/insert';
$route['api/lldikti/jafa/update']['PATCH'] 	= 'dikti/jafa/update';

// ajuan serdos
$route['api/lldikti/serdos/default']['PUT'] 	= 'dikti/serdos/get_data';
$route['api/lldikti/serdos/simpan']['POST'] 	= 'dikti/serdos/insert';
$route['api/lldikti/serdos/edit']['PUT'] 		= 'dikti/serdos/edit';
$route['api/lldikti/serdos/update']['PATCH'] 	= 'dikti/serdos/update';


$route['api/kinerja/setup/asesor/get_data']['PUT'] = 'kinerja/setup/asesor/get_data';
$route['api/kinerja/setup/asesor/insert']['POST'] = 'kinerja/setup/asesor/insert'; 
$route['api/kinerja/setup/asesor/delete']['DELETE'] = 'kinerja/setup/asesor/delete';