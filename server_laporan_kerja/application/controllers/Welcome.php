<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/welcome
     *    - or -
     *         http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        
        redirect("http://usm.ac.id");
        //  $this->load->view('welcome_message');
    }

    function tc(){ 
        $sql = "SELECT * FROM absesni_setingan_jam";
        $query = $this->db->query($sql, array());
        //$this->db_app->krs()->query();
        if ($query->num_rows() > 0) {
             $result = $query->result_array();
            $query->free_result();
                return $this->app->respons_data($result, 'Data berhasil diload', 200);
            } else {
                return $this->app->respons_data(array(), 'data gagal diload', 200);
            }
           
    }

    public function cek_token()
    {
        $td = $this->token->checkGlobalToken();
        return print_r($td);
    }
}
