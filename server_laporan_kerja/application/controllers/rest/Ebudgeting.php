<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ebudgeting extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        //PENAMBAHAN KHUSUS UNTUK RES KE APLIKASI LAIN PERLU CEK SSO TOKEN
        $this->load->model('rest/m_ebudgeting');
    }

    public function index()
    {
    }
    public function akses_struktural()
    {
        $this->app->cekRequest('PUT');
        $this->m_ebudgeting->akses_struktural();
    }
}

// $route['api/ebudgeting/struktural']['PUT'] = 'rest/ebudgeting/akses_struktural';