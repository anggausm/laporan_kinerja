<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Akademik extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        //PENAMBAHAN KHUSUS UNTUK RES KE APLIKASI LAIN PERLU CEK SSO TOKEN
        $this->load->model('rest/m_akademik');
    }

    public function index()
    {
    }
    public function akses_fakultas_tu()
    {
        $this->app->cekRequest('PUT');
        $this->m_akademik->akses_fakultas_tu();
    }
}
