<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Validasi_skp extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "da64af53-4eea-11ef-8a24-e6c3c46e6b60";
        $this->token->cek_akses_app($this->tkn);

        $this->load->model('kinerja/m_validasi_skp');
    }

    public function index()
    {

    }

    public function periode_aktif()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_validasi_skp->get_periode_aktif();
    }

    public function periode_skp()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_validasi_skp->get_periode_skp();
    }

    public function log_pegawai()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $data['logp']   = $this->m_validasi_skp->data_skp_pegawai();
        $data['vlog']   = $this->m_validasi_skp->data_skp_valid();
        return $this->app->respons_data($data, 'Data Berhasil Diload', 200);
    }

    public function data_periode_skp()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_validasi_skp->edit_periode_skp();
    }

    public function insert()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_validasi_skp->insert_data();
    }

    public function update()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_validasi_skp->update_data();
    }

    public function revisi()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_validasi_skp->update_revisi();
    }
    

    public function delete()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_validasi_skp->delete_data();
    }

    public function get_list()
    {
        $this->app->cekRequest('GET');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_validasi_skp->get_list();
    }

    public function get_skp()
    {
        $this->app->cekRequest('GET');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_validasi_skp->get_skp();
    }

     public function get_skp_valid()
    {
        $this->app->cekRequest('GET');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_validasi_skp->get_skp_valid();
    }

    

    
}