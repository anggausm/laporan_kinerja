<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Test extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "04de8cf2-5561-11ea-8ebb-1cb72c27dd68";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('apps/m_test');
    }

    public function index()
    {
    }
    public function get_app_akses()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_test->get_app();

    }

}
