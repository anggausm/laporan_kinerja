<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tendik extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "34f96dc1-8449-11ea-b58e-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('biodata/m_tendik');
    }

    public function index()
    {
    }
    public function get_biodata($user_key = '')
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_tendik->get_biodata();
    }

    public function edit($user_key = '')
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_tendik->edit();
    }

    public function update($user_key = '')
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_tendik->update();
    }
}