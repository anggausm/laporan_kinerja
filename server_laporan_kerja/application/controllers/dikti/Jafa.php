<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Jafa extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "9dc639fc-b528-11ea-95bf-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('dikti/m_jafa');
    }

    public function index()
    {
    }
    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_jafa->default();
    }

    public function dosen_usm()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_jafa->dosen_usm();
    }

    public function jabfung()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_jafa->jabfung();
    }

    public function insert()
    {
        $this->app->cekRequest('POST');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_jafa->insert();
    }

    // public function histori_bulanan()
    // {
    //     $this->app->cekRequest('PUT');
    //     $this->app->cek_akses($this->menu_key, 'r');
    //     $this->m_histori->histori_bulanan();
    // }

}
