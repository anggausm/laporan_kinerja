<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class All_posting extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn 		= $this->token->checkGlobalToken();
        $this->menu_key = "34f96dc1-8449-11ea-b58e-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);

        $this->load->model('transport/m_allposting');
    }

    public function index()
    {

    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_allposting->get_data();
    }

    public function cek_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_allposting->cek_data();
    }

    public function insert()
    {
        $this->app->cekRequest('POST');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_allposting->insert();
    }
}