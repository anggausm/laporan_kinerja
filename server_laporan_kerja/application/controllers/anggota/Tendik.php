<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tendik extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "1dcb2eda-8483-11ea-b58e-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('anggota/m_tendik');
    }

    public function index()
    {
    }
    public function get_angggota()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_tendik->get_angggota();
    }
}
