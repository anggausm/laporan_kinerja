<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @package        CodeIgniter
 * @subpackage    Libraries
 * @category    Libraries
 * @author        wahyu budi santosa
 */
class CI_Log_app
{

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    public function index()
    {
        print "";
    }

    public function get_last($nm_tabel = '', $tbl_key = '', $id_key = '')
    {
        return $this->CI->m_log->get_last($nm_tabel, $tbl_key, $id_key);

    }
    public function get_data($nm_tabel = '', $tbl_key = '', $id_key = '')
    {
        return $this->CI->m_log->get_data($nm_tabel, $tbl_key, $id_key);
    }

    public function log_data($user = '', $nm_tabel = '', $tbl_key = '', $id_key = '', $act = '', $data = '', $keterangan = '', $tabel_master_log = '')
    {
        //$this->CI->load->model('root/m_log');
        return $this->CI->m_log->insert_log($user, $nm_tabel, $tbl_key, $id_key, $act, $data, $keterangan, $tabel_master_log);
    }
    public function log_insert($user = '', $nm_tabel = '', $tbl_key = '', $keterangan = '', $tabel_master_log = '')
    {
        return $this->CI->m_log->log_c($user, $nm_tabel, $tbl_key, $keterangan, $tabel_master_log);
    }
    public function log_update($user = '', $nm_tabel = '', $tbl_key = '', $id_key = '', $keterangan = '', $tabel_master_log = '')
    {
        return $this->CI->m_log->log_u($user, $nm_tabel, $tbl_key, $id_key, $keterangan, $tabel_master_log);
    }
    public function log_d($user = '', $nm_tabel = '', $tbl_key = '', $id_key = '', $data = '', $keterangan = '', $tabel_master_log = '')
    {
        return $this->CI->m_log->log_d($user, $nm_tabel, $tbl_key, $id_key, $data, $keterangan, $tabel_master_log);
    }

}
