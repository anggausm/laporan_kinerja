<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pimpinan_fakultas extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '6b21b124-884d-11ea-b58e-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();

    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['data'] = $this->_restruktur_pimpinan_fakultas();

        $this->tema->backend('restruktur/pimpinan_fakultas/index', $data);
    }
    public function edit($id_sub_struktural = '')
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'u');
        $data['add_js'] = array('<script src="' . base_url() . 'assets/backend/html/scripts/pimpinan_fakultas.js?version=' . time() . '"></script>');
        $edit = $this->_edit(array('id_sub_struktural' => $id_sub_struktural));
        $data['struktural'] = $edit['struktural'];
        $data['anggota'] = $edit['anggota'];
        $data['doskar_usm'] = $this->_doskar_usm();
        if ($id_sub_struktural == '54' || $id_sub_struktural == '55' || $id_sub_struktural == '56' ||
            $id_sub_struktural == '57' || $id_sub_struktural == '58' || $id_sub_struktural == '59') {
            $this->tema->backend('restruktur/pimpinan_fakultas/edit', $data);
        } else {
            $data['fakultas'] = $this->_fakultas();
            $data['program_studi'] = $this->_program_studi();
            $this->tema->backend('restruktur/pimpinan_fakultas/edit2', $data);
        }
    }
    public function simpan()
    {
        $post = $this->input->post();
        $data = array(
            'user_key' => $post['doskar_usm'],
            'id_sub_struktural' => $post['id_sub_struktural'],
            'fakultas' => $post['fakultas'],
        );
        if (!empty($post['program_studi'])) {
            $data['kode_khusus'] = $post['program_studi'];
        }
        $aksi = $this->_simpan($data);
        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] == false) {
            $this->session->set_flashdata('type', 'danger');
        } else {
            $this->session->set_flashdata('type', 'success');
        }
        return redirect(site_url("restruktur/pimpinan_fakultas/edit/$post[id_sub_struktural]"), 'refresh');
    }
    public function update()
    {
        $post = $this->input->post();
        $data = array(
            'user_key' => $post['doskar_usm'],
            'id_angota_struktural' => $post['id_angota_struktural'],
            'id_sub_struktural' => $post['id_sub_struktural'],
        );
        $aksi = $this->_update($data);
        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] == false) {
            $this->session->set_flashdata('type', 'danger');
        } else {
            $this->session->set_flashdata('type', 'success');
        }
        return redirect(site_url("restruktur/pimpinan_fakultas/edit/$post[id_sub_struktural]"), 'refresh');
    }
    public function delete($id_sub_struktural, $id_angota_struktural)
    {
        $data = array(
            'id_angota_struktural' => $id_angota_struktural,
        );
        $aksi = $this->_delete($data);
        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] == false) {
            $this->session->set_flashdata('type', 'danger');
        } else {
            $this->session->set_flashdata('type', 'success');
        }
        return redirect(site_url("restruktur/pimpinan_fakultas/edit/$id_sub_struktural"), 'refresh');
    }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    public function _restruktur_pimpinan_fakultas($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/restruktur/pimpinan_fakultas', 'base_token', $data), true)['result'];
    }
    public function _edit($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/restruktur/pimpinan_fakultas/edit', 'base_token', $data), true)['result'];
    }
    public function _update($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/restruktur/pimpinan_fakultas/update', 'base_token', $data), true);
    }
    public function _simpan($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('POST', $this->base_url . 'api/restruktur/pimpinan_fakultas/simpan', 'base_token', $data), true);
    }
    public function _delete($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/restruktur/pimpinan_fakultas/delete', 'base_token', $data), true);
    }
    public function _fakultas($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/restruktur/pimpinan_fakultas/fakultas', 'base_token', $data), true)['result'];
    }
    public function _program_studi($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/restruktur/pimpinan_fakultas/program_studi', 'base_token', $data), true)['result'];
    }
    public function _doskar_usm($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/restruktur/tendik/doskar_usm', 'base_token', $data), true)['result'];
    }
}
