<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tendik extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '845f00eb-884d-11ea-b58e-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();

    }

//     public function ip_loc()
    //     {
    // // PHP code to obtain country, city,
    //         // continent, etc using IP Address

//         $ip = $_SERVER['HTTP_X_REAL_IP'];

// // Use JSON encoded string and converts
    //         // it into a PHP variable
    //         $iptolocation = json_decode($this->curl->request('GET', "http://ip-api.com/json/$ip", 'base_token', ""), true);

//         return print_r($iptolocation);

//         echo 'Country Name: ' . $ipdat->geoplugin_countryName . "\n";
    //         echo 'City Name: ' . $ipdat->geoplugin_city . "\n";
    //         echo 'Continent Name: ' . $ipdat->geoplugin_continentName . "\n";
    //         echo 'Latitude: ' . $ipdat->geoplugin_latitude . "\n";
    //         echo 'Longitude: ' . $ipdat->geoplugin_longitude . "\n";
    //         echo 'Currency Symbol: ' . $ipdat->geoplugin_currencySymbol . "\n";
    //         echo 'Currency Code: ' . $ipdat->geoplugin_currencyCode . "\n";
    //         echo 'Timezone: ' . $ipdat->geoplugin_timezone;
    //     }

    public function get_ip()
    {
        $ip = $_SERVER['HTTP_X_REAL_IP'];
        $iptolocation = json_decode($this->curl->request('GET', "http://ip-api.com/json/$ip", 'base_token', ""), true);
        if ($iptolocation['isp'] == "Universitas Semarang") {
            $kordinat = array("isp" => "Universitas Semarang", "latitude" => "-6.9822203", "longitude" => "110.4520017");
        } else {
            $kordinat = array("isp" => "", "latitude" => "", "longitude" => "");
        }
        return $kordinat;
    }
    public function getOS()
    {
        $user_agent = $_SERVER["HTTP_USER_AGENT"];
        $os_platform = "Unknown OS Platform";
        $os_array = array(
            '/windows nt 10/i' => 'Windows 10',
            '/windows nt 6.3/i' => 'Windows 8.1',
            '/windows nt 6.2/i' => 'Windows 8',
            '/windows nt 6.1/i' => 'Windows 7',
            '/windows nt 6.0/i' => 'Windows Vista',
            '/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
            '/windows nt 5.1/i' => 'Windows XP',
            '/windows xp/i' => 'Windows XP',
            '/windows nt 5.0/i' => 'Windows 2000',
            '/windows me/i' => 'Windows ME',
            '/win98/i' => 'Windows 98',
            '/win95/i' => 'Windows 95',
            '/win16/i' => 'Windows 3.11',
            '/macintosh|mac os x/i' => 'Mac OS X',
            '/mac_powerpc/i' => 'Mac OS 9',
            '/linux/i' => 'Linux',
            '/ubuntu/i' => 'Ubuntu',
            '/iphone/i' => 'iPhone',
            '/ipod/i' => 'iPod',
            '/ipad/i' => 'iPad',
            '/android/i' => 'Android',
            '/blackberry/i' => 'BlackBerry',
            '/webos/i' => 'Mobile',
        );
        foreach ($os_array as $regex => $value) {
            if (preg_match($regex, $user_agent)) {
                $os_platform = $value;
            }
        }
        return $os_platform;
    }

    public function get_wfh($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/wfh/get_wfh', 'base_token', $data), true);
    }
    public function cek_presensi($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/wfh/cek_presensi', 'base_token', $data), true);
    }
    public function mulai_kerja($id_presensi = '')
    {
        $data = array();
        if (empty($id_presensi)) {
            redirect(base_url() . 'absensi/wfh/');
        }
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'c');
        $os = $this->getOS();

        if ($os == 'iPhone') {
            $notifikasi = "0";
        } elseif ($os == 'Android') {
            $notifikasi = "0";
        } elseif ($os == 'BlackBerry') {
            $notifikasi = "0";
        } elseif ($os == 'iPhone') {
            $notifikasi = "0";
        } elseif ($os == 'iPad') {
            $notifikasi = "1";
        } else {
            $notifikasi = "1";
        }
        $ip = $this->get_ip();

        if ($ip['isp'] == "Universitas Semarang") {
            $data['latitude'] = $ip['latitude'];
            $data['longitude'] = $ip['longitude'];
        } else {
            $data['latitude'] = "";
            $data['longitude'] = "";
        }
        $data['notifikasi'] = $notifikasi;

        $data['wfh'] = $this->get_wfh(array('id_absen' => $id_presensi));
        $this->tema->backend('restruktur/tendik/wfh/mulai_kerja', $data);
    }

    public function wfh()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['tgl'] = date('d-m-Y');
        $data['hari'] = $this->namahari();
        $data['jam'] = date("H:i:s");
        $os = $this->getOS();

        if ($os == 'iPhone') {
            $notifikasi = "0";
        } elseif ($os == 'Android') {
            $notifikasi = "0";
        } elseif ($os == 'BlackBerry') {
            $notifikasi = "0";
        } elseif ($os == 'iPhone') {
            $notifikasi = "0";
        } elseif ($os == 'iPad') {
            $notifikasi = "1";
        } else {
            $notifikasi = "1";
        }
        $ip = $this->get_ip();

        if ($ip['isp'] == "Universitas Semarang") {
            $data['latitude'] = $ip['latitude'];
            $data['longitude'] = $ip['longitude'];
        } else {
            $data['latitude'] = "";
            $data['longitude'] = "";
        }
        $data['notifikasi'] = $notifikasi;

        $cek_presensi = $this->cek_presensi();
        if (!empty($cek_presensi['result'])) {
            redirect(base_url() . 'restruktur/tendik/mulai_kerja/' . $cek_presensi['result']['id_absen']);
        }

        $this->tema->backend('restruktur/tendik/wfh/index', $data);
    }
    public function namahari()
    {
        $tgl = date('d');
        $bln = date('m');
        $thn = date('Y');
        $info = date('w', mktime(0, 0, 0, $bln, $tgl, $thn));
        if ($info == '0') {
            $hari = "Minggu";
        } elseif ($info == '1') {
            $hari = "Senin";
        } elseif ($info == '2') {
            $hari = "Selasa";
        } elseif ($info == '3') {
            $hari = "Rabu";
        } elseif ($info == '4') {
            $hari = "Kamis";
        } elseif ($info == '5') {
            $hari = "Jumat";
        } elseif ($info == '6') {
            $hari = "Sabtu";
        } elseif ($info == '7') {

        }
        return $hari;
    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['data'] = $this->_restruktur_tendik();

        $this->tema->backend('restruktur/tendik/index', $data);
    }

    public function edit($id_sub_struktural = '')
    {
        $data = array();
        $data['add_js'] = array('<script src="' . base_url() . 'assets/backend/html/scripts/restruktur_tendik.js?version=' . time() . '"></script>');
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'u');
        $edit = $this->_edit(array('id_sub_struktural' => $id_sub_struktural));
        $data['struktural'] = $edit['struktural'];
        $data['anggota'] = $edit['anggota'];
        $data['doskar_usm'] = $this->_doskar_usm();
        $this->tema->backend('restruktur/tendik/edit', $data);
    }
    public function simpan()
    {
        $post = $this->input->post();
        $data = array(
            'user_key' => $post['doskar_usm'],
            'id_sub_struktural' => $post['id_sub_struktural'],
        );
        $aksi = $this->_simpan($data);
        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] == false) {
            $this->session->set_flashdata('type', 'danger');
        } else {
            $this->session->set_flashdata('type', 'success');
        }
        return redirect(site_url("restruktur/tendik/edit/$post[id_sub_struktural]"), 'refresh');
    }

    public function delete($id_sub_struktural, $id_angota_struktural)
    {
        $data = array(
            'id_angota_struktural' => $id_angota_struktural,
        );
        $aksi = $this->_delete($data);
        if ($aksi['result'] == false) {
            $this->session->set_flashdata('type', 'danger');
        } else {
            $this->session->set_flashdata('type', 'success');
        }
        $this->session->set_flashdata('pesan', $aksi['message']);
        return redirect(site_url("restruktur/tendik/edit/$id_sub_struktural"), 'refresh');
    }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/

    public function _restruktur_tendik($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/restruktur/tendik', 'base_token', $data), true)['result'];
    }

    public function _edit($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/restruktur/tendik/edit', 'base_token', $data), true)['result'];
    }
    public function _simpan($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('POST', $this->base_url . 'api/restruktur/tendik/simpan', 'base_token', $data), true);
    }
    public function _delete($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/restruktur/tendik/delete', 'base_token', $data), true);
    }

    public function _doskar_usm($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/restruktur/tendik/doskar_usm', 'base_token', $data), true)['result'];
    }
}
