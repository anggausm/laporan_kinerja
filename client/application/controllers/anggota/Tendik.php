<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tendik extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '1dcb2eda-8483-11ea-b58e-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();

    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['rs_data'] = $this->get_data();
        $this->tema->backend('anggota/tendik/index', $data);
    }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/

    public function get_data($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/angota/tendik', 'base_token', $data), true);
    }

}
