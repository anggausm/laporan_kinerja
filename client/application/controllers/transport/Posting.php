<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Posting extends CI_Controller
{
    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '662c130b-71c2-11ea-8d6b-1cb72c27dd68';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        // $this->load->helper('download');
    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');

        $data['get_default'] = $this->get_default();
        $this->tema->backend('transport/posting/index', $data);
    }

    public function simpan()
    {
        // $data = $this->get_default();
        // $dt   = $data['result']['rs'];

        // print_r($dt);

        $aksi = $this->_simpan();
        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] == false) {
            $this->session->set_flashdata('type', 'danger');
        } else {
            $this->session->set_flashdata('type', 'success');
        }
        return redirect(site_url("transport/posting/index"), 'refresh');
    }




    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    public function get_default($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/transport/posting/default', 'base_token', $data), true);
    }

    public function _simpan($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('POST', $this->base_url . 'api/transport/posting/simpan', 'base_token', $data), true);
    }
}