<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rekap_kinerja extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = 'f0916575-3f5b-11ef-8a24-e6c3c46e6b60';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        $this->view = 'backend/pak/layanan/rekap_kinerja/';
        $this->link = 'layanan_pak/rekap_kinerja/';
    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $this->tema->backend($this->view . 'index', $data);
    }

    public function biodata()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $this->tema->backend($this->view . 'biodata', $data);
    }

    public function pendidikan()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $this->tema->backend($this->view . 'pendidikan', $data);
    }

    public function pendidikan_detail()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $this->tema->backend($this->view . 'pendidikan_detail', $data);
    }

    public function penelitian()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $this->tema->backend($this->view . 'penelitian', $data);
    }

    public function penelitian_detail()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $this->tema->backend($this->view . 'penelitian_detail', $data);
    }

    public function pengabdian()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $this->tema->backend($this->view . 'pengabdian', $data);
    }

    public function penunjang()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $this->tema->backend($this->view . 'penunjang', $data);
    }

    public function core_values()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $this->tema->backend($this->view . 'core_values', $data);
    }

    public function tambahan()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $this->tema->backend($this->view . 'tambahan', $data);
    }
}
