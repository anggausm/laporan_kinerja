<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ajuan_jafa extends CI_Controller
{
    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '9dc639fc-b528-11ea-95bf-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
    }

    public function index()
    {
        $data               = array();
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'r');
        $data['get_data']   = $this->get_data();
        $this->tema->backend('sistem_lldikti/jafa/index', $data);
    }

    public function tambah()
    {
        $data               = array();
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'r');
        $data['dosen']      = $this->get_dosen();
        $data['jafa']       = $this->get_jafa();
        $this->tema->backend('sistem_lldikti/jafa/tambah', $data);
    }

    public function simpan()
    {
        $input = $this->input->post();

        // if (empty($input)) {
        //     redirect(base_url() . 'sistem_lldikti/jafa');
        // }

        $data               = array();
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'c');
        $data['data_jf']    = array('user_key'      => $input['dosen'], 
                                    'bidang_ilmu'   => $input['bidang'],
                                    'id_jafa'       => $input['jafa']);
        // print_r($data);
        print_r("aaaaaaaaaaa". $data);
        
        // $update_notif       = $this->simpan($array);
        // $this->session->set_flashdata('pesan', $update_notif['message']);
        // redirect(base_url() . 'sistem_lldikti/jafa/index');
    }
 /*
    public function detail()
    {
        $data = array();
        if (empty($this->input->post())) {
            redirect(base_url() . 'sistem_lldikti/jafa');
        }
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'r');
        $data['detail']     = $this->get_detail(array('id_ajuan' => dec_data($this->input->post('id'))));
        $this->tema->backend('sistem_lldikti/jafa/detail', $data);
    }
*/
    // =============================================== Get Data   ============================================================= 

    
    public function get_dosen($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/lldikti/jafa/get_dosen', 'base_token', $data), true);
    }
    
    public function get_jafa($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/lldikti/jafa/get_jafa', 'base_token', $data), true);
    }

    public function get_data($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/lldikti/jafa/default', 'base_token', $data), true);
    }
/* 
    public function simpan($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('POST', $this->base_url . 'api/lldikti/jafa/simpan', 'base_token', $data), true);
    }

    public function edit($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/lldikti/jafa/edit', 'base_token', $data), true);
    }
    
    public function update($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/lldikti/jafa/update', 'base_token', $data), true);
    }
*/
}
