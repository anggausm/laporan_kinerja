<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Versi extends CI_Controller
{
//construktor
    public function __construct()
    {
        parent::__construct();
        $this->base_url = $this->app->base_server();
        error_reporting("ALL");

    }
    public function index()
    {

    }

    public function get_versi()
    {
        $this->app->cekRequest('PUT');
        $token = "";
        $data = "";
        $rs = json_decode($this->curl->request_manual_tkn('PUT', $this->base_url . 'api/versi', $token, $data), true);
        return $this->app->respons_data($rs['result'], "versi aplikasi presensi online", '200');
    }

}
