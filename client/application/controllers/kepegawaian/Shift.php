<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Shift extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '8cce1c67-9591-11ea-aca3-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();

    }

    public function index()
    {
        // $data['rs_data'] = $this->get_biodata();
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['shift'] = $this->get_list();
        $this->tema->backend('kepegawaian/shift/index', $data);
    }

    public function get_list($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/shift/list', 'base_token', $data), true);
    }

    public function edit($user_key = '') {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $user = json_encode(array('user_key' => dec_data($user_key)));
        $data['rs_data'] = json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/shift/edit', 'base_token', $user), true);
        $this->tema->backend('kepegawaian/shift/edit', $data);
    }

    public function update() {
        $data = array();
        $input = $this->input->post();
        // $file = $_FILES['nama_file'];

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'u');

        if (empty($input)) {
            redirect(base_url() . 'kepegawaian/shift/index');
        }
        $array = array('shift_finjer' => $input['shift_finjer'], 
                        'user_key' => $input['user_key']
                    );
        $update_shift = $this->update_shift($array);
        $this->session->set_flashdata('pesan', $update_shift['message']);
        redirect(base_url() . 'kepegawaian/shift/index');
    }

    public function update_shift($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kpg/shift/update', 'base_token', $data), true);
    }
}