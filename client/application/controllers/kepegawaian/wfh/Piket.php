<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Piket extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '2ae2ff64-98a9-11ea-aca3-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        $this->load->helper('download');

    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['periode'] = $this->periode();
        $data['level'] = $this->level();
        $this->tema->backend('kepegawaian/wfh/piket/index', $data);

    }

    public function unit($id_piket_wfh = '', $key = '')
    {
        $input = $this->input->post();
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'c');
        if (empty($input)) {
            // $this->session->set_flashdata('pesan', "Maaf proses belum bisa kami lanjutkan.");
            // redirect(base_url() . 'kepegawaian/wfh/piket');
            $arr = array("id_piket_wfh" => dec_data($id_piket_wfh), "key" => dec_data($key));
            $data['id_piket_wfh'] = dec_data($id_piket_wfh);
            $data['key'] = dec_data($key);
        } else {
            $arr = array("id_piket_wfh" => $input["id_piket_wfh"], "key" => $input["key"]);
            $data['id_piket_wfh'] = $input['id_piket_wfh'];
            $data['key'] = $input['key'];
        }

        $data['periode'] = $this->periode();
        $data['level'] = $this->level();
        $data['level_list'] = $this->level_list($arr);
        $this->tema->backend('kepegawaian/wfh/piket/unit', $data);
    }
    public function set_jadwal($id_struktural = '', $key = "", $id_piket_wfh = '')
    {
        if (empty($id_struktural)) {
            $this->session->set_flashdata('pesan', "Maaf proses belum bisa kami lanjutkan.");
            redirect(base_url() . 'kepegawaian/wfh/piket');
        }

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'c');
        $data['id_piket_wfh'] = dec_data($id_piket_wfh);
        $data['key'] = dec_data($key);
        $data['id_struktural'] = dec_data($id_struktural);
        $data['list_anggota'] = $this->anggota(array("id_piket_wfh" => dec_data($id_piket_wfh), "key" => dec_data($key), "id_struktural" => dec_data($id_struktural)));
        $this->tema->backend('kepegawaian/wfh/piket/set_jadwal/index', $data);
    }

    public function set_jadwal_piket($id_struktural = '', $key = "", $id_piket_wfh = '', $user_key = "", $id_sub_struktural = "")
    {
        if (empty($id_struktural)) {
            $this->session->set_flashdata('pesan', "Maaf proses belum bisa kami lanjutkan.");
            redirect(base_url() . 'kepegawaian/wfh/piket');
        }

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'c');
        $data['id_piket_wfh'] = dec_data($id_piket_wfh);
        $data['key'] = dec_data($key);
        $data['id_struktural'] = dec_data($id_struktural);
        $data['user_key'] = dec_data($user_key);
        $data['id_sub_struktural'] = dec_data($id_sub_struktural);
        $data['jadwal_piket'] = $this->jadwal_piket(array("id_piket_wfh" => dec_data($id_piket_wfh), "key" => dec_data($key), "id_struktural" => dec_data($id_struktural), "user_key" => dec_data($user_key), "id_sub_struktural" => dec_data($id_sub_struktural)));

        $this->tema->backend('kepegawaian/wfh/piket/set_jadwal/set_jadwal_piket', $data);

    }
    public function simpan_jadwal_piket($id_struktural = "", $key = "", $id_piket_wfh = '', $user_key = "", $id_sub_struktural = "", $tgl = "")
    {
        if (empty($id_struktural)) {
            $this->session->set_flashdata('pesan', "Maaf proses belum bisa kami lanjutkan.");
            redirect(base_url() . 'kepegawaian/wfh/piket');
        }

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'u');
        $data['id_piket_wfh'] = dec_data($id_piket_wfh);
        $data['key'] = dec_data($key);
        $data['id_struktural'] = dec_data($id_struktural);
        $data['user_key'] = dec_data($user_key);
        $data['id_sub_struktural'] = dec_data($id_sub_struktural);
        $arr = array("id_piket_wfh" => dec_data($id_piket_wfh), "key" => dec_data($key), "id_struktural" => dec_data($id_struktural), "user_key" => dec_data($user_key), "id_sub_struktural" => dec_data($id_sub_struktural), "tanggal" => dec_data($tgl));
        $simpan_jadwal = $this->simpan_jadwal($arr);

        $this->session->set_flashdata('pesan', $simpan_jadwal['message']);
        redirect(base_url() . 'kepegawaian/wfh/piket/set_jadwal_piket/' . $id_struktural . '/' . $key . '/' . $id_piket_wfh . '/' . $user_key . '/' . $id_sub_struktural);

    }

    public function delete_jadwal_piket($id_struktural = '', $key = "", $id_piket_wfh = '', $user_key = "", $id_sub_struktural = "", $tgl = "")
    {
        if (empty($id_struktural)) {
            $this->session->set_flashdata('pesan', "Maaf proses belum bisa kami lanjutkan.");
            redirect(base_url() . 'kepegawaian/wfh/piket');
        }

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'c');
        $data['id_piket_wfh'] = dec_data($id_piket_wfh);
        $data['key'] = dec_data($key);
        $data['id_struktural'] = dec_data($id_struktural);
        $data['user_key'] = dec_data($user_key);
        $data['id_sub_struktural'] = dec_data($id_sub_struktural);
        $arr = array("id_piket_wfh" => dec_data($id_piket_wfh), "key" => dec_data($key), "id_struktural" => dec_data($id_struktural), "user_key" => dec_data($user_key), "id_sub_struktural" => dec_data($id_sub_struktural), "tanggal" => dec_data($tgl));

        $delete_jadwal = $this->delete_jadwal($arr);

        $this->session->set_flashdata('pesan', $delete_jadwal['message']);
        redirect(base_url() . 'kepegawaian/wfh/piket/set_jadwal_piket/' . $id_struktural . '/' . $key . '/' . $id_piket_wfh . '/' . $user_key . '/' . $id_sub_struktural);
    }
    /*
     * transaksi
    $route['api/kpg/wfh/piket/simpan_jadwal']['PUT'] = 'kepegawaian/wfh/piket/simpan_jadwal_piket';
    $route['api/kpg/wfh/piket/delete_jadwal
     */

    public function periode($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/wfh/piket/periode', 'base_token', $data), true);
    }
    public function level($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/wfh/piket/level', 'base_token', $data), true);
    }
    public function level_list($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/wfh/piket/level_list', 'base_token', $data), true);
    }
    public function anggota($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/wfh/piket/anggota', 'base_token', $data), true);
    }

    public function jadwal_piket($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/wfh/piket/jadwal_piket', 'base_token', $data), true);
    }
    public function simpan_jadwal($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kpg/wfh/piket/simpan_jadwal', 'base_token', $data), true);
    }
    public function delete_jadwal($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kpg/wfh/piket/delete_jadwal', 'base_token', $data), true);
    }

}
