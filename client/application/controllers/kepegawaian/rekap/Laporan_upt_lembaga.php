<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Laporan_upt_lembaga extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '36d4fcac-9582-11ea-aca3-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        $this->load->helper('download');

    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['fakultas'] = $this->unit_lembaga();
        $this->tema->backend('kepegawaian/rekap/lembaga/index', $data);
    }

    public function anggota($id = '')
    {
        if (empty($id)) {
            $this->session->set_flashdata('pesan', "Maaf proses belum bisa kami lanjutkan.");
            redirect(base_url() . 'kepegawaian/rekap/laporan_upt_lembaga');
        }
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['id_struktural'] = dec_data($id);
        $data['struktural'] = $this->list_anggota(array("id_struktural" => dec_data($id)));
        $this->tema->backend('kepegawaian/rekap/lembaga/anggota', $data);

    }

    public function rincian_kerja($id = '', $user_key = '', $bulan = '')
    {
        if (empty($id)) {
            $this->session->set_flashdata('pesan', "Maaf proses belum bisa kami lanjutkan.");
            redirect(base_url() . 'kepegawaian/rekap/laporan_upt_lembaga');
        }
        if (empty($bulan)) {
            $bulan = date('Y-m');
        }
        $data['tahun'] = date('Y');
        $data['bulan'] = date('m');

        $data['id_struktural'] = dec_data($id);
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['pekerjaan'] = $this->presensi(array("user_key" => dec_data($user_key), "id_struktural" => dec_data($id), "bulan" => $bulan));
        $this->tema->backend('kepegawaian/rekap/lembaga/rincian_kerja', $data);

    }
    public function bulan()
    {
        $input = $this->input->post();
        $data['id_struktural'] = $input['id_struktural'];
        $bulan = $input['tahun'] . '-' . $input['bulan'];
        $data['tahun'] = $input['tahun'];
        $data['bulan'] = $input['bulan'];
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['pekerjaan'] = $this->presensi(array("user_key" => $input['user_key'], "id_struktural" => $input['id_struktural'], "bulan" => $bulan));
        $this->tema->backend('kepegawaian/rekap/lembaga/rincian_kerja', $data);

    }

    public function download_files($id = '')
    {
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $rs_data = $this->download(array("id_task" => $id));
        $nama_file = $rs_data['result']['dokumen'];
        $file = $rs_data['result']['jdl_file'];
        $server_file = $this->server_file . 'file/laporan_pekerjaan/' . $nama_file;
        $data = file_get_contents("$server_file"); // Read the file's contents
        $ext = explode('.', $nama_file);
        $name = date('Ymd') . '_' . $file . '.' . $ext[1];
        return force_download($name, $data);
    }
    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/

    public function unit_lembaga($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/lap/upt_lembaga/unit_lembaga', 'base_token', $data), true);
    }
    public function list_anggota($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/lap/upt_lembaga/list_anggota', 'base_token', $data), true);
    }
    public function presensi($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/lap/upt_lembaga/presensi', 'base_token', $data), true);
    }
    public function download($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/lap/upt_lembaga/download', 'base_token', $data), true);
    }

}
