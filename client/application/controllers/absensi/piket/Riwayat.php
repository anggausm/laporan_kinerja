<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Riwayat extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '2a1d4643-965e-11ea-aca3-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        $this->load->helper('download');

    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        if (!empty($this->input->post())) {
            $data['bulan'] = $this->input->post('bulan');
            $data['tahun'] = $this->input->post('tahun');
            $data['get_default'] = $this->histori_laporan($this->input->post());
        } else {
            $data['bulan'] = date('m');
            $data['tahun'] = date('Y');
            $data['get_default'] = $this->get_default();
        }
        $this->tema->backend('absensi/piket/riwayat/index', $data);
    }

    public function detail_task()
    {
        $data = array();
        if (empty($this->input->post())) {
            redirect(base_url() . 'absensi/piket/riwayat/detail_task');
        }
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['wfh'] = $this->get_wfh(array('id_absen' => dec_data($this->input->post('id_absen'))));
        $this->tema->backend('absensi/piket/riwayat/mulai_kerja', $data);
    }

    public function download_files($id = '')
    {
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $rs_data = $this->get_download(array("id_task" => $id));
        $nama_file = $rs_data['result']['dokumen'];
        $file = $rs_data['result']['jdl_file'];
        $server_file = $this->server_file . 'file/laporan_pekerjaan/' . $nama_file;
        $data = file_get_contents("$server_file"); // Read the file's contents
        $ext = explode('.', $nama_file);
        $name = date('Ymd') . '_' . $file . '.' . $ext[1];
        return force_download($name, $data);
    }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    public function get_default($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/piket/histori/default', 'base_token', $data), true);
    }
    //histori_laporan
    public function histori_laporan($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/piket/histori/histori_laporan', 'base_token', $data), true);
    }
    public function get_wfh($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/piket/get_wfh', 'base_token', $data), true);
    }

    public function get_download($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/piket/download', 'base_token', $data), true);
    }
}
