<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tendik extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '34f96dc1-8449-11ea-b58e-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();

    }

    public function index()
    {
        // $data['rs_data'] = $this->get_biodata();
        // var_dump($data);
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['rs_data'] = $this->get_biodata();
        $this->tema->backend('biodata/tendik/index', $data);
    }

    public function get_biodata($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/biodata/tendik', 'base_token', $data), true);
    }

    public function edit($array = '') {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['rs_data'] = $this->get_biodata();
        $this->tema->backend('biodata/tendik/edit', $data);
    }

    public function update() {
        $data = array();
        $input = $this->input->post();
        // $file = $_FILES['nama_file'];

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'u');

        if (empty($input)) {
            redirect(base_url() . 'biodata/tendik/index');
        }

        // if (!empty($file) and $file['size'] < 1000) {
        //     $this->session->set_flashdata('pesan', 'Maaf proses anda belum bisa kami lanjutkan.. Ukuran File tidak sesuai standar kami');
        //     redirect(base_url() . 'absensi/wfh');
        // };

        // $upload = json_decode($this->curl->upload($this->server_file . 'upload/laporan_pekerjaan', $file, 'sso_token'), 'TRUE');
        // if ($upload['status'] == '201') {
        //     $this->session->set_flashdata('pesan', $upload['message']);
        //     return redirect(base_url() . 'akd/dosen/presensi/kuliah/');
        // }
        $array = array('nama_doskar' => $input['nama_doskar'], 
                        'nidn' => $input['nidn'], 
                        'no_ktp' => $input['no_ktp'], 
                        'tgl_lhr' => $input['tgl_lhr'],
                        'tmpt_lhr' => $input['tmpt_lhr'],
                        'agama' => $input['agama'],
                        'jns_kelamin' => $input['jns_kelamin'],
                        'gol_darah' => $input['gol_darah'],
                        'handphone' => $input['handphone'],
                        'email' => $input['email'],
                        'alamat' => $input['alamat'],
                        'kota' => $input['kota'],
                        'kode_pos' => $input['kode_pos']
                    );
        $update_biodata = $this->update_biodata($array);
        $this->session->set_flashdata('pesan', $update_biodata['message']);
        redirect(base_url() . 'biodata/tendik/index');
    }

    public function update_biodata($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/biodata/tendik/update', 'base_token', $data), true);
    }
}