<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Skp extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = 'acf4ad3e-45a5-11ef-8a24-e6c3c46e6b600';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        $this->load->helper('download');
        $this->judul     = 'SKP';
        $this->link = 'kinerja/skp';

    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');

         $data['periode'] = $this->get_periode()['result'];
         $data['jenis_peg'] = $this->get_jenis_peg()['result'];
         $data['default'] = $this->get_default()['result'];

        $this->tema->backend('backend/kinerja/skp/index', $data);
    }

    public function data()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $input        = $this->input->post();

         $data['periode'] = $this->get_periode()['result'];
         $data['jenis_peg'] = $this->get_jenis_peg()['result'];
         $data['datas'] = $this->get_data_skp((array("jenis_peg" => $input['jenis_peg'], "periode" => $input['periode'])))['result'];

        $this->tema->backend('backend/kinerja/skp/data', $data);
    }

     public function tambah()
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['periode'] = $this->get_periode()['result'];
        $data['iu'] = $this->get_ind_utama()['result'];
        $data['sip'] = $this->get_sub_ind()['result'];
        $data['ip'] = $this->get_ind_penilaian()['result'];
        $data['jenis_peg'] = $this->get_jenis_peg()['result'];

        $this->tema->backend('backend/kinerja/skp/tambah', $data);
    }

    public function simpan()
    {
        $post = $this->input->post();
        $data = array(
            'periode' => $post['periode'],
            'jenis_pegawai' => $post['id_jp'],
            'id_indikator_utama' => $post['ind_utama'],
            'id_sub_indikator' => $post['sub_ind'],
            'id_ind_penilaian' => $post['ind_penilaian'],
            'rincian_indikator' => $post['rincian_ind'],
            'syarat' => $post['prasyarat'],
            'bobot' => $post['bobot'],
            'skor' => $post['skor'],
            'ket' => $post['keterangan']
        );

        $aksi = $this->_simpan($data);

        if ($aksi['result'] === false) {
            $this->session->set_flashdata('error', $aksi['message']);
            return redirect(base_url() . 'kinerja/skp/tambah');
        } else {
            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/skp/index');
        }
        
    }

     public function edit($id = '')
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['edit'] = $this->_edit((array("id" => dec_data($id))))['result'];

        $data['periode'] = $this->get_periode()['result'];
        $data['iu'] = $this->get_ind_utama()['result'];
        $data['sip'] = $this->get_sub_ind()['result'];
        $data['ip'] = $this->get_ind_penilaian()['result'];
        $data['jenis_peg'] = $this->get_jenis_peg()['result'];

        $this->tema->backend('backend/kinerja/skp/edit', $data);
    }

    public function update()
    {
        $post = $this->input->post();
        $data = array(
            'id_skp' => $post['id_skp'],
            'periode' => $post['periode'],
            'jenis_pegawai' => $post['id_jp'],
            'id_indikator_utama' => $post['ind_utama'],
            'id_sub_indikator' => $post['sub_ind'],
            'id_ind_penilaian' => $post['ind_penilaian'],
            'rincian_indikator' => $post['rincian_ind'],
            'syarat' => $post['prasyarat'],
            'bobot' => $post['bobot'],
            'skor' => $post['skor'],
            'ket' => $post['keterangan']
        );

        $aksi = $this->_update($data);
         
        if ($aksi['result'] === false) {
            $id_log = trim($post['id']);
            $this->session->set_flashdata('error', $aksi['message']);
            return redirect(base_url() . 'kinerja/skp/edit/'. $id_log);
        } else {
            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/skp/index');
        }
        
    }

    public function detail()
    {
        $data = array();
        if (empty($this->input->post())) {
            redirect(base_url() . 'kinerja/skp/detail_task');
        }
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['wfh'] = $this->get_wfh(array('id_absen' => dec_data($this->input->post('id_absen'))));
        $this->tema->backend('backend/kinerja/skp/mulai_kerja', $data);
    }

    public function download_files($id = '')
    {
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $rs_data = $this->get_download(array("id_task" => $id));
        $nama_file = $rs_data['result']['dokumen'];
        $file = $rs_data['result']['jdl_file'];
        $server_file = $this->server_file . 'file/laporan_pekerjaan/' . $nama_file;
        $data = file_get_contents("$server_file"); // Read the file's contents
        $ext = explode('.', $nama_file);
        $name = date('Ymd') . '_' . $file . '.' . $ext[1];
        return force_download($name, $data);
    }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    public function get_default($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/skp/default', 'base_token', $data), true);
    }

    public function _simpan($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/skp/simpan', 'base_token', $data), true);
    }
    public function _edit($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/skp/edit', 'base_token', $data), true);
    }
    public function _update($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/skp/update', 'base_token', $data), true);
    }

    public function _hapus($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/skp/hapus', 'base_token', $data), true);
    }

    public function get_periode($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/skp/periode', 'base_token', $data), true);
    }

    public function get_ind_utama($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/skp/ind_utama', 'base_token', $data), true);
    }

    public function get_sub_ind($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/skp/sub_ind', 'base_token', $data), true);
    }

    public function get_ind_penilaian($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/skp/ind_nilai', 'base_token', $data), true);
    }

    public function get_jenis_peg($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/skp/jenis_peg', 'base_token', $data), true);
    }

    public function get_data_skp($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/skp/get_data_skp', 'base_token', $data), true);
    }
}
