<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Asesor extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = 'd2907e43-1ce3-11ef-8a24-e6c3c46e6b60';
        $this->base_url = $this->app->get_server('base_server');
        // $this->isdm_url     = 'https://isdm.usm.ac.id/server/index.php/';
        $this->view = 'backend/kinerja/setup_asesor/';
        $this->link = 'kinerja/setup/asesor/';
        $this->isdm_url = "http://192.168.19.200/server/index.php/";
    }

    public function index()
    {
        
        // $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['data'] = $this->_doskar_list()['result'];
        $periods = $this->get_periode();
        $data['periode'] = $periods['result']['all_periode'];
        $data['p_aktif'] = $periods['result']['periode_now'];

        $this->tema->backend($this->view . 'index', $data);

    }

    public function json()
    {
        $input = $this->input->post();

        $dinilai_id_doskar = empty($input['dinilai_id_doskar']) ? '' : $input['dinilai_id_doskar'];
        $table = empty($input['table']) ? '' : $input['table'];
        $response = array();
        $doskar = $this->_doskar_list()['result'];
        usort($doskar, function ($a, $b) {
            // return $a['id_doskar'] <=> $b['id_doskar'];
            return strcmp($a['fak_unit'], $b['fak_unit']);
        });
        $get_data = $this->_get_data((array("id_periode" => $input['periode'])))['result'];
        $list_asesor = array();
        foreach ($get_data as $key => $value) {
            $list_asesor[$value['dinilai_id_doskar']][] = $value;
        }
        $cari_asesor = empty($list_asesor[$dinilai_id_doskar]) ? array() : $list_asesor[$dinilai_id_doskar];
        $no = 1;
        $data = array();
        foreach ($doskar as $key => $value) {
            if ($table == 'modal_tambah' && $value['id_doskar'] == $dinilai_id_doskar) {
                continue;
            }
            $value['no'] = $no++;
            $search_asesor = empty($list_asesor[$value['id_doskar']]) ? array() : $list_asesor[$value['id_doskar']];
            $search_asesor_teman = array();
            $search_asesor_pimpinan = array();
            $search_asesor_universitas = array();
            foreach ($search_asesor as $key2 => $val2) {
                if ($val2['kategori'] == 'teman') {
                    $search_asesor_teman[] = $val2;
                }
                if ($val2['kategori'] == 'pimpinan') {
                    $search_asesor_pimpinan[] = $val2;
                }
                if ($val2['kategori'] == 'universitas') {
                    $search_asesor_universitas[] = $val2;
                }
            }
            $value['search_asesor_teman'] =  $search_asesor_teman;
            $value['search_asesor_pimpinan'] =  $search_asesor_pimpinan;
            $value['search_asesor_universitas'] =  $search_asesor_universitas;
            $allow_tambah = '1';
            foreach ($cari_asesor as $key2 => $val2) {
                if ($val2['penilai_id_doskar'] == $value['id_doskar']) {
                    $allow_tambah = '0';
                }
            }
            $value['allow_tambah'] =  $allow_tambah;
            $data[] = $value;
        }
        $response['data'] = $data;
        echo json_encode($response);
    }

    public function ajax_update()
    {
        $input = $this->input->post();
        $aksi = $this->_insert($input);
        $response['message'] = $aksi['message'];
        $response['type'] = 'alert-success';
        if (empty($aksi['result'])) {
            $response['type'] = 'alert-warning';
        }
        echo json_encode($response);
    }

    public function ajax_delete($id_asesor)
    {
        $data = array(
            'id_asesor' => $id_asesor,
        );
        $aksi = $this->_delete($data);
        $response['message'] = $aksi['message'];
        $response['type'] = 'alert-success';
        if (empty($aksi['result'])) {
            $response['type'] = 'alert-warning';
        }
        echo json_encode($response);
    }

    public function _get_data($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/setup/asesor/get_data', 'base_token', $data), true);
    }
    public function _insert($array = array())
    {
        $data = json_encode($array);
        
        return json_decode($this->curl->request('POST', $this->base_url . 'api/kinerja/setup/asesor/insert', 'base_token', $data), true);
    }
    public function _delete($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/setup/asesor/delete', 'base_token', $data), true);
    }
    public function _doskar_list($array = array())
    {
        $array['keperluan'] = 'penilaian_kinerja';
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/lapker_doskar_list', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }

   public function get_periode($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/periode', 'base_token', $data), true);
    }
}
