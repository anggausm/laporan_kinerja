<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Validasi_skp extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = 'fe71f08b-5ddd-11ef-8a24-e6c3c46e6b60';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        $this->isdm_url = "http://192.168.19.200/server/index.php/";
        $this->load->helper('download');
        $this->token    = $this->app->cek_token();
        $this->userkey = $this->token['result']['user_key'];
    }

    public function index()
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'r');
        $periode        = $this->get_periode();
        $data['periode']= $periode['result']['all_periode'];
 
        $this->tema->backend('backend/kinerja/validasi_skp/index', $data);
    }

     public function data()
    {
        $data             = array();
        $data['crud']     = $this->app->AksesMenu($this->appUrl, 'r');
        $post             = $this->input->post();
        $data['p_chosen'] = $post['periode'];

        $periode        = $this->get_periode();
        $data['periode']  = $periode['result']['all_periode'];

        $data['list']     = $this->_list_dinilai((array("periode" => $post['periode'])))['result'];
    
        $data['p_aktif']  = $this->get_data_periode((array("id" => $post['periode'])))['result'];

        $this->tema->backend('backend/kinerja/validasi_skp/data', $data);

    }

    public function detail()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $post = $this->input->post();

        // $periods = $this->get_periode();
        // $data['periode'] = $periods['result']['all_periode'];
        
        $data['periode_dipilih'] = $post['periode'];
        $data['p_aktif'] = $this->get_data_periode((array("id" => $post['id_periode'])))['result'];

        $data['personel']   = $this->_cek_pegawai((array("user_key" => $post['user_key'])))['result'];
        $data['skp_peg'] = $this->get_skp_pegawai((array("periode" => $post['id_periode'], "user_key" => $post['user_key'])))['result'];

        // $data['edit'] = $this->get_skp_peg((array("jabatan" => $data['personel']['jenis_pegawai'])))['result'];
               
        // // $data['hk'] = $this->data_hari_kerja((array("jabatan" => $data['jabatan'], "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])))['result'];
        //  $data['rincian'] = $this->get_rincian((array("jabatan" => $data['personel']['jabatan'], "periode" => $data['periode_dipilih'])))['result'];
        //  foreach($data['rincian'] as $key){
        //     $data['period'] = $key['periode'];
        //     $data['nm_period'] = $key['nama_periode'];
        //     $data['jenis_peg'] = $key['jenis_pegawai'];
        //  }

        //  $log_book = $this->data_log_book((array("awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'], "id_periode" => $post['periode'])));
        //     $data['periode_log']    = $log_book['result']['plog'];
        //     $data['rekap_log']     = $log_book['result']['rlog'];
        //  //sertifikats
        //  $serkom = $this->data_sertifikat((array("jenis" => '2', "periode" => $data['periode_dipilih'], "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])));
        //     $data['sertifikat']    = $serkom['result']['sertp'];
        //     $data['sert_komp']    = $serkom['result']['sertj'];
        //     $data['sert_1']    = $serkom['result']['sert1'];
        //     $data['sert_event']    = $serkom['result']['sert2'];
        //     $serjur = $this->data_sertifikat((array("jenis" => '1', "periode" => $data['periode_dipilih'], "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])));
        //  $data['sert_juara']    = $serjur['result']['sertj'];
        //  //sks
        //  $sk_pan = $this->data_sk((array("jenis_sk" => 'panitia', "periode" => $data['periode_dipilih'], "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])));
        //     $data['sk_panitia']    = $sk_pan['result']['skj'];
         
        //  $sk_peg = $this->data_sk((array("jenis_sk" => 'pegawai', "periode" => $data['periode_dipilih'], "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])));
        //     $data['sk_pegawai']    = $sk_peg['result']['skpegawai'];

        //  //ijazah
        //  $data['ijazah'] = $this->data_ijazah((array("periode" => $data['periode_dipilih'])))['result'];

        //  //event
        //  $event1 = $this->data_event((array("jenis_event" => '1', "periode" => $data['periode_dipilih'], "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])));
        //     $data['event_wajib']    = $event1['result']['event1'];
        //     foreach($data['event_wajib'] as $ew){
        //         $data['foto2'] = $ew['file_foto'];
        //     }
        //     $data['event_pilihan']    = $event1['result']['event2'];
        //      foreach($data['event_pilihan'] as $ep){
        //         $data['foto1'] = $ep['file_foto'];
        //     }

        //     //nilai kuesioner
        //     $data['kuesioner'] = $this->nilai_kuesioner((array("periode" => $data['periode_dipilih'])))['result'];

        //     //cek_skp_peg
        //     

            $this->tema->backend('backend/kinerja/validasi_skp/detail', $data);        
    }

     public function validasi_pengajuan($userkey = '', $periode = '')
    {
        $post = $this->input->post();
        $data = array(
            'periode_penilaian' => dec_data($periode),
            'user_key' => dec_data($userkey)
        );

        $aksi = $this->_update_validasi($data);
         
        if ($aksi['result'] === false) {
            $id_per = trim($periode);
             $id_user = trim($userkey);
            $this->session->set_flashdata('error', $aksi['message']);
            return redirect(base_url() . 'kinerja/validasi_skp/detail/'. $id_user.'/'.$id_per);
        } else {

             $skp_valid = $this->get_skp_valid((array("periode" => dec_data($periode), "user_key" => dec_data($userkey))))['result'];
             $sekor = $skp_valid['total_nilai']*100;
             if($sekor >= 81){
                $nilai = 'Sangat Baik';
                $kategori = '100';
             } else if($sekor >= 60){
                $nilai = 'Baik';
                $kategori = '100';
             } else if($sekor >= 40){
                $nilai = 'Cukup';
                $kategori = '75';
             } else if($sekor >= 20){
                $nilai = 'Buruk';
                $kategori = '50';
            } else {
                $nilai = 'Sangat Buruk';
                $kategori = '25';
            }


             $datainsert = array(
                    'id_periode' => dec_data($periode),
                    'user_key' => dec_data($userkey),
                    'skor' => $skp_valid['total_nilai'],
                    'nilai' => $nilai,
                    'kategori' => $kategori

                );

             $aksi = $this->_insert_rekap($datainsert);

            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/validasi_skp/index');
        }
        
    }

    public function revisi($userkey = '', $periode = '')
    {
        $post = $this->input->post();

        $data = array(
            'id_periode' => dec_data($periode),
            'user_key' => dec_data($userkey)
        );

        $aksi = $this->_update_revisi($data);
        
        if ($aksi['result'] === false) {
            $this->session->set_flashdata('error', $aksi['message']);
             return redirect(base_url() . 'kinerja/validasi_skp/detail/'. $id_user.'/'.$id_per);
        } else {
            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/validasi_skp/index');
        }
        
    }

     public function edit($id = '')
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['edit'] = $this->_edit((array("id" => dec_data($id))))['result'];

         $data['stat_kerja'] = array(
            array('id_stat' => '1', 'nama_stat' => 'Selesai'),
            array('id_stat' => '2', 'nama_stat' => 'Berlanjut'),
            array('id_stat' => '3', 'nama_stat' => 'Tidak Selesai')
        );
        $this->tema->backend('backend/kinerja/validasi_skp/edit', $data);
    }

   

    public function hapus($id = '')
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'd');
        if (empty($id)) {
            $this->session->set_flashdata('error', 'Maaf proses anda belum bisa kami lanjutkan..');
            redirect(base_url() . 'kinerja/validasi_skp/index');
        }

        $delete         = $this->_batal(array("id" => dec_data($id)));
        $this->session->set_flashdata('pesan', $delete['message']);
        redirect(base_url() . 'kinerja/validasi_skp/index');
    }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/

    public function get_skp_pegawai($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('GET', $this->base_url . 'api/kinerja/val_skp/skp_pegawai', 'base_token', $data), true);
    }

    public function get_periode($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/periode', 'base_token', $data), true);
    }

    public function _update_validasi($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/val_skp/update_validasi', 'base_token', $data), true);
    }    

    public function _update_revisi($array = '')
    {
        $data = json_encode($array);
        // var_dump($data); die();
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/val_skp/update_revisi', 'base_token', $data), true);
    }   

     public function _periode_aktif($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/val_log/p_aktif', 'base_token', $data), true);
    }

    public function get_periode_skp($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/p_skp', 'base_token', $data), true);
    }

    public function get_skp_valid($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('GET', $this->base_url . 'api/kinerja/val_skp/get_skp_valid', 'base_token', $data), true);
    }

    public function _insert_rekap($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/val_skp/simpan', 'base_token', $data), true);
    }   
    

    public function get_data_pl($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/val_skp/data_pl', 'base_token', $data), true);
    }

    public function _simpan($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/val_skp/simpan', 'base_token', $data), true);
    }

    public function _batal($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/val_skp/hapus', 'base_token', $data), true);
    }

    public function _cek_pegawai($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/cek_data_pegawai', 'base_token', $data), true);
    }

     // REST API KEPEGAWAIAN
   public function _isdm_profil($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/lapker_doskar_detail', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }

    public function _isdm_kependidikan_list($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/kependidikan_list', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }

     public function _list_dinilai($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('GET', $this->base_url . 'api/kinerja/val_skp/list', 'base_token', $data), true);
    }

    public function get_data_periode($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/data_periode', 'base_token', $data), true);
    }


}
