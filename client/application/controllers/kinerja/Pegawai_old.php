<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pegawai extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '45dfc7ab-44d9-11ef-8a24-e6c3c46e6b60';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        $this->load->helper('download');
        $this->token    = $this->app->cek_token();
        $this->link = 'kinerja/pegawai/';

    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['periode'] = $this->get_periode()['result'];

        $this->tema->backend('backend/kinerja/pegawai/index', $data);
    }

    public function dashboard()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $post = $this->input->post();

        $data['periode']        = $this->get_periode()['result'];
        
        $data['periode_dipilih'] = $post['periode'];
        $data['p_aktif'] = $this->get_data_periode((array("id" => $post['periode'])))['result'];

        $user                   = $this->token['result'];
        $userkey                = $user['user_key'];
        $data['personel']       = $this->get_profil((array("user_key" => $userkey)))['result'];
        foreach($data['personel'] as $jab){
        $data['nama_doskar'] = $jab['nama_doskar'];
        $data['nis'] = $jab['nis'];
        $data['jabatan'] = $jab['jabatan'];
        $data['nama_unit'] = $jab['nama_unit'];
        $data['gol_baru'] = $jab['gol_baru'];
        $data['tmt_usm'] = $jab['tmt_usm'];
        $data['status_kerja'] = $jab['status_kerja'];
        }

         $data['edit'] = $this->get_skp_peg((array("jabatan" => $data['jabatan'])))['result'];
         $data['rincian'] = $this->get_rincian((array("jabatan" => $data['jabatan'], "periode" => $data['periode_dipilih'])))['result'];
         foreach($data['rincian'] as $key){
            $data['period'] = $key['periode'];
            $data['nm_period'] = $key['nama_periode'];
            $data['jenis_peg'] = $key['jenis_pegawai'];
         }

         $data['log_book'] = $this->data_log_book((array("jabatan" => $jabatan, "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'] )))['result'];
         $data['hk'] = $this->data_hari_kerja((array("jabatan" => $jabatan, "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])))['result'];
         $data['sertifikat'] = $this->data_sertifikat((array("jabatan" => $jabatan, "periode" => $data['periode_dipilih'])))['result'];
         $data['sk_panitia'] = $this->data_sk((array("jenis_sk" => 'panitia', "periode" => $data['periode_dipilih'])))['result'];
         $data['sk_pegawai'] = $this->data_sk((array("jenis_sk" => 'pegawai', "periode" => $data['periode_dipilih'])))['result'];
         $data['ijazah'] = $this->data_ijazah((array("periode" => $data['periode_dipilih'])))['result'];

        if($data['jenis_peg'] == 1){
            $this->tema->backend('backend/kinerja/pegawai/dashboard_tendik_ns', $data);
        } else if($data['jenis_peg'] == 2){
            $this->tema->backend('backend/kinerja/pegawai/dashboard_tendik_s', $data);
        } else if($data['jenis_peg'] == 3){
            $this->tema->backend('backend/kinerja/pegawai/dashboard_tunjang_1', $data);
        }  else if($data['jenis_peg'] == 4){
            $this->tema->backend('backend/kinerja/pegawai/dashboard_tunjang_2', $data);
        } else {
            $this->tema->backend('backend/kinerja/pegawai/dashboard', $data);
        }

        
    }

    public function form_spek_peg($id_periode='')
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');

        $user                   = $this->token['result'];
        $userkey                = $user['user_key'];
        $data['personel']       = $this->get_profil((array("user_key" => $userkey)))['result'];
        $data['p_aktif']        = $this->get_data_periode((array("id" => dec_data($id_periode))))['result'];

        $data['log_book']       = $this->data_log_book((array("jabatan" => $jabatan, "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'] )))['result'];
         $data['hk']            = $this->data_hari_kerja((array("jabatan" => $jabatan, "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])))['result'];

         $data['sertifikat']    = $this->data_sertifikat((array("jabatan" => $jabatan, "periode" => $data['periode_dipilih'])))['result'];
         $data['sk_pegawai']    = $this->data_sk((array("jenis_sk" => 'pegawai', "periode" => $data['periode_dipilih'])))['result'];
         $data['ijazah']        = $this->data_ijazah((array("periode" => $data['periode_dipilih'])))['result'];
      
        $this->tema->backend('backend/kinerja/pegawai/form_spek_peg', $data);
    }


     public function tambah_sertifikat()
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['sertifikat']        = $this->data_sertifikat()['result'];

         $data['tingkat'] = array(
            array('id_stat' => 'lokal', 'nama_stat' => 'Lokal'),
            array('id_stat' => 'provinsi', 'nama_stat' => 'Provinsi'),
            array('id_stat' => 'nasional', 'nama_stat' => 'Nasional'),
            array('id_stat' => 'internasional', 'nama_stat' => 'Internasional')
        );

        $this->tema->backend('backend/kinerja/pegawai/form_sertifikat', $data);
    }

    public function simpan_sertifikat()
    {
        $post = $this->input->post();
        $data['sertifikat']        = $this->data_sertifikat()['result'];
        $data = array(
            'no_sertifikat' => $post['nomor'],
            'nama_sertifikat' => $post['judul'],
            'tanggal_sertifikat' => $post['tgl_sert'],
            'tanggal_berlaku' => $post['tgl_akhir'],
            'penyelenggara' => $post['penyelenggara'],
            'tingkat_sertifikat' => $post['tingkat'],
            'ket' => $post['keterangan']
        );

        $aksi = $this->_simpan_sertifikat($data);
        
        if ($aksi['result'] == false) {
            $this->session->set_flashdata('error', $aksi['message']);
            return redirect(base_url() . 'kinerja/pegawai/tambah_sertifikat');
        } else {
            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/pegawai/tambah_sertifikat');
        }
        
    }

     public function tambah_ijazah()
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['ijazah']        = $this->data_ijazah()['result'];

        $data['jenis_ijazah'] = array(
            array('id_stat' => 'SD', 'nama_stat' => 'SD'),
            array('id_stat' => 'MI', 'nama_stat' => 'MI'),
            array('id_stat' => 'SMP', 'nama_stat' => 'SMP'),
            array('id_stat' => 'MTs', 'nama_stat' => 'MTs'),
            array('id_stat' => 'SMA', 'nama_stat' => 'SMA'),
            array('id_stat' => 'SMK', 'nama_stat' => 'SMK'),
            array('id_stat' => 'MA', 'nama_stat' => 'MA'),
            array('id_stat' => 'D1', 'nama_stat' => 'D1'),
            array('id_stat' => 'D2', 'nama_stat' => 'D2'),
            array('id_stat' => 'D3', 'nama_stat' => 'D3'),
            array('id_stat' => 'D4', 'nama_stat' => 'D4'),
            array('id_stat' => 'S1', 'nama_stat' => 'S1'),
            array('id_stat' => 'S2', 'nama_stat' => 'S2'),
            array('id_stat' => 'S3', 'nama_stat' => 'S3')
        );

        $this->tema->backend('backend/kinerja/pegawai/form_ijazah', $data);
    }

    public function simpan_ijazah()
    {
        $post = $this->input->post();
        $data = array(
            'jenjang_pendidikan' => $post['jenjang'],
            'tanggal_ijazah' => $post['tgl_jazah'],
            'nama_instansi' => $post['nama_sekolah'],
            'no_ijazah' => $post['nomor'],
            'tahun_masuk' => $post['t_masuk'],
            'tahun_lulus' => $post['t_lulus'],
            'file' => $post['file']
        );

        $aksi = $this->_simpan_ijazah($data);
        
        if ($aksi['result'] == false) {
            $this->session->set_flashdata('error', $aksi['message']);
            return redirect(base_url() . 'kinerja/pegawai/tambah_ijazah');
        } else {
            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/pegawai/tambah_ijazah');
        }
        
    }

    public function tambah_sk_panitia()
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['sk']        = $this->data_sk((array("jenis_sk" => 'pegawai', "periode" => $periode)))['result'];

        $data['jenis_sk'] = array(
            array('id_stat' => 'panitia', 'nama_stat' => 'Panitia Kegiatan'),
            array('id_stat' => 'pegawai', 'nama_stat' => 'Pengangkatan Pegawai'),
            array('id_stat' => '', 'nama_stat' => 'lainnya')
        );

        $this->tema->backend('backend/kinerja/pegawai/form_sk_panitia', $data);
    }

    public function tambah_sk_pegawai()
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['sk']        = $this->data_sk((array("jenis_sk" => 'pegawai', "periode" => $periode)))['result'];

        $data['jenis_sk'] = array(
            array('id_stat' => 'panitia', 'nama_stat' => 'Panitia Kegiatan'),
            array('id_stat' => 'pegawai', 'nama_stat' => 'Pengangkatan Pegawai'),
            array('id_stat' => '', 'nama_stat' => 'lainnya')
        );

        $this->tema->backend('backend/kinerja/pegawai/form_sk_pegawai', $data);
    }

    public function simpan_sk()
    {
        $post = $this->input->post();
        $data = array(
            'jenis_sk' => $post['jenis'],
            'tanggal_sk' => $post['tgl'],
            'judul_sk' => $post['judul'],
            'no_sk' => $post['nomor'],
            'tmt_sk' => $post['tmt'],
            'exp_sk' => $post['exp'],
            'file_sk' => $post['filesk']
        );

        $aksi = $this->_simpan_sk($data);
        
        if ($aksi['result'] == false) {
            $this->session->set_flashdata('error', $aksi['message']);
            return redirect(base_url() . 'kinerja/pegawai/tambah_sk');
        } else {
            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/pegawai/tambah_sk');
        }
        
    }

    public function simpan_sk_pegawai()
    {
        $post = $this->input->post();
        $data = array(
            'jenis_sk' => $post['jenis_sk'],
            'tanggal_sk' => $post['tgl'],
            'judul_sk' => $post['judul'],
            'no_sk' => $post['nomor'],
            'tmt_sk' => $post['tmt'],
            'exp_sk' => $post['exp'],
            'file_sk' => $post['filesk']
            'flus' => $post['flus'],
            'prodi' => $post['prodi'],
            'tmt_usm' => $post['tmt_usm'],
            'status_kerja' => $post['status_kerja'],
            'jenis_pekerjaan' => $post['jenis_pekerjaan']
            
        );

        $aksi = $this->_simpan_sk_pegawi($data);
        
        if ($aksi['result'] == false) {
            $this->session->set_flashdata('error', $aksi['message']);
            return redirect(base_url() . 'kinerja/pegawai/tambah_sk');
        } else {
            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/pegawai/tambah_sk');
        }
        
    }

     public function form_event()
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['sk']        = $this->data_sk((array("jenis_sk" => 'pegawai', "periode" => $periode)))['result'];

        $data['jenis_sk'] = array(
            array('id_stat' => 'panitia', 'nama_stat' => 'Panitia Kegiatan'),
            array('id_stat' => 'pegawai', 'nama_stat' => 'Pengangkatan Pegawai'),
            array('id_stat' => '', 'nama_stat' => 'lainnya')
        );

        $this->tema->backend('backend/kinerja/pegawai/form_sk', $data);
    }

    
    public function kuesioner()
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');

        $user                   = $this->token['result'];
        $userkey                = $user['user_key'];
        $data['personel']       = $this->get_profil((array("user_key" => $userkey)))['result'];

        $this->tema->backend('backend/kinerja/pegawai/form_kuesioner', $data);
    }

     public function edit($id = '')
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['edit'] = $this->_edit((array("id" => dec_data($id))))['result'];

         $data['stat_kerja'] = array(
            array('id_stat' => '1', 'nama_stat' => 'Selesai'),
            array('id_stat' => '2', 'nama_stat' => 'Berlanjut'),
            array('id_stat' => '3', 'nama_stat' => 'Tidak Selesai')
        );
        $this->tema->backend('backend/kinerja/pegawai/edit', $data);
    }

    public function update()
    {
        $post = $this->input->post();
        $data = array(
            'id_pegawai' => dec_data($post['id']),
            'tanggal_pegawai' => $post['tgl_pegawai'],
            'tugas' => $post['tugas'],
            'status_pekerjaan' => $post['stat_kerja'],
            'ket' => $post['keterangan']
        );

        $aksi = $this->_update($data);
         
        if ($aksi['result'] === false) {
            $id_log = trim($post['id']);
            $this->session->set_flashdata('error', $aksi['message']);
            return redirect(base_url() . 'kinerja/pegawai/edit/'. $id_log);
        } else {
            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/pegawai/index');
        }
        
    }

    public function detail()
    {
        $data = array();
        if (empty($this->input->post())) {
            redirect(base_url() . 'kinerja/pegawai/detail_task');
        }
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['wfh'] = $this->get_wfh(array('id_absen' => dec_data($this->input->post('id_absen'))));
        $this->tema->backend('backend/kinerja/pegawai/mulai_kerja', $data);
    }

    public function download_files($id = '')
    {
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $rs_data = $this->get_download(array("id_task" => $id));
        $nama_file = $rs_data['result']['dokumen'];
        $file = $rs_data['result']['jdl_file'];
        $server_file = $this->server_file . 'file/laporan_pekerjaan/' . $nama_file;
        $data = file_get_contents("$server_file"); // Read the file's contents
        $ext = explode('.', $nama_file);
        $name = date('Ymd') . '_' . $file . '.' . $ext[1];
        return force_download($name, $data);
    }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/

    public function get_profil($data)
    {
        $tkn = 'be2ddf82b4b3feae7c3bfa771caa72eb';
        $data1 = json_encode($data);
        return json_decode($this->curl->request_isdm_profil('PATCH', 'https://isdm.usm.ac.id/server/index.php/connection/isdm/sppd_profil', $tkn, $data1), true);
    }

    public function get_periode($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/periode', 'base_token', $data), true);
    }

     public function get_skp_peg($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/skp_peg', 'base_token', $data), true);
    }

    public function _simpan($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/simpan', 'base_token', $data), true);
    }
    public function _edit($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/edit', 'base_token', $data), true);
    }
    public function _update($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/update', 'base_token', $data), true);
    }

    public function _hapus($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/pegawai/hapus', 'base_token', $data), true);
    }

    public function get_rincian($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/rincian', 'base_token', $data), true);
    }

    public function data_log_book($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/get_log_book', 'base_token', $data), true);
    }

    public function data_hari_kerja($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/get_wd', 'base_token', $data), true);
    }

    public function data_sertifikat($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/get_sertifikat', 'base_token', $data), true);
    }

      public function _simpan_sertifikat($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/simpan_sertifikat', 'base_token', $data), true);
    }

    public function data_sk($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/get_sk', 'base_token', $data), true);
    }

    public function _simpan_sk($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/simpan_sk', 'base_token', $data), true);
    }

    public function _simpan_sk_pegawi($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/simpan_sk_pegawai', 'base_token', $data), true);
    }

    public function data_ijazah($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/get_ijazah', 'base_token', $data), true);
    }

    public function _simpan_ijazah($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/simpan_ijazah', 'base_token', $data), true);
    }

    public function get_data_periode($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/data_periode', 'base_token', $data), true);
    }


}
