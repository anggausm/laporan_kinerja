<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tambah_ijin extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '662c130b-71c2-11ea-8d6b-1cb72c27dd68';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        // $this->load->helper('download');
    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');

        $data['get_default'] = $this->get_default();
        $this->tema->backend('ijin/tambah_ijin/index', $data);
    }

    public function tambah()
    {
        $data = array();
        $data['doskar_usm'] = $this->_doskar_usm();
        $data['jenis_ijin'] = $this->_jenis_ijin();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $this->tema->backend('ijin/tambah_ijin/tambah', $data);
    }

    public function simpan()
    {
        $post = $this->input->post();
        $data = array(
            'user_key' => $post['doskar_usm'],
            'tanggal_awal' => $post['tgl_mulai'],
            'tanggal_akhir' => $post['tgl_selesai'],
            'jenis_ijin' => $post['jenis_ijin'],
            'keterangan' => $post['keterangan'],
            'status' => 'Menunggu'
        );

        $aksi = $this->_simpan($data);
        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] == false) {
            $this->session->set_flashdata('type', 'danger');
        } else {
            $this->session->set_flashdata('type', 'success');
        }
        return redirect(site_url("ijin/tambah_ijin/"), 'refresh');
    }

    public function edit()
    {
        $data               = array();     
        $post               = $this->input->post();   
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'r');
        $data['doskar_usm'] = $this->_doskar_usm();
        $data['jenis']      = $this->_jenis_ijin();
        $user               = array('id' => dec_data($post['id']));
        $data['rs_data']    = $this->_edit($user);

        $this->tema->backend('ijin/tambah_ijin/edit', $data);
    }

    public function update() {
        $data           = array();
        $input          = $this->input->post();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'u');

        if (empty($input)) 
        {
            redirect(base_url() . 'ijin/tambah_ijin');
        }

        $array          = array('id'            => $input['id'],
                                'user_key'      => $input['doskar_usm'],
                                'tanggal_awal'  => $input['tgl_mulai'],
                                'tanggal_akhir' => $input['tgl_selesai'],
                                'jenis_ijin'    => $input['jenis_ijin'],
                                'keterangan'    => $input['keterangan'],
                                'status'        => $input['status']);

        $update_ijin    = $this->_update($array);
        $this->session->set_flashdata('pesan', $update_ijin['message']);
        redirect(base_url() . 'ijin/tambah_ijin');
    }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    public function get_default($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/ijin/tambah/default', 'base_token', $data), true);
    }

    public function _doskar_usm($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/ijin/tambah/get_doskar', 'base_token', $data), true)['result'];
    }

    public function _jenis_ijin($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/ijin/tambah/get_jenis', 'base_token', $data), true)['result'];
    }

    public function _simpan($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('POST', $this->base_url . 'api/ijin/tambah/simpan', 'base_token', $data), true);
    }

    public function _edit($array = "")
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/ijin/tambah/edit', 'base_token', $data), true);
    }

    public function _update($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/ijin/tambah/update', 'base_token', $data), true);
    }

    // public function _delete($array = array())
    // {
    //     $data = json_encode($array);
    //     return json_decode($this->curl->request('DELETE', $this->base_url . 'api/ijin/tambah/delete', 'base_token', $data), true);
    // }
}
