<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kpg/anggota/dosen">Daftar Angota Unit</a></li>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa fa-list"></i> <b>Unit  <?php print $rs_data['result']['posisi_user']['unit_bagian'];?></b> <br>Jabatan Struktural <?php print $rs_data['result']['posisi_user']['nama_sub_struktural'];?>
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
							<thead>
								<tr>
									<th style="width:5%" class="text-center">No</th>
									<th style="width:40%">Nama Lengkap</th>
									<th style="width:20%">Posisi</th>
									<th style="width:15%" class="text-center">User name</th>
									<th style="width:15%" class="text-center">Password Default</th>
								</tr>
							</thead>
							<tbody>
								<!-- 	 "id_sub_struktural": "67",
								"id_struktural": "8",
								"nama_sub_struktural": "Ka. UPT. PD USM (Pangkalan Data USM)",
								"kode_sto": "8.2",
								"level": "2",
								"user_key": "3b6894d0-6f68-11ea-89e7-1cb72c27dd68",
								"nama_doskar": "B. VERY CHRISTIOKO, S.Kom., M.Kom.",
								"pas": "*sama dengan password SIA",
								"nickname": "GDOS038",
								"nidn": "0615098202" -->
								<?php
$no = 1;
foreach ($rs_data['result']['rs_data'] as $value) {
    // print var_dump($value);
    ?>
								<tr>
									<td class="text-center"><?=$no?></td>
									<td><?=$value['nama_doskar'];?></td>
									<td><?=$value['nama_sub_struktural'];?></td>
									<td><?=$value['nickname'];?></td>
									<td><?=$value['pas'];?></td>
								</tr>
								<?php
$no++;
}
?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>