
<div class="grey-300 bg-sm">
	<div class="text-center">
		<h2 class="text-shadow no-margin text-white text-4x p-v-lg">
		<span class="text-1x font-bold m-t-lg block">Maaf...</span>
		</h2>
		<h2 class="h2 m-v-lg text-white">Anda belum memiliki akses di halaman ini.</h2>
		<div class="p-v-lg">
			<a ui-sref="app.dashboard" md-ink-ripple class="md-btn indigo md-raised p-h-lg" href="<?php print base_url() . 'app';?>">
				<span class="text-white"><i class="fa  fa-reply-all"></i> Kembali</span>
			</a>
		</div>
	</div>
</div>
