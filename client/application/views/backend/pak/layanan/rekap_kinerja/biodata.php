<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb r-3x light-blue-900">
                <li class="breadcrumb-item"><a href="<?php print base_url() ?>dashboard/"><i class="fa fa-dashboard "></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>">Rekap Kinerja</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>biodata">Biodata</a></li>
                <span style="float: right;">
                    <a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih"></i> &nbsp;Kembali</a>
                </span>
            </ol>
        </nav>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) { ?>
    <div class="alert alert-warning" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <h4 class="font-bold text-capitalize no-margin m-b">biodata dosen</h4>

            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <tbody>
                        <tr>
                            <td style="width:30%">NIDN</td>
                            <td style="width:70%">0601020304</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Status Dosen</td>
                            <td style="width:70%">Dosen Tetap Yayasan</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Jabatan Fungsional</td>
                            <td style="width:70%">Profesor</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Program Studi</td>
                            <td style="width:70%">S1-Perencanaan Wilayah dan Kota</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Gelar depan</td>
                            <td style="width:70%">Prof.Dr.</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Nama</td>
                            <td style="width:70%">Aljabar Linear</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Gelar belakang</td>
                            <td style="width:70%">M.Si.</td>
                        </tr>
                        <tr>
                            <td style="width:30%">TMT Jabatan Fungsional Terakhir</td>
                            <td style="width:70%">01 Januari 2000</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Status Keaktifan</td>
                            <td style="width:70%">Aktif</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>