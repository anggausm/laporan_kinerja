<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb r-3x light-blue-900">
                <li class="breadcrumb-item"><a href="<?php print base_url() ?>dashboard/"><i class="fa fa-dashboard "></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>">Rekap Kinerja</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>pendidikan">Pendidikan</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>pendidikan_detail">Detail</a></li>
                <span style="float: right;">
                    <a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih"></i> &nbsp;Kembali</a>
                </span>
            </ol>
        </nav>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) { ?>
    <div class="alert alert-warning" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <h4 class="text-capitalize no-margin">daftar kegiatan <span class="font-bold">Pendidikan</span></h4>
            <p class="no-margin m-b">Sub Komponen 1. Melaksanakan perkuliahan</p>

            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <thead class="light-blue-100">
                        <tr>
                            <th style="width:3%" class="v-m">No.</th>
                            <th style="width:10%" class="v-m text-center">Semester</th>
                            <th style="width:32%" class="v-m">Nama MK</th>
                            <th style="width:10%" class="v-m text-center">SKS</th>
                            <th style="width:15%" class="v-m text-center">Jumlah Kelas</th>
                            <th style="width:15%" class="v-m text-center">Jumlah Dosen</th>
                            <th style="width:15%" class="v-m text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td class="text-center">20231</td>
                            <td>Analisis dan Perancangan Berorientasi Objek</td>
                            <td class="text-center">3</td>
                            <td class="text-center">1</td>
                            <td class="text-center">1</td>
                            <td class="text-center">
                                <a href="#" class="btn btn-success r-2x btn-sm">Lihat</a>
                                <a href="#" class="btn btn-stroke btn-info r-2x btn-sm">Edit</a>
                                <a href="#" class="btn btn-stroke btn-danger r-2x btn-sm" alt="Hapus"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td class="text-center">20231</td>
                            <td>Organisasi dan Arsitektur Komputer</td>
                            <td class="text-center">3</td>
                            <td class="text-center">1</td>
                            <td class="text-center">1</td>
                            <td class="text-center">
                                <a href="#" class="btn btn-success r-2x btn-sm">Lihat</a>
                                <a href="#" class="btn btn-stroke btn-info r-2x btn-sm">Edit</a>
                                <a href="#" class="btn btn-stroke btn-danger r-2x btn-sm"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <h4 class="no-margin m-b">Perhitungan KUM</h4>

            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <thead class="light-blue-100">
                        <tr>
                            <th rowspan="2" style="width:3%" class="v-m">No.</th>
                            <th rowspan="2" style="width:17%" class="v-m text-center">Semester</th>
                            <th colspan="2" style="width:40%" class="v-m text-center">Usulan Dosen</th>
                            <th colspan="2" style="width:40%" class="v-m text-center">Penilaian Asesor</th>
                        </tr>
                        <tr>
                            <th class="v-m text-center">SKS Total</th>
                            <th class="v-m text-center">Nilai KUM</th>
                            <th class="v-m text-center">SKS Total</th>
                            <th class="v-m text-center">Nilai KUM</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td class="text-center">20231</td>
                            <td class="text-center">6</td>
                            <td class="text-center">3</td>
                            <td class="text-center">6</td>
                            <td class="text-center">3</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <h4 class="text-capitalize no-margin">tambah kegiatan <span class="font-bold">Pendidikan</span></h4>
            <p class="no-margin m-b">Sub Komponen 1. Melaksanakan perkuliahan</p>

            <form role="form" method="POST" class="form-horizontal" action="#" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-2">Tanggal Terbit Dokumen</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control r-2x" name="" placeholder="dd/mm/yyyy" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">Semester</label>
                    <div class="col-sm-10">
                        <select name="" class="form-control r-2x select2" style="width: 100%;" required>
                            <option value="">Pilih Semester</option>
                            <option value="">2023/2024 Genap</option>
                            <option value="">2023/2024 Gasal</option>
                            <option value="">2022/2023 Genap</option>
                            <option value="">2022/2023 Gasal</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">Nama Mata Kuliah</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control r-2x" name="" placeholder="Inputkan Nama Mata Kuliah" required>
                        <p class="text-muted no-margin">Inputkan hanya 1 mata kuliah</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">SKS</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control r-2x" name="" placeholder="Inputkan Jumlah SKS" required>
                        <p class="text-muted no-margin">Cukup ditulis angka</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">Jumlah Kelas</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control r-2x" name="" placeholder="Inputkan Jumlah Kelas" required>
                        <p class="text-muted no-margin">Cukup ditulis angka</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">Jumlah Dosen</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control r-2x" name="" placeholder="Inputkan Jumlah Dosen" required>
                        <p class="text-muted no-margin">Cukup ditulis angka</p>
                    </div>
                </div>
                <button type="submit" class="btn btn-success r-2x" id="btnSubmit"> Simpan</button>
            </form>
        </div>
    </div>
</div>