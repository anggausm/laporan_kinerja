<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb r-3x light-blue-900">
                <li class="breadcrumb-item"><a href="<?php print base_url() ?>dashboard/"><i class="fa fa-dashboard "></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>">Rekap Kinerja</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>penelitian">Penelitian</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>penelitian_detail">Detail</a></li>
                <span style="float: right;">
                    <a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih"></i> &nbsp;Kembali</a>
                </span>
            </ol>
        </nav>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) { ?>
    <div class="alert alert-warning" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <h4 class="text-capitalize no-margin">daftar kegiatan <span class="font-bold">Penelitian</span></h4>
            <p class="no-margin m-b">Sub Komponen 1. Hasil penelitian atau hasil pemikian yang dipublikasikan pada Jurnal internasional bereputasi/Buku Referensi (syarat buku referensi harus memuat minimal 5 artikel hasil karya penulis)</p>

            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <thead class="light-blue-100">
                        <tr>
                            <th style="width:3%" class="v-m">No.</th>
                            <th style="width:20%" class="v-m">Judul Buku</th>
                            <th style="width:12%" class="v-m">Penulis Buku</th>
                            <th style="width:10%" class="v-m text-center">ISBN</th>
                            <th style="width:10%" class="v-m text-center">Penerbit</th>
                            <th style="width:10%" class="v-m text-center">Bulan Tahun Terbit</th>
                            <th style="width:10%" class="v-m text-center">Usulan Dosen</th>
                            <th style="width:10%" class="v-m text-center">Penilain Asesor</th>
                            <th style="width:15%" class="v-m text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Organisasi dan Arsitektur Komputer</td>
                            <td>Aljabar Linier</td>
                            <td class="text-center">123176777</td>
                            <td class="text-center">USM PRESS</td>
                            <td class="text-center">Juli 2024</td>
                            <td class="text-center">10</td>
                            <td class="text-center">10</td>
                            <td class="text-center">
                                <a href="#" class="btn btn-success r-2x btn-sm">Lihat</a>
                                <a href="#" class="btn btn-stroke btn-info r-2x btn-sm">Edit</a>
                                <a href="#" class="btn btn-stroke btn-danger r-2x btn-sm" alt="Hapus"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <h4 class="text-capitalize no-margin">tambah kegiatan <span class="font-bold">Penelitian</span></h4>
            <p class="no-margin m-b">Sub Komponen 1. Hasil penelitian atau hasil pemikian yang dipublikasikan pada Jurnal internasional bereputasi/Buku Referensi (syarat buku referensi harus memuat minimal 5 artikel hasil karya penulis)</p>

            <form role="form" method="POST" class="form-horizontal" action="#" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-2">Tanggal Terbit Dokumen</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control r-2x" name="" placeholder="dd/mm/yyyy" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">Judul Buku</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control r-2x" name="" placeholder="Inputkan Judul Buku" required>
                        <p class="text-muted no-margin">Buku referensi : Buku (Ber ISBN) subtansi pembahasannya pada 1 bidang ilmu kompetensi penulis; Jika diambil dari disertasi/tesis tidak dapat dinilai untuk usulan kenaikan jabatan akademik/pangkat</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">Penulis Buku</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control r-2x" name="" placeholder="Inputkan Penulis Buku" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">ISBN</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control r-2x" name="" placeholder="Inputkan ISBN" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">Penerbit Buku</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control r-2x" name="" placeholder="Inputkan Penerbit Buku" required>
                    </div>
                </div>
                <button type="submit" class="btn btn-success r-2x" id="btnSubmit"> Simpan</button>
            </form>
        </div>
    </div>
</div>