<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb r-3x light-blue-900">
                <li class="breadcrumb-item"><a href="<?php print base_url() ?>dashboard/"><i class="fa fa-dashboard "></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>">Rekap Kinerja</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>pendidikan">Pendidikan</a></li>
                <span style="float: right;">
                    <a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih"></i> &nbsp;Kembali</a>
                </span>
            </ol>
        </nav>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) { ?>
    <div class="alert alert-warning" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <h4 class="font-bold text-capitalize no-margin m-b">pendidikan</h4>

            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <thead class="light-blue-100">
                        <tr>
                            <th style="width:5%" class="v-m">No.</th>
                            <th style="width:40%" class="v-m">Sub Komponen</th>
                            <th style="width:10%" class="v-m text-center">Aksi</th>
                            <th style="width:15%" class="v-m text-center">Satuan</th>
                            <th style="width:15%" class="v-m text-center">Usulan Dosen</th>
                            <th style="width:15%" class="v-m text-center">Penilaian Asesor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Melaksanakan perkuliahan</td>
                            <td class="text-center"><a href="<?= base_url($this->link) ?>pendidikan_detail" class="btn btn-info r-2x btn-sm">Detail</a></td>
                            <td class="text-center">Tiap SKS</td>
                            <td class="text-center">12</td>
                            <td class="text-center">12</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Membimbing KKN/PKL/KP/KKL/MBKM</td>
                            <td class="text-center"><a href="<?= base_url($this->link) ?>pendidikan_detail" class="btn btn-info r-2x btn-sm">Detail</a></td>
                            <td class="text-center">Tiap Semester</td>
                            <td class="text-center">2</td>
                            <td class="text-center">2</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Membimbing Skripsi/TA/Thesis/Disertasi</td>
                            <td class="text-center"><a href="<?= base_url($this->link) ?>pendidikan_detail" class="btn btn-info r-2x btn-sm">Detail</a></td>
                            <td class="text-center">Setiap Mahasiswa</td>
                            <td class="text-center">2</td>
                            <td class="text-center">2</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Menguji Skripsi/TA/Thesis/Disertasi</td>
                            <td class="text-center"><a href="<?= base_url($this->link) ?>pendidikan_detail" class="btn btn-info r-2x btn-sm">Detail</a></td>
                            <td class="text-center">Setiap Mahasiswa</td>
                            <td class="text-center">2</td>
                            <td class="text-center">2</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Membina kegiatan mahasiswa</td>
                            <td class="text-center"><a href="<?= base_url($this->link) ?>pendidikan_detail" class="btn btn-info r-2x btn-sm">Detail</a></td>
                            <td class="text-center">Tiap Semester</td>
                            <td class="text-center">2</td>
                            <td class="text-center">2</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>