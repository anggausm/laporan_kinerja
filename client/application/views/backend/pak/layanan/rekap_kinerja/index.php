<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb r-3x light-blue-900">
                <li class="breadcrumb-item"><a href="<?php print base_url() ?>dashboard/"><i class="fa fa-dashboard "></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>">Rekap Kinerja</a></li>
                <span style="float: right;">
                    <a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih"></i> &nbsp;Kembali</a>
                </span>
            </ol>
        </nav>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) { ?>
    <div class="alert alert-warning" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php } ?>

<div class="row row-flex">
    <div class="col-md-6">
        <div class="card p r-3x box-shadow-md">
            <h4 class="font-bold text-capitalize no-margin m-b">data ajuan</h4>

            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" style="border: 1px solid #e7eaec">
                    <thead class="light-blue-100">
                        <tr>
                            <th style="width:5%" class="v-m">No.</th>
                            <th style="width:35%" class="v-m">Jenis Kinerja</th>
                            <th style="width:30%" class="v-m">Syarat</th>
                            <th style="width:15%" class="v-m">Poin Kinerja</th>
                            <th style="width:15%" class="v-m">Penilaian Asesor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Pendidikan</td>
                            <td>Tidak boleh kosong</td>
                            <td>7</td>
                            <td>7</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Penelitian</td>
                            <td>Tidak boleh kosong</td>
                            <td>7</td>
                            <td>7</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Pengabdian</td>
                            <td>Tidak boleh kosong</td>
                            <td>7</td>
                            <td>7</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Penunjang</td>
                            <td>Tidak boleh kosong</td>
                            <td>7</td>
                            <td>7</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Core Values</td>
                            <td>Tidak boleh kosong</td>
                            <td>7</td>
                            <td>7</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Tugas Tambahan</td>
                            <td>-</td>
                            <td>7</td>
                            <td>7</td>
                        </tr>
                        <tr>
                            <td colspan="3" class="bg-light lt text-black text-center font-bold">Total Kinerja</td>
                            <td class="bg-light lt text-black font-bold">44</td>
                            <td class="bg-light lt text-black font-bold">80</td>
                        </tr>
                        <tr>
                            <td colspan="3" class="bg-light lt text-black text-center font-bold">Nilai</td>
                            <td class="bg-success font-bold">Baik</td>
                            <td class="bg-success font-bold">Baik</td>
                            <!-- <td class="bg-warning text-black font-bold">Cukup</td>
                            <td class="bg-warning text-black font-bold">Cukup</td> -->
                            <!-- <td class="bg-danger font-bold">Sangat Kurang</td>
                            <td class="bg-danger font-bold">Sangat Kurang</td> -->
                        </tr>
                    </tbody>
                </table>
            </div>

            <hr class="m-b">

            <a href="#" class="btn btn-info r-2x p-h-md">Ajukan <i class="fa fa-send-o"></i></a>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card p r-3x box-shadow-md">
            <h4 class="font-bold text-capitalize no-margin m-b">data dosen</h4>

            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <tbody>
                        <tr>
                            <td style="width:30%">NIDN</td>
                            <td style="width:70%">0601020304</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Status Dosen</td>
                            <td style="width:70%">Dosen Tetap Yayasan</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Jabatan Fungsional</td>
                            <td style="width:70%">Profesor</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Program Studi</td>
                            <td style="width:70%">S1-Perencanaan Wilayah dan Kota</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Gelar depan</td>
                            <td style="width:70%">Prof.Dr.</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Nama</td>
                            <td style="width:70%">Aljabar Linear</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Gelar belakang</td>
                            <td style="width:70%">M.Si.</td>
                        </tr>
                        <tr>
                            <td style="width:30%">TMT Jabatan Fungsional Terakhir</td>
                            <td style="width:70%">01 Januari 2000</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Status Keaktifan</td>
                            <td style="width:70%">Aktif</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <hr class="m-b">

            <a href="<?= base_url($this->link) ?>biodata" class="btn btn-stroke btn-info r-2x p-h-md">Detail Data</a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-sm-2 col-xs-6">
                <a href="<?= base_url($this->link) ?>pendidikan" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                    <div class="text-center">
                        <i class="fa fa-mortar-board fa-3x text-primary"></i>
                        <p class="no-margin m-t font-bold">Pendidikan</p>
                    </div>
                </a>
            </div>
            <div class="col-sm-2 col-xs-6">
                <a href="<?= base_url($this->link) ?>penelitian" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                    <div class="text-center">
                        <i class="fa fa-search fa-3x text-primary"></i>
                        <p class="no-margin m-t font-bold">Penelitian</p>
                    </div>
                </a>
            </div>
            <div class="col-sm-2 col-xs-6">
                <a href="<?= base_url($this->link) ?>pengabdian" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                    <div class="text-center">
                        <i class="fa fa-share-alt fa-3x text-primary"></i>
                        <p class="no-margin m-t font-bold text-ellipsis">Pengabdian</p>
                    </div>
                </a>
            </div>
            <div class="col-sm-2 col-xs-6">
                <a href="<?= base_url($this->link) ?>penunjang" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                    <div class="text-center">
                        <i class="fa fa-rocket fa-3x text-primary"></i>
                        <p class="no-margin m-t font-bold text-ellipsis">Penunjang</p>
                    </div>
                </a>
            </div>
            <div class="col-sm-2 col-xs-6">
                <a href="<?= base_url($this->link) ?>core_values" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                    <div class="text-center">
                        <i class="fa fa-heart fa-3x text-primary"></i>
                        <p class="no-margin m-t font-bold">Core Values</p>
                    </div>
                </a>
            </div>
            <div class="col-sm-2 col-xs-6">
                <a href="<?= base_url($this->link) ?>tamb
                ahan" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                    <div class="text-center">
                        <i class="fa fa-tasks fa-3x text-primary"></i>
                        <p class="no-margin m-t font-bold">Tugas Tambahan</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <h4 class="font-bold text-capitalize no-margin m-b">hisori ajuan</h4>

            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <thead class="light-blue-100">
                        <tr>
                            <th>Jenis Ajuan</th>
                            <th>Tanggal Ajuan</th>
                            <th>Tanggal Verifikasi</th>
                            <th>Status Ajuan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Ajuan Kinerja</td>
                            <td>12 Juli 2024</td>
                            <td>01 Agustus 2024</td>
                            <td><span class="label red">Ditolak</span></td>
                            <td><a href="#" class="btn btn-info r-2x btn-sm"><i class="fa fa-info-circle"></i></a></td>
                        </tr>
                        <tr>
                            <td>Ajuan Kinerja</td>
                            <td>13 Juli 2024</td>
                            <td>02 Agustus 2024</td>
                            <td><span class="label orange">Ditangguhkan</span></td>
                            <td><a href="#" class="btn btn-info r-2x btn-sm"><i class="fa fa-info-circle"></i></a></td>
                        </tr>
                        <tr>
                            <td>Ajuan Kinerja</td>
                            <td>14 Juli 2024</td>
                            <td>03 Agustus 2024</td>
                            <td><span class="label teal">Disetuji</span></td>
                            <td><a href="#" class="btn btn-info r-2x btn-sm"><i class="fa fa-info-circle"></i></a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>