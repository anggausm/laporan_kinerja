<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb r-3x light-blue-900">
                <li class="breadcrumb-item"><a href="<?php print base_url() ?>dashboard/"><i class="fa fa-dashboard "></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>">Rekap Kinerja</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>penelitian">Penelitian</a></li>
                <span style="float: right;">
                    <a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih"></i> &nbsp;Kembali</a>
                </span>
            </ol>
        </nav>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) { ?>
    <div class="alert alert-warning" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <h4 class="font-bold text-capitalize no-margin m-b">penelitian</h4>

            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <thead class="light-blue-100">
                        <tr>
                            <th style="width:5%" class="v-m">No.</th>
                            <th style="width:40%" class="v-m">Sub Komponen</th>
                            <th style="width:10%" class="v-m text-center">Aksi</th>
                            <th style="width:15%" class="v-m text-center">Satuan</th>
                            <th style="width:15%" class="v-m text-center">Usulan Dosen</th>
                            <th style="width:15%" class="v-m text-center">Penilaian Asesor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Hasil penelitian atau hasil pemikian yang dipublikasikan pada Jurnal internasional bereputasi/Buku Referensi (syarat buku referensi harus memuat minimal 5 artikel hasil karya penulis)</td>
                            <td class="text-center"><a href="<?= base_url($this->link) ?>penelitian_detail" class="btn btn-info r-2x btn-sm">Detail</a></td>
                            <td class="text-center">Tiap Buku</td>
                            <td class="text-center">10</td>
                            <td class="text-center">10</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Hasil penelitian atau hasil pemikian yang dipublikasikan pada Jurnal nasional terakreditasi Kemenristekdikti peringkat 1 dan 2/Buku Monograf (syarat buku monograf harus memuat minimal 3 artikel karya dari penulis)</td>
                            <td class="text-center"><a href="<?= base_url($this->link) ?>penelitian_detail" class="btn btn-info r-2x btn-sm">Detail</a></td>
                            <td class="text-center">Tiap Monograf</td>
                            <td class="text-center">10</td>
                            <td class="text-center">10</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Hasil penelitian atau hasil pemikiran yang dipublikasikan pada Jurnal internasional (Tidak bereputasi)</td>
                            <td class="text-center"><a href="<?= base_url($this->link) ?>penelitian_detail" class="btn btn-info r-2x btn-sm">Detail</a></td>
                            <td class="text-center">Tiap Jurnal</td>
                            <td class="text-center">6</td>
                            <td class="text-center">6</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>