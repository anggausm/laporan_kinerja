<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb r-3x light-blue-900">
                <li class="breadcrumb-item"><a href="<?php print base_url() ?>dashboard/"><i class="fa fa-dashboard "></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>">Rekap Kinerja</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>tambahan">Tambahan</a></li>
                <span style="float: right;">
                    <a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih"></i> &nbsp;Kembali</a>
                </span>
            </ol>
        </nav>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) { ?>
    <div class="alert alert-warning" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <h4 class="font-bold text-capitalize no-margin m-b">tugas tambahan</h4>

            <h4 class="text-capitalize no-margin m-b">jabatan struktural</h4>
            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <thead class="light-blue-100">
                        <tr>
                            <th style="width:5%" class="v-m">No.</th>
                            <th style="width:40%" class="v-m">Sub Komponen</th>
                            <th style="width:10%" class="v-m text-center">Aksi</th>
                            <th style="width:15%" class="v-m text-center">Satuan</th>
                            <th style="width:15%" class="v-m text-center">Usulan Dosen</th>
                            <th style="width:15%" class="v-m text-center">Penilaian Asesor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Rektor</td>
                            <td class="text-center"><a href="<?= base_url($this->link) ?>tambahan_detail" class="btn btn-info r-2x btn-sm">Detail</a></td>
                            <td class="text-center">Tiap Periode Jabatan</td>
                            <td class="text-center">6</td>
                            <td class="text-center">6</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Wakil Rektor</td>
                            <td class="text-center"><a href="<?= base_url($this->link) ?>tambahan_detail" class="btn btn-info r-2x btn-sm">Detail</a></td>
                            <td class="text-center">Tiap Periode Jabatan</td>
                            <td class="text-center">5</td>
                            <td class="text-center">5</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Sekretaris Universitas</td>
                            <td class="text-center"><a href="<?= base_url($this->link) ?>tambahan_detail" class="btn btn-info r-2x btn-sm">Detail</a></td>
                            <td class="text-center">Tiap Periode Jabatan</td>
                            <td class="text-center">5</td>
                            <td class="text-center">5</td>
                        </tr>
                        <tr>
                            <td>..</td>
                            <td>..</td>
                            <td class="text-center"><a href="<?= base_url($this->link) ?>tambahan_detail" class="btn btn-info r-2x btn-sm">Detail</a></td>
                            <td class="text-center">..</td>
                            <td class="text-center">..</td>
                            <td class="text-center">..</td>
                        </tr>
                        <tr>
                            <td>24</td>
                            <td>Ketua Bidang ( LPPM, UPT, BPM )</td>
                            <td class="text-center"><a href="<?= base_url($this->link) ?>tambahan_detail" class="btn btn-info r-2x btn-sm">Detail</a></td>
                            <td class="text-center">Tiap Periode Jabatan</td>
                            <td class="text-center">2</td>
                            <td class="text-center">2</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <h4 class="text-capitalize no-margin m-b">dosen pendidikan</h4>
            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <thead class="light-blue-100">
                        <tr>
                            <th style="width:5%" class="v-m">No.</th>
                            <th style="width:40%" class="v-m">Sub Komponen</th>
                            <th style="width:10%" class="v-m text-center">Aksi</th>
                            <th style="width:15%" class="v-m text-center">Satuan</th>
                            <th style="width:15%" class="v-m text-center">Usulan Dosen</th>
                            <th style="width:15%" class="v-m text-center">Penilaian Asesor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Dosen gelar akademik Doktor (S3)</td>
                            <td class="text-center"><a href="<?= base_url($this->link) ?>tambahan_detail" class="btn btn-info r-2x btn-sm">Detail</a></td>
                            <td class="text-center">Ijazah</td>
                            <td class="text-center">5</td>
                            <td class="text-center">5</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <h4 class="text-capitalize no-margin m-b">jabatan fungsional</h4>
            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <thead class="light-blue-100">
                        <tr>
                            <th style="width:5%" class="v-m">No.</th>
                            <th style="width:40%" class="v-m">Sub Komponen</th>
                            <th style="width:10%" class="v-m text-center">Aksi</th>
                            <th style="width:15%" class="v-m text-center">Satuan</th>
                            <th style="width:15%" class="v-m text-center">Usulan Dosen</th>
                            <th style="width:15%" class="v-m text-center">Penilaian Asesor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Dosen gelar Jabatan Fungsional akademik Profesor</td>
                            <td class="text-center"><a href="<?= base_url($this->link) ?>tambahan_detail" class="btn btn-info r-2x btn-sm">Detail</a></td>
                            <td class="text-center">SK JAFA</td>
                            <td class="text-center">10</td>
                            <td class="text-center">10</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>