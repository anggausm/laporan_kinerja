<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/log_book ">Validasi Log Book</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Validasi Log Book
					<?php 

					$mulai =  date("d-F-Y",strtotime($pa['pengisian_mulai']));
					$selesai =  date("d-F-Y",strtotime($pa['pengisian_selesai']));
					?>
					<button class="btn btn-sm btn-primary pull-right"><i class="fa fa-clock-o"></i> Periode Aktif : <?php echo $mulai.' s.d. '.$selesai ?> </button>
				</div>
				<div class="panel-body">
					<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/validasi_log/data" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">Nama Pegawai </label>
									<div class="col-sm-8">
										<select class="form-control select2" name="periode" required>
											<option value="">Pilih Pegawai</option>
										<?php
										foreach ($pegawai  as $vl) {
											// if($vl['nama_unit'] == $profil['nama_unit1'])
											?>
											<option value="<?= $vl['user_key'] ?>" <?php if($vl['user_key'] == $userkey_pegawai) { print 'selected'; } ?>><?= $vl['nama_doskar'] ?></option>
										<?php }?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-5">
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">Periode Log </label>
									<div class="col-sm-8">
										<select class="form-control select2" name="periode" required>
											<option value="">Pilih Periode Log</option>
										<?php
										foreach ($p_log  as $val) {?>
											<option value="<?= $val['id'] ?>"  <?php if($val['id'] == $log_mg_cosen) { print 'selected'; } ?>>Minggu <?= $val['nama_log'] ?> (<?= date("d-M-Y",strtotime($val['tgl_awal'])) ?> s.d. <?= date("d-M-Y",strtotime($val['tgl_akhir'])) ?>)</option>
										<?php }?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-1">
								<button class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</button>
							</div>
						</div>
					</form>

					<hr>
					<div class="row">
						<div class="col-md-12">
							<div class="btn-groups">
								
								<div class="table-responsive">
									<table class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:10%" class="text-center">Tanggal</th>
												<th style="width:25%" class="text-center">Isi Log Book</th>
												<th style="width:15%" class="text-center">Ket</th>
												<th style="width:10%" class="text-center">Status Pekerjaan</th>
												
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 1;
											foreach ($data_log as $key => $value) {
											    ?>
											<tr>
												<td class="text-center"><?=$no?></td>
												<td><?php echo date("d-F-Y",strtotime($value['tanggal_log_book'])); ?></td>
												<td style="text-align: left;"><?php echo $value['tugas'];?></td>
												<td style="text-align: left;"><?php echo $value['ket'];?></td>
												<td class="text-center">
													<?php if($value['status_pekerjaan'] == '1') {
														echo 'Selesai';
													} else if($value['status_pekerjaan'] == '2') {
														echo 'Berlanjut';
													} else { 
														echo 'Tidak Selesai';
													}
        											?>
												</td>
												<!-- <td class="text-center">
													<a href="<?php echo base_url() ?>kinerja/log_book/edit/<?php echo enc_data($value['id_log_book']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-edit"></i> </button></a>
													<a href="<?php echo base_url() ?>kinerja/log_book/detail/<?php echo enc_data($value['id_log_book']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-list"></i> </button></a>
												</td> -->
											</tr>
											<?php
											$no++;
											}
											?>
										</tbody>
									</table>
									<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/validasi_log/simpan" enctype="multipart/form-data">
										<input type="hidden" name="user_pegawai" value="<?= $userkey_pegawai; ?>"> 
										<input type="hidden" name="periode_log" value="<?= $log_mg_cosen; ?>"> 
										<input type="hidden" name="periode_kin" value="<?= $kinerja_periode; ?>"> 
										<span>Klik pada indikator yang sesuai dengan kriteria pekerjaan yang telah dilakukan pegawai :</span><br>
										<input type="checkbox" id="ind_1" name="ind_1" value="1"> Menyelesaikan pekerjaan dengan benar<br>
								<input type="checkbox" id="ind_2" name="ind_2" value="1"> Menyelesaikan pekerjaan tepat waktu<br>
								<input type="checkbox" id="ind_3" name="ind_3" value="1"> Bersedia menjalankan tugas tambahan yang relevan sesuai dengan pekerjaan<br>
								<input type="checkbox" id="ind_4" name="ind_4" value="1"> Kesesuaian spesifikasi pekerjaan
								<hr>
									<button class="btn btn-md btn-success pull-right" type="submit"><i class="fa fa-check-square-o"></i> Validasi Log Book</button>										
								</div>
								</form>
							</div>
							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>