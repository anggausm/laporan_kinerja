<div id="app">
  <div class="row">
    <div class="col-sm-12">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
          <li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/pegawai">Dashboard Penilaian Kinerja</a></li>
          <!-- <li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/pegawai/tambah_sertifikat">Tambah Sertifikat</a></li> -->
          <span  style="float: right;">
            <a href="<?php print base_url()?>kinerja/pegawai"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
          </span>
        </ol>
      </nav>
    </div>
  </div>
  <?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
      <?php elseif ($this->session->flashdata('error')): ?>
          <div class="alert alert-danger text-center" role="alert">
              <?php print $this->session->flashdata('error')?> 
          </div>
  <?php endif ?>
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading bg-white">
        Dokumen Surat Keputusan<br>
        <small class="text-muted"> </small>
      </div>
      <div class="panel-body">

        <div class="md-whiteframe-z0 bg-white">
        <ul class="nav nav-md nav-tabs nav-lines b-info">
          <li class="active">
            <a href="" data-toggle="tab" data-target="#tab_1" aria-expanded="false"><i class="fa fa-file"></i> SK Pegawai</a>
          </li>
          <li class="">
            <a href="" data-toggle="tab" data-target="#tab_2" aria-expanded="false"><i class="fa fa-file"></i> SK Panitia</a>
          </li>
        </ul>
        <div class="tab-content p m-b-md b-t b-t-2x">
          <div role="tabpanel" class="tab-pane animated fadeIn active" id="tab_1">
           <a href="https://apps.usm.ac.id/kpg/file/Panduan_presensi_WFH.pdf">
            <button class="btn btn-info"><i class="fa fa-plus"></i> Tambah SK Pegawai</button>
          </a>
          <div class="table-responsive">
                  <table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
                    <thead>
                      <tr>
                        <th style="width:15%" class="text-center">No</th>
                        <th style="width:70%" class="text-center">Rincian SK</th>
                        <th style="width:15%" class="text-center">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      foreach ($sk_pegawai as $value) {
                          ?>
                      <tr>
                        <td class="text-center"><?=$no?></td>
                        <td style="text-align: left;">SK No. <?php echo $value['no_sk'];?> tentang <br><?php echo $value['judul_sk'];?>
                          <br>Tanggal <?php echo format_indo(date("Y-m-d",strtotime($value['tanggal_sk'])));?></td>
                        <td class="text-center">
                          <a href="<?php echo base_url() ?>kinerja/pegawai/edit_sk_panitia/<?php echo enc_data($value['id_pegawai']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-edit"></i> </button></a>
                          <!-- <a href="<?php echo base_url() ?>kinerja/pegawai/detail/<?php echo enc_data($value['id_pegawai']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-list"></i> </button></a> -->
                        </td>
                      </tr>
                      <?php
                      $no++;
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
          </div>
          <div role="tabpanel" class="tab-pane animated fadeIn active" id="tab_2">
           <a href="https://apps.usm.ac.id/kpg/file/PANDUAN_MENGATASI_ERROR.pdf">
            <button class="btn btn-info"><i class="fa fa-plus"></i> Tambah SK Panitia</button>
            <div class="table-responsive">
                  <table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
                    <thead>
                      <tr>
                        <th style="width:15%" class="text-center">No</th>
                        <th style="width:70%" class="text-center">Rincian SK</th>
                        <th style="width:15%" class="text-center">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      foreach ($sk_panitia as $value) {
                          ?>
                      <tr>
                        <td class="text-center"><?=$no?></td>
                        <td style="text-align: left;">SK No. <?php echo $value['no_sk'];?> tentang <br><?php echo $value['judul_sk'];?>
                          <br>Tanggal <?php echo format_indo(date("Y-m-d",strtotime($value['tanggal_sk'])));?></td>
                        <td class="text-center">
                          <a href="<?php echo base_url() ?>kinerja/pegawai/edit_sk_panitia/<?php echo enc_data($value['id_pegawai']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-edit"></i> </button></a>
                          <!-- <a href="<?php echo base_url() ?>kinerja/pegawai/detail/<?php echo enc_data($value['id_pegawai']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-list"></i> </button></a> -->
                        </td>
                      </tr>
                      <?php
                      $no++;
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
          </a>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>
</div>