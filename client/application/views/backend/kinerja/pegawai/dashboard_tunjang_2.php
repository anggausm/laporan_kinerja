<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/pegawai ">Penilaian Pegawai</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="row">
                <div class="col-sm-3">
                    <div class="font-bold text-u-c">
                        <i class="icon mdi-action-view-list i-20"></i> Penilaian Pegawai
                    </div>
                </div>
                <div class="col-sm-9">
                    <form action="<?php print base_url()?>kinerja/pegawai/dashboard" class="form-inline text-right" method='post'>
                       <select class="form-control select2" name="periode" required>
											<option>Pilih Periode</option>
										<?php
										foreach ($periode  as $vl) {?>
											<option value="<?= $vl['id_periode'] ?>" <?php if($vl['id_periode'] == $p_aktif['id_periode']) { print 'selected'; }?>><?= $vl['nama_periode'] ?></option>
										<?php }?>
										</select>
                        <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-plus"></i> Tampilkan </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
	
	<?php
        $hariini = date('Y-m-d'); 
      
						$mulai = format_indo(date("Y-m-d",strtotime($p_aktif['pengisian_mulai'])));
						$selesai = format_indo(date("Y-m-d",strtotime($p_aktif['pengisian_selesai'])));
    ?>
	
	<div class="row">
		<div class="col-md-8">
			
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Satuan Kinerja Pegawai
							
				</div>
				<div class="panel-body">
					<?php if (date('Y-m-d') >= $mulai && date('Y-m-d') <= $selesai) { ?>
					<h4>Periode Pengisian : <?php echo $mulai; ?> s.d. <?php echo $selesai; ?></h4>
								<div class="table-responsive">
									<table class="table table-hover">
										<!-- <thead>
											<tr>
												<th style="width:5%" class="text-center">Indikator</th>
												<th style="width:40%" class="text-center">Rincian SKP</th>
												<th style="width:10%" class="text-center">Aksi</th>
											</tr>
										</thead> -->
										<tbody>
											<?php
											// $no = 1;
											// foreach ($edit as $skp) {
											//     ?>
											<tr>
												<td style="width:10%" class="text-center">
													<h4>I. </h4>
												</td>
												<td style="width:40%" class="text-left"><h4>Pelaksanaan Tugas  Utama </h>
																								</td>
												<td style="width:12.5%" class="text-right">
													Aksi
												</td>
												<td style="width:12.5%" class="text-right">
													Skor
												</td>
												<td style="width:12.5%" class="text-right">
													Bobot
												</td>
												<td style="width:12.5%" class="text-right">
													Nilai
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-left">
												</td>
												<td style="width:40%" class="text-left">
													1. Kesesuaian pekerjaan:
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%" class="text-left">   a. Menyelesaikan pekerjaan dengan benar</td>
												<td style="width:12.5%" class="text-right">
													<a href="<?php echo base_url() ?>kinerja/log_book/index"><button class="btn btn-md btn-default"><i class="fa fa-edit"></i> </button></a>
													</td>
												<td style="width:12.5%" class="text-right"><?php 
													$jml_log = count($log_book);
													$hari_kerja = $hk['jumlah_hari_kerja'];
													$skor_1 = floor($jml_log/$hari_kerja);
													if(!empty($log_book)){
														$skor_1 = '1';
													} else {
														$skor_1 = '0';
													}
													$bobot_1 = "0.16";
														$nil1 = $skor_1*$bobot_1; 
													?>
													<button class="btn btn-md btn-default"><?php echo $skor_1; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_1;?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"> <?php echo $nil1; ?> </button>
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left">   b. Menyelesaikan pekerjaan tepat waktu</td>
												<td style="width:12.5%" class="text-right">
											
													<?php 
														$jml_log = count($log_book);
													if(!empty($jml_log)){
														$skor_2 = '1';
													} else {
														$skor_2 = '0';
													}

													$bobot_2 = "0.16";
														$nil2 = $skor_1*$bobot_2; 
													?>
													<a href="<?php echo base_url() ?>kinerja/log_book/index"><button class="btn btn-md btn-default"><i class="fa fa-edit"></i> </button></a>
												</td>
												<td style="width:12.5%" class="text-right"><button class="btn btn-md btn-default"><?php echo $skor_1; ?></button></td>
												<td style="width:12.5%" class="text-right"><button class="btn btn-md btn-default"><?php echo $bobot_2;?></button> </td>
												<td style="width:12.5%" class="text-right"><button class="btn btn-md btn-default"><?php echo $nil2; ?></button> </td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left">   c. Bersedia menjalankan tugas tambahan yang relevan</td>
												<td style="width:12.5%" class="text-right">
													<?php 
														$jml_log = count($log_book);
													if(!empty($jml_log)){
														$skor_3 = '1';
													} else {
														$skor_3 = '0';
													}

													$bobot_3 = "0.14";
														$nil3 = $skor_1*$bobot_3; 
													?>
													<a href="<?php echo base_url() ?>kinerja/log_book/index"><button class="btn btn-md btn-default"><i class="fa fa-edit"></i> </button></a>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_1; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"> <?php echo $bobot_3; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $nil3; ?></button> 
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left">b. Penilaian Kepuasan Layanan</td>
												<td style="width:12.5%" class="text-right">												
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 1. Kuesioner dari atasan langsung dan teman sejawat </td>
												<td style="width:12.5%" class="text-right">
													<?php if(!empty($kuesioner)){
														$skor_4 = '1';
													} else {
														$skor_4 ='0';
														
													}
													$bobot_4 = "0.20";
														$nil4 = $skor_4*$bobot_4; 
													?>
													<a href="<?php echo base_url() ?>kinerja/pegawai/kuesioner"><button class="btn btn-md btn-default"><i class="fa fa-edit"></i> </button></a>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_4; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_4; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $nil4; ?></button> 
												</td>

											</tr>
											<tr>
												<td style="width:10%" class="text-center">
													<h4>II. </h4>
												</td>
												<td style="width:40%"class="text-left"><h4>Core Values </h4></td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 1. Kehadiran Fingerprint </td>
												<td style="width:12.5%" class="text-right">
													<?php if(!empty($finger)){
														$skor_5 = '1';
													} else {
														$skor_5 ='0';
														
													} 
													$bobot_5 = "0.17";
														$nil5 = $skor_5*$bobot_5; 
													?>
													<button class="btn btn-md btn-default"><i class="mdi mdi-device-access-time"></i></button>
													
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_5; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_5; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $nil5; ?></button> 
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 2. Menyelesaikan Pekerjaan dengan Tuntas 
																								</td>
												<td style="width:12.5%" class="text-right">
													<?php if(!empty($log_book)){
														$skor_6 = '1';
													} else {
														$skor_6 ='0';
														
													} 
													$bobot_6 = "0.17";
														$nil6 = $skor_6*$bobot_6; 
													?>
													<a href="<?php echo base_url() ?>kinerja/pegawai/kuesioner"><button class="btn btn-md btn-default"><i class="fa fa-edit"></i> </button></a>
													
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_6; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_6; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $nil6; ?></button> 
												</td>
											</tr>
											</tr>
												<td style="width:10%" class="text-center"> </td>
												<td style="width:40%"class="text-left">Total Nilai </td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
													<b><?php $tot = $nil1 + $nil2 + $nil3 + $nil4 + $nil5 + $nil6; 
													echo $tot; ?></b>
												</td>
											<tr>
										</tbody>
									</table>
									
								</div>

								<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/pegawai/simpan_ajuan" enctype="multipart/form-data">
										
										<input type="hidden" name="skor1" value="<?php echo number_format($skor_1, 2); ?>" >
										<input type="hidden" name="bobot1" value="<?php echo $bobot_1;?>" >
										<input type="hidden" name="nilai1" value="<?php echo number_format($nil1, 2); ?>" >

									
										<input type="hidden" name="skor2" value="<?php echo number_format($skor_2,2); ?>" >
										<input type="hidden" name="bobot2" value="<?php echo $bobot_2;?>" >
										<input type="hidden" name="nilai2" value="<?php echo number_format($nil2,2); ?>" >

										
										<input type="hidden" name="skor3" value="<?php echo number_format($skor_3,2); ?>" >
										<input type="hidden" name="bobot3" value=" <?php echo $bobot_3; ?>" >
										<input type="hidden" name="nilai3" value="<?php echo number_format($nil3,2); ?>" >

									
										<input type="hidden" name="skor4" value="<?php echo number_format($skor_4,2); ?>" >
										<input type="hidden" name="bobot4" value=" <?php echo $bobot_4; ?>" >
										<input type="hidden" name="nilai4" value="<?php echo number_format($nil4,2); ?>" >

									
										<input type="hidden" name="skor5" value="<?php echo number_format($skor_5,2); ?>" >
										<input type="hidden" name="bobot5" value="<?php echo $bobot_5; ?>" >
										<input type="hidden" name="nilai5" value="<?php echo number_format($nil5,2); ?>" >

									
										<input type="hidden" name="skor6" value="<?php echo $skor_6; ?>" >
										<input type="hidden" name="bobot6" value="<?php echo $bobot_6; ?>" >
										<input type="hidden" name="nilai6" value="<?php echo number_format($nil6,2); ?>" >

										<input type="hidden" name="ttl_nilai" value="<?php echo number_format($tot, 3); ?>" >
										<input type="hidden" name="id_periode" value="<?php echo $p_aktif['id_periode']; ?>" > 
								<div class="panel-footer bg-white">
									<?php if(empty($cek_skp_peg)){ ?>
									<?php if($tot < 0.6){ ?>
									<span  class="label red">Syarat penilaian belum terpenuhi, silahkan cek Kembali berkas yang harus di ajukan!</span>	
								<?php } else { ?>
										<span  class="label green">Syarat penilaian sudah terpenuhi, silahkan submit pengajuan </span>
									<?php
								} ?>
									<button class="btn btn-md btn-success pull-right"  onclick="return confirm('PASTIKAN ANDA MELAKUKAN PENGISIAN PENILAIAN KINERJA DENGAN BENAR. \n \nSETELAH MELAKUKAN SUBMIT ANDA TIDAK DAPAT MELAKUKAN PERUBAHAN PENGISIAN KINERJA. \n \nApakah Anda yakin akan menyimpan penilaian kinerja ini ???');"><i class="fa fa-paper-plane"></i> Submit Pengajuan</button>
											<?php } else { echo 'Penilaian Kinerja sudah diajukan'; } ?>
								</div>
							</form>
							<?php } else { echo '<h2>Periode Pengisian Penilaian telah berakhir</h2>'; } ?>
				</div>
			</div>
			
		</div>
		<div class="col-md-4">
			
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Biodata Pegawai
							
				</div>
				<div class="panel-body">
					<form class="form" method="POST" action="<?php echo base_url() . $this->link . 'simpan' ?>">
					
								<h4 class="font-bold text-capitalize no-margin m-b">Data Pegawai</h4>

            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <tbody>
			                    	<tr>
			                            <td style="width:30%">Nama</td>
			                            <td style="width:70%"><?php echo $personel['nama_doskar']; ?></td>
			                        </tr>
			                        <tr>
			                            <td style="width:30%">NIS</td>
			                            <td style="width:70%"><?php echo $personel['nis']; ?></td>
			                        </tr>
			                        <tr>
			                            <td style="width:30%">Pekerjaan</td>
			                            <td style="width:70%"><?php echo $personel['pekerjaan']; ?></td>
			                        </tr>
			                        <tr>
			                            <td style="width:30%">Unit</td>
			                            <td style="width:70%"><?php echo $personel['nama_unit']; ?></td>
			                        </tr>
			                        <tr>
			                            <td style="width:30%">TMT </td>
			                            <td style="width:70%"><?php echo $personel['tmt_usm']; ?></td>
			                        </tr>
			                        <tr>
			                            <td style="width:30%">Status Kerja</td>
			                            <td style="width:70%"><?php echo $personel['status_kerja']; ?></td>
			                        </tr>
			                    </tbody>
                </table>
            </div>
							</form>
				</div>
			</div>
		</div>
			
	</div>

 
