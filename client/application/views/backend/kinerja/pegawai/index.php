<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/pegawai ">Penilaian Pegawai</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="row">
                <div class="col-sm-3">
                    <div class="font-bold text-u-c">
                        <i class="icon mdi-action-view-list i-20"></i> Penilaian Pegawai
                    </div>
                </div>
                <div class="col-sm-9">
                    <form action="<?php print base_url()?>kinerja/pegawai/dashboard" class="form-inline text-right" method='post'>
                       <select class="form-control select2" name="periode" required>
											<option>Pilih Periode</option>
										<?php
										foreach ($periode  as $vl) {?>
											<option value="<?= $vl['id_periode'] ?>"><?= $vl['nama_periode'] ?></option>
										<?php }?>
										</select>
                        <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-eye"></i> Tampilkan </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> 

<!-- 	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Penilaian Pegawai
				</div>
				<div class="panel-body">
					<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/pegawai/dashboard" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group ">
									<label class="col-sm-6 control-label text-right">Periode </label>
									<div class="col-md-6">
										<select class="form-control select2" name="periode" required>
											<option>Pilih Periode</option>
										<?php
										foreach ($periode  as $vl) {?>
											<option value="<?= $vl['id_periode'] ?>"><?= $vl['nama_periode'] ?></option>
										<?php }?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<button class="btn btn-info btn-sm"><i class="fa fa-eye"></i> Lihat</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div> -->

<div class="row">
		<div class="col-md-8">
			
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  <b>Master Dokumen Pegawai</b>
							
				</div>
				<div class="panel-body">
					<div class="row">
            <div class="col-sm-3 col-xs-6">
                <a href="<?= base_url($this->link) ?>tambah_ijazah" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                    <div class="text-center">
                        <i class="fa fa-mortar-board fa-3x text-primary"></i>
                        <p class="no-margin m-t font-bold">Pendidikan</p>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 col-xs-6">
                <a href="<?= base_url($this->link) ?>tambah_sertifikat" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                    <div class="text-center">
                        <i class="fa fa-certificate fa-3x text-primary"></i>
                        <p class="no-margin m-t font-bold">Sertifikat</p>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 col-xs-6">
                <a href="<?= base_url($this->link) ?>tambah_sk_pegawai" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                    <div class="text-center">
                        <i class="fa fa-file-text-o fa-3x text-primary"></i>
                        <p class="no-margin m-t font-bold text-ellipsis">SK Pegawai</p>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 col-xs-6">
                <a href="<?= base_url($this->link) ?>tambah_sk_panitia" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                    <div class="text-center">
                        <i class="fa fa-file-text-o fa-3x text-primary"></i>
                        <p class="no-margin m-t font-bold text-ellipsis">SK Panitia</p>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 col-xs-6">
                <a href="<?= base_url() ?>kinerja/log_book" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                    <div class="text-center">
                        <i class="fa fa-list-alt fa-3x text-primary"></i>
                        <p class="no-margin m-t font-bold text-ellipsis">Log Book</p>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 col-xs-6">
                <a href="<?= base_url() ?>kinerja/lpp" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                    <div class="text-center">
                        <i class="fa fa-list-ul fa-3x text-primary"></i>
                        <p class="no-margin m-t font-bold text-ellipsis">LPP</p>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 col-xs-6">
                <a href="<?= base_url($this->link) ?>kuesioner" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                    <div class="text-center">
                        <i class="fa fa-check-circle-o fa-3x text-primary"></i>
                        <p class="no-margin m-t font-bold">Kuesioner</p>
                    </div>
                </a>
            </div>
        </div>
				</div>
			</div>
			
		</div>
		<div class="col-md-4">
			
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  <b>Biodata Pegawai</b>
							
				</div>
				<div class="panel-body">
					<form class="form" method="POST" action="<?php echo base_url() . $this->link . 'simpan' ?>">

            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <tbody>
                    	<tr>
                            <td style="width:30%">Nama</td>
                            <td style="width:70%"><?php echo $personel['nama_doskar']; ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%">NIS</td>
                            <td style="width:70%"><?php echo $personel['nis']; ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%">Jabatan</td>
                            <td style="width:70%"><?php echo $personel['nama_jabatan']; ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%">Unit</td>
                            <td style="width:70%"><?php echo $personel['nama_unit1']; ?></td>
                        </tr>
                      <!--   <tr>
                            <td style="width:30%">Golongan</td>
                            <td style="width:70%"><?php echo $personel->gol_baru; ?></td>
                        </tr> -->
                        <tr>
                            <td style="width:30%">TMT </td>
                            <td style="width:70%"><?php echo $personel['tmt_usm']; ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%">Status Kerja</td>
                            <td style="width:70%"><?php echo $personel['status_kerja']; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
							</form>
				</div>
			</div>
		</div>
			
	</div>