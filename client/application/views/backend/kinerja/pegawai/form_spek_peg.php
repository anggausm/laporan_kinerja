<div id="app">
    <div class="row">
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
                    <li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/pegawai ">Penilaian Pegawai</a></li>
                    <span  style="float: right;">
                        <a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
                    </span>
                </ol>
            </nav>
        </div>
    </div>
    <?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
        <?php elseif ($this->session->flashdata('error')): ?>
            <div class="alert alert-danger text-center" role="alert">
                <?php print $this->session->flashdata('error')?> 
            </div>
    <?php endif ?>

    <?php
    
                        foreach($rekap_log as $rlog){
                            $log1 = $rlog['p1'];
                            $log2 = $rlog['p2'];
                            $log3 = $rlog['p3'];
                            $log4 = $rlog['p4'];
                            $log5 = $rlog['p5'];
                            $log6 = $rlog['p6'];
                        }

                        $jml_log = count($periode_log);
    ?>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p animated fadeIn">
            <h4 class="font-bold text-c-l no-margin m-b">Syarat Berkas : Indikator Kesesuaian spesifikasi pekerjaan</h4>

            <div class="table-responsive">
                <table class="table mb-0">
                    <thead>
                        <tr>
                            <th style="width:25%" class="text-center">Berkas</th>
                            <th style="width:25%" class="text-center">File Diupload</th>
                            <th style="width:25%" class="text-center">Skor File</th>
                            <th style="width:25%" class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="width:25%" class="text-left">SK Pegawai</td>
                            <td style="width:25%" class="text-left">: <?php 
                                

                            if(!empty($sk_pegawai)){
                                foreach($sk_pegawai as $sk){
                                    $surat = $sk['judul_sk'];
                                    $nomor = $sk['no_sk'];
                                } 
                                 echo $surat.' <br>No. '.$nomor; 

                                } else { echo 'SK belum di upload'; } ?></td>
                            <td style="width:25%" class="text-center"> <?php 
                                if(!empty($sk_pegawai)){
                                    $skor1 = '0.25';
                                } else {
                                    echo $skor1 = '0';
                                    } ?>
                                        <?php echo $skor1;?>
                                    </td>
                            <td style="width:25%" class="text-center">
                                <?php if(empty($sk_pegawai)){ ?>
                                <a class="badge bg-danger" href="<?= base_url() ?>kinerja/pegawai/tambah_sk_pegawai">Belum Lengkap</a>
                                <?php } else { ?>
                                    <a class="badge bg-green" href="<?= base_url() ?>kinerja/pegawai/tambah_sk_pegawai">Sudah Lengkap</a>
                                    <?php
                                } ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:25%" class="text-left">Ijazah terakhir</td>
                            <td style="width:25%" class="text-left">: <?php 
                                foreach($ijazah as $ijaz){
                                    $jenjang = $ijaz['jenjang_pendidikan'];
                                    $instansi = $ijaz['nama_instansi'];
                                }
                            ?> <?php echo $jenjang.' - '.$instansi; ?></td>
                            <td style="width:25%" class="text-center"> 
                                <?php 
                                if(!empty($ijazah)){
                                    $skor2 = '0.25';
                                } else {
                                    echo $skor2 = '0';
                                    } ?>
                                       <?php echo $skor2; ?></td>
                            <td style="width:25%" class="text-center">
                               <?php if(empty($ijazah)){ ?>
                                <a class="badge bg-danger" href="<?= base_url() ?>kinerja/pegawai/tambah_ijazah">Belum Lengkap</a>
                                <?php } else { ?>
                                    <a class="badge bg-green" href="<?= base_url() ?>kinerja/pegawai/tambah_ijazah">Sudah Lengkap</a>
                                    <?php
                                } ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:25%" class="text-left">Sertifikat Kompetensi</td>
                            <td style="width:25%" class="text-left">: 
                                <?php 
                                foreach($sert_komp as $sert){
                                    $nama_sertif = $sert['nama_sertifikat'];
                                    $instansi = $sert['penyelenggara'];
                                }
                            ?><?php echo $nama_sertif.' '.$instansi; ?></td>
                            <td style="width:25%" class="text-center"> 
                                <?php 
                                if(!empty($sert_komp)){
                                    $skor3 = '0';
                                } else {
                                    echo $skor3 = '0';
                                } 
                                 ?></td>
                            <td style="width:25%" class="text-center">
                                <?php if(empty($sert_komp)){ ?>
                                <a class="badge bg-danger" href="<?= base_url() ?>kinerja/pegawai/tambah_sertifikat">Belum Lengkap</a>
                                <?php } else { ?>
                                    <a class="badge bg-green" href="<?= base_url() ?>kinerja/pegawai/tambah_sertifikat">Sudah Lengkap</a>
                                    <?php
                                } ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:25%" class="text-left">Log Book</td>
                            <td style="width:25%" class="text-left">: </td>
                            <td style="width:25%" class="text-center"> 
                                 <?php 
                                 $skor_log4 = $log4/$jml_log;
                                 if($skor_log4 >= 0.75 && $jenis_peg == 2){
                                    $skor4 = '0.25';
                                 } else if($skor_log4 >= 0.75 && $jenis_peg == 1){
                                    $skor4 = '0.50';
                                    } else {
                                      $skor4 = '0';
                                    }?>
                                    <?php echo number_format($skor4, 2); ?></td>
                            <td style="width:25%" class="text-center"> 
                                <?php if(empty($log_book)){ ?>
                                <a class="badge bg-danger" href="<?= base_url() ?>kinerja/log_book">Belum Lengkap</a>
                                <?php } else { ?>
                                    <a class="badge bg-green" href="<?= base_url() ?>kinerja/log_book">Sudah Lengkap</a>
                                    <?php
                                } ?>
                            </td>
                        </tr>

                        <?php
                        if($jenis_peg == 2){ ?>
                            <tr>
                            <td style="width:25%" class="text-left">LPP</td>
                            <td style="width:25%" class="text-left"></td>
                            <td style="width:25%" class="text-center"> 
                                 <?php 
                                if(!empty($lpp)){
                                    $skor5 = '0.25';
                                } else {
                                    echo $skor5 = '0';
                                    } ?><?php echo $skor5; ?></td>
                            <td style="width:25%" class="text-center"> 
                                <?php if(empty($lpp)){ ?>
                                <a class="badge bg-danger" href="<?= base_url() ?>kinerja/lpp">Belum Lengkap</a>
                                <?php } else { ?>
                                    <a class="badge bg-green" href="<?= base_url() ?>kinerja/lpp">Sudah Lengkap</a>
                                    <?php
                                } ?>
                            </td>
                        </tr><?php
                            }
                            ?>
                    </tbody>
                </table>
            </div>
            <span>Total Skor : 
            <?php 
                 if($jenis_peg   == 2){
                    $skor_final = $skor1 + $skor2 + $skor3 + $skor4 + $skor5 ;
                } else {
                    $skor_final = $skor1 + $skor2 + $skor3 + $skor4 ;
                    }
                    echo $skor_final;         
                    ?>

        </span>
            <!-- <a href="#" class="btn btn-block btn-info">Ajukan <i class="fa fa-send-o"></i></a> -->
        </div>

    </div>
</div>

</div>