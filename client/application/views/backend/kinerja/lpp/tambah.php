<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/lpp"><?php print $this->title; ?></a></li>
					<span  style="float: right;">
						<a href="<?php print base_url()?>kinerja/lpp"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-plus "></i> Tambah <?php print $this->title; ?>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/lpp/simpan" enctype="multipart/form-data">
							<div class="form-group">
								<input type="text" name="periode_aktiff" value="<?php echo $pa['id_periode']; ?>">
	                            <label for="" class="col-sm-3 control-label">Periode Laporan</label>
	                            <div class="col-sm-2">
	                               <select class="form-control" name="bulan">
											<option value="01" <?php if ($bulan == '01') {print 'selected=""';}?>>Januari</option>
											<option value="02" <?php if ($bulan == '02') {print 'selected=""';}?> >Februari</option>
											<option value="03" <?php if ($bulan == '03') {print 'selected=""';}?> >Maret</option>
											<option value="04" <?php if ($bulan == '04') {print 'selected=""';}?> >April</option>
											<option value="05" <?php if ($bulan == '05') {print 'selected=""';}?> >Mei</option>
											<option value="06" <?php if ($bulan == '06') {print 'selected=""';}?> >Juni</option>
											<option value="07" <?php if ($bulan == '07') {print 'selected=""';}?> >Juli</option>
											<option value="08" <?php if ($bulan == '08') {print 'selected=""';}?> >Agustus</option>
											<option value="09" <?php if ($bulan == '09') {print 'selected=""';}?> >September</option>
											<option value="10" <?php if ($bulan == '10') {print 'selected=""';}?> >Oktober</option>
											<option value="11" <?php if ($bulan == '11') {print 'selected=""';}?> >November</option>
											<option value="12" <?php if ($bulan == '12') {print 'selected=""';}?> >Desember</option>
										</select>
	                            </div>
	                             <div class="col-sm-2">
	                              <select class="form-control" name="tahun">
											<option value="2024" <?php if ($tahun == '2024') {print 'selected=""';}?> >2024</option>
										</select>
	                            </div>
	                        </div>
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Ringkasan Pekerjaan </label>
								<div class="col-sm-9">
									<textarea name="tugas" placeholder="Isi Tugas / Ringkasan Pekerjaan" class="form-control" rows="5" ></textarea>
								</div>
							</div>
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right"> Status Pekerjaan </label>
								<div class="col-sm-9">
									<select class="form-control select2" name="stat_kerja" required>
										<option value="">Pilih Status Pekerjaan</option>
										<?php foreach ($stat_kerja as $stat): ?>
										<option value="<?php echo $stat['id_stat'] ?>">
											<?php echo $stat['nama_stat'] ?>
										</option>
										<?php endforeach?>
									</select>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Keterangan / Tugas Tambahan</label>
								<div class="col-sm-9">
									<textarea name="keterangan" placeholder="Tuliskan Keterangan atau Tugas Tambahan" class="form-control" rows="5" ></textarea>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">File Laporan</label>
								<div class="col-sm-9">
									<input type="file" name="file_lpp" class="form-control">
								</div>
							</div>
							<hr>
							<div class="col-sm-4"></div> 
							<div class="col-sm-4">
								<button class="btn btn-info btn-sm">
									<i class="fa fa-save"></i> Simpan 
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
	</div>
</div>