<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> dudoks  <![endif]-->
<!--[if gt IE 8]> Pangkalan Data Universitas Semrang<!-->
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <title>Portal Universitas Semarang</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="College Education Responsive Template">
    <meta name="author" content="Esmet">
    <meta charset="UTF-8">

    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800' rel='stylesheet' type='text/css'>

    <!-- CSS Bootstrap & Custom -->
    <link href="<?php print base_url();?>assets/frondend/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="<?php print base_url();?>assets/frondend/css/font-awesome.min.css" rel="stylesheet" media="screen">
    <link href="<?php print base_url();?>assets/frondend/css/animate.css" rel="stylesheet" media="screen">

    <link href="<?php print base_url();?>assets/frondend/style.css" rel="stylesheet" media="screen">

    <!-- Favicons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php print base_url();?>assets/frondend/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php print base_url();?>assets/frondend/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php print base_url();?>assets/frondend/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php print base_url();?>assets/frondend/images/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?php print base_url();?>assets/frondend/images/ico/favicon.png">

    <!-- JavaScripts -->
    <script src="<?php print base_url();?>assets/frondend/js/jquery-1.10.2.min.js"></script>
    <script src="<?php print base_url();?>assets/frondend/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php print base_url();?>assets/frondend/js/modernizr.js"></script>
    <!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
            <a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" alt="" /></a>
        </div>
    <![endif]-->
</head>
<body>

   <?php
print $header;

?>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
            	<?php print $konten;?>
            </div> <!-- /.col-md-12 -->

            <div class="col-md-4">
                <?php print $sidebar;?>
            </div> <!-- /.col-md-4 -->
        </div>
    </div>




    <!-- begin The Footer -->
     <?php print $footer;?>


    <script src="<?php print base_url();?>assets/frondend/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php print base_url();?>assets/frondend/js/plugins.js"></script>
    <script src="<?php print base_url();?>assets/frondend/js/custom.js"></script>

</body>
</html>
