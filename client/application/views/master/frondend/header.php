 <!-- This one in here is responsive menu for tablet and mobiles -->
    <div class="responsive-navigation visible-sm visible-xs">
        <a href="#" class="menu-toggle-btn">
            <i class="fa fa-bars"></i>
        </a>
       <div class="responsive_menu">
            <ul class="main_menu">
                <li><a href="<?php print base_url();?>">Home</a></li>
                <li><a href="<?php print base_url() . 'registrasi/mahasiswa'?>">Registrasi</a></li>
                <!--li><a href="#">Info Mahasiswa</a>
                    <ul>
                        <li><a href="events-grid.html">Jadwal Kuliah</a></li>
                        <li><a href="events-list.html">Jadwal Dosen</a></li>
                    </ul>
                </li>
                <li><a href="#">Panduan SIA</a> </li>
                <li><a href="#">Registrasi </a>
                    <ul>
                        <li><a href="blog.html">Blog Grid</a></li>
                        <li><a href="blog-single.html">Blog Single</a></li>
                        <li><a href="blog-disqus.html">Blog Disqus</a></li>
                    </ul>
                </li>
                <li><a href="">Pages</a>
                    <ul>
                        <li><a href="archives.html">Archives</a></li>
                        <li><a href="shortcodes.html">Shortcodes</a></li>
                        <li><a href="gallery.html">Our Gallery</a></li>
                    </ul>
                </li>
                <li><a href="contact.html">Contact</a></li-->
            </ul> <!-- /.main_menu -->
            <ul class="social_icons">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-rss"></i></a></li>
            </ul> <!-- /.social_icons -->
        </div> <!-- /.responsive_menu -->
    </div> <!-- /responsive_navigation -->


    <header class="site-header">
        <div class="container">
            <div class="row">
                <div class="col-md-4 header-left">
                    <p><i class="fa fa-phone"></i>  (024) 6702757</p>
                    <p><i class="fa fa-envelope"></i> <a href="mailto:email@universe.com">pdpt@usm.ac.id</a></p>
                </div> <!-- /.header-left -->

                <div class="col-md-4">
                    <div class="logo">
                        <a href="index.html" title="Universe" rel="home">
                            <img src="<?php print base_url()?>assets/frondend/images/logo.png" alt="Universe" width ='90%'>
                        </a>
                    </div> <!-- /.logo -->
                </div> <!-- /.col-md-4 -->

                <!--div class="col-md-4 header-right">
                    <ul class="small-links">
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Apply Now</a></li>
                    </ul>
                    <div class="search-form">
                        <form name="search_form" method="get" action="#" class="search_form">
                            <input type="text" name="s" placeholder="Search the site..." title="Search the site..." class="field_search">
                        </form>
                    </div>
                </div--> <!-- /.header-right -->
            </div>
        </div> <!-- /.container -->

        <div class="nav-bar-main" role="navigation">
            <div class="container">
                <nav class="main-navigation clearfix visible-md visible-lg" role="navigation">
                        <ul class="main-menu sf-menu">
                            <li class="active"><a href="<?php print base_url();?>">Home</a></li>
                              <li><a href="<?php print base_url() . 'registrasi/mahasiswa';?>">Registrasi</a></li>
                            <!--li><a href="#">Events</a>
                                <ul class="sub-menu">
                                    <li><a href="events-grid.html">Events Grid</a></li>
                                    <li><a href="events-list.html">Events List</a></li>
                                    <li><a href="event-single.html">Events Details</a>
                                </ul>
                            </li>
                            <li><a href="courses.html">Courses</a>
                                <ul class="sub-menu">
                                    <li><a href="course-single.html">Course Single</a></li>
                                </ul>
                            </li>
                            <li><a href="blog.html">Blog Entries</a>
                                <ul class="sub-menu">
                                    <li><a href="blog-single.html">Blog Single</a></li>
                                    <li><a href="blog-disqus.html">Blog Disqus</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Pages</a>
                                <ul class="sub-menu">
                                    <li><a href="archives.html">Archives</a></li>
                                    <li><a href="shortcodes.html">Shortcodes</a></li>
                                    <li><a href="gallery.html">Our Gallery</a></li>
                                </ul>
                            </li>
                            <li><a href="contact.html">Contact</a></li-->
                        </ul> <!-- /.main-menu -->

                        <!--ul class="social-icons pull-right">
                            <li><a href="#" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" title="Google+"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" title="RSS"><i class="fa fa-rss"></i></a></li>
                        </ul--> <!-- /.social-icons -->
                </nav> <!-- /.main-navigation -->
            </div> <!-- /.container -->
        </div> <!-- /.nav-bar-main -->

    </header> <!-- /.site-header -->
