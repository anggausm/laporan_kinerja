<div class="widget-item">
                    <div class="request-information">
                        <?php if (!empty($this->session->flashdata('pesan'))) {?>
            <div class="alert alert-danger" role="alert">
                <?php print $this->session->flashdata('pesan')?>
            </div>
            <?php }?>
                        <h4 class="widget-title">Login SISMA</h4>
                        <form class="request-info clearfix" method="POST" action="<?php print base_url() . 'login'?>">

                            <div class="full-row">
                                <label for="yourname">Username :</label>
                                <input type="text" id="username" name="username" required="">
                            </div> <!-- /.full-row -->
                                <input type="hidden" id="token" name="token"  value="<?php print $token;?>">
                            <div class="full-row">
                                <label for="email-id">Password :</label>
                                <input type="password" id="email-id" name="password" required="">
                            </div> <!-- /.full-row -->

                            <div class="full-row">
                                <div class="submit_field">
                                    <span class="small-text pull-left"></span>
                                    <input class="mainBtn pull-right" type="submit" name="" value="Login System">
                                </div> <!-- /.submit-field -->
                            </div> <!-- /.full-row -->


                        </form> <!-- /.request-info -->
                    </div> <!-- /.request-information -->
                </div> <!-- /.widget-item -->
<div class="widget-main">
                    <div class="widget-main-title">
                        <h4 class="widget-title">Informasi</h4>
                    </div>
                    <div class="widget-inner">
                        <div class="prof-list-item clearfix">
                            <a href="https://forlap.ristekdikti.go.id/" target="_blank">
                                <div class="prof-thumb">
                                    <img src="http://portal.usm.ac.id/file/forlap.JPG" alt="Profesor Name">
                                </div> <!-- /.prof-thumb -->
                            </a>
                            <div class="prof-details">
                                <h5 class="prof-name-list">Forlap DIKTI</h5>
                                <p class="small-text">Informasi profil USM di <a href="https://forlap.ristekdikti.go.id/perguruantinggi/detail/NURGMTY2MjEtRTVDRS00RTlFLTkyMUEtMjBFRTVCNTgyODMy" target="_blank">Forlap DIKTI </a></p>
                            </div> <!-- /.prof-details -->
                        </div> <!-- /.prof-list-item -->
                        <div class="prof-list-item clearfix">
                            <a href="https://ijazah.ristekdikti.go.id/" target="_blank">
                                <div class="prof-thumb">
                                    <img src="http://portal.usm.ac.id/file/sivil.jpg" alt="">
                                </div> <!-- /.prof-thumb -->
                            </a>
                            <div class="prof-details">
                                <h5 class="prof-name-list">Sistem Verifikasi Ijasah</h5>
                                <p class="small-text">Cek Validasi ijasah anda di <a href="https://ijazah.ristekdikti.go.id/" target="_blank">  sini </a></p>
                            </div> <!-- /.prof-details -->
                        </div> <!-- /.prof-list-item -->

                    </div> <!-- /.widget-inner -->
                </div>
