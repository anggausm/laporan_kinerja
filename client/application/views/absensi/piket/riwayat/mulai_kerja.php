<div id="app" >
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>absensi/piket/riwayat">Rincian Riwayat Pekerjaan Piket</a></li>

				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa fa-list"></i> Rencana Kerja Piket
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="btn-groups" >
								<div style="float:  left;">
									<a href="#" class="text-sm btn btn-lg btn-rounded btn-stroke btn-info m-r waves-effect">
										<i class="fa  fa-clock-o fa-3x pull-left"></i>
										<span class="block clear text-left m-v-xs">Hari <?php print $wfh['result']['detail_wfh']['hari'];?>, <?php print $wfh['result']['detail_wfh']['tgl'];?>
											<br> Mulai Kerja Jam  <b class="text-lg block font-bold"><?php print $wfh['result']['detail_wfh']['jam_masuk'];?><br></b>
										</span>
									</a>
								</div>

								<div style="float: right;">
									<?php // print_r($wfh['result']);?>
									<a href="#" class="text-sm btn btn-lg btn-rounded btn-stroke btn-info m-r waves-effect">
										<i class="fa fa-smile-o fa-3x pull-left"></i>
										<span class="block clear text-left m-v-xs">Hari <?php print $wfh['result']['detail_wfh']['hari'];?>, <?php print $wfh['result']['detail_wfh']['tgl'];?>
											<br> Selesai Kerja Jam  <b class="text-lg block font-bold"><?php print $wfh['result']['detail_wfh']['jam_pulang'];?><br></b>
										</span>
									</a>
								</div>

							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered" ui-options="{  }"  data-page-length='5'  style="width:100%">
									<thead>
										<tr>
											<th  style="width:5%"  >No </th>
											<th  style="width:65%" >Rencana Pekerjaan </th>
											<th  style="width:15%" >File Laporan </th>
											<th  style="width:10%; text-align: center;" >Status</th>
										</tr>
									</thead>
									<tbody>
										<?php
$no = 1;
foreach ($wfh['result']['get_task_day'] as $value) {
    ?>
										<tr >
											<td style="text-align: center;"><?php print $no;?></td>
											<td><?php print $value['judul_kerjaan'];?><br>
												Catatan : <?php print $value['keterangan'];?></td>
											<td class="text-center">
												<?php if (!empty($value['dokumen'])) {?>
													<a href="<?php print base_url()?>absensi/piket/riwayat/download_files/<?php print $value['id_task']?>">
														<button class="btn btn-warning btn-xs"><i class="fa fa-file"></i> Dokumen Laporan</button>
													</a>
												<?php }?>

											</td>
											<td style="text-align: center;">
												<?php
if (empty($value['jam_riport'])) {
        ?>
												<button class="btn btn-warning btn-xs"><i class="fa fa-warning"></i></button>
												<?php
} else {
        ?>
												<button class="btn btn-info btn-xs"><i class="fa fa-check"></i> Jam Laporan <?php print $value['jam_riport']?></button>
												<?php
}
    ?>
											</td>

										</tr>
									</tr>
									<?php
$no++;
}
?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function startTime() {
	var today = new Date();
	var h = today.getHours();
	var m = today.getMinutes();
	var s = today.getSeconds();
	m = checkTime(m);
	s = checkTime(s);
	document.getElementById('txt').innerHTML =
	h + ":" + m + ":" + s;
	var t = setTimeout(startTime, 500);
	}
	function checkTime(i) {
	if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
	return i;
	}
</script>