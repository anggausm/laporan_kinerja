<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php echo site_url('restruktur/tendik') ?>">Re Struktur Tendik </a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-danger" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa fa-folder-open"></i>  Re Struktur Tendik
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body">
					<form method="POST" action="<?php echo site_url('restruktur/tendik/update2') ?>">
						<div class="row" id="wrap-row">
							<div class="col-md-6 wrap-clone" id="clone">
								<div class="form-group">
									<label for="nama_doskar">Nama Lengkap</label>
									<input type="text" class="form-control" id="nama_doskar" placeholder="Nama Lengkap" disabled value="<?php echo $data['nama_doskar'] ?>">
								</div>
								<div class="form-group">
									<label for="nama_doskar">Jabatan Struktural</label>
									<select class="form-control id_sub_struktural" required name="id_sub_struktural">
										<option value="">Pilih Jabatan Struktural</option>
										<?php foreach ($struktural as $value): ?>
										<option value="<?php echo $value['id_sub_struktural'] ?>" <?php echo $data['id_sub_struktural'] == $value['id_sub_struktural'] ? 'selected' : '' ?>><?php echo $value['nama_sub_struktural'] ?></option>
										<?php endforeach?>
									</select>
								</div>
								<input type="hidden" name="id_angota_struktural" value="<?php echo $data['id_angota_struktural'] ?>">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<button type="submit" class="btn btn-primary">Simpan</button>
								<a href="<?php echo site_url('restruktur/tendik') ?>" class="btn btn-default">Batal</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>