<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php echo site_url('restruktur/tendik') ?>">Re Struktur Tendik </a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-folder-open"></i>  Re Struktur Tendik
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="btn-groups">
								<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th class="text-center" width="2%">No</th>
												<th class="text-center">Nama Lengkap </th>
												<th class="text-center">Jabatan Struktural</th>
												<th class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $no = 1;?>
											<?php foreach ($data as $value): ?>
											<tr>
												<td class="text-center"><?php echo $no++ ?></td>
												<td><?php echo $value['nama_doskar'] ?></td>
												<td><?php echo $value['ket_struktural'] ?></td>
												<td>
													<a href="<?php echo site_url("restruktur/tendik/edit2/$value[id_angota_struktural]") ?>" class="btn btn-sm btn-primary">
													Re Struktur
													</a>
												</td>
											</tr>
											<?php endforeach?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>