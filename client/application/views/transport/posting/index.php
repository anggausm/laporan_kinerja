<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>ijin/tambah_ijin ">All Posting </a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php
	if (!empty($this->session->flashdata('pesan'))) {
	    ?>
		<div class="alert alert-success" role="alert">
			<?php print $this->session->flashdata('pesan')?>
		</div>
		<?php
	}
	?>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<?php
				//if ($crud['c'] == '1') 
				//{	
				/*
				?>
				<div class="col-md-12">
					<div class="panel-heading bg-white">
						<form method="POST" action="<?php print base_url() . 'transport/posting/simpan'?>">
							<button class="btn btn-info pull-left">
								<i class="fa fa-add-square-o"></i> Kirim ke Penggajian
							</button>
						</form>
					</div>
				</div>
				<?php */
				//} ?>

				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="btn-groups">
								<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center"> No </th>
												<th style="width:35%" class="text-center"> Nama </th>
												<th style="width:3%" class="text-center"> IM </th>
												<th style="width:3%" class="text-center"> IP </th>
												<th style="width:3%" class="text-center"> CT </th>
												<th style="width:3%" class="text-center"> TG </th>
												<th style="width:3%" class="text-center"> S </th>
												<th style="width:3%" class="text-center"> TM </th>
												<th style="width:3%" class="text-center"> TP </th>
												<th style="width:3%" class="text-center"> M </th>
												<th style="width:3%" class="text-center"> B </th>
												<th style="width:3%" class="text-center"> H </th>
												<th style="width:3%" class="text-center"> TA </th>
												<th style="width:3%" class="text-center"> HT </th>
												<th style="width:5%" class="text-center"> TARIF </th>
												<th style="width:5%" class="text-center"> JUMLAH </th>
											</tr>
										</thead>

										<tbody>
											<?php
											$no = 1;
											foreach ($get_default['result']['rs'] as $value) 
											{
											    // print var_dump($value);
											    ?>
												<tr>
													<td class="text-center"> <?=$no?> </td>
													<td> <?=$value['nama_doskar']?></td>
													<td class="text-center"> <?=$value['ijin_masuk']?> </td>
													<td class="text-center"> <?=$value['ijin_pulang']?> </td>
													<td class="text-center"> <?=$value['cuti']?> </td>
													<td class="text-center"> <?=$value['tugas']?> </td>
													<td class="text-center"> <?=$value['sakit']?> </td>
													<td class="text-center"> <?=$value['telat_masuk']?> </td>
													<td class="text-center"> <?=$value['telat_pulang']?> </td>
													<td class="text-center"> <?=$value['hari_khusus']?> </td>
													<td class="text-center"> <?=$value['bolos']?> </td>
													<td class="text-center"> <?=$value['hadir']?> </td>
													<td class="text-center"> <?=$value['j_absen']?> </td>
													<td class="text-center"> <?=$value['hitung_transport']?> </td>
													<td class="text-center"> <?=$value['tarif']?> </td>
													<td class="text-center"> <?=$value['j_transport']?> </td>
												</tr>
											<?php
											$no++; 
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>