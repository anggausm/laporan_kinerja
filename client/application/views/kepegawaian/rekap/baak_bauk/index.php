<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kepegawaian/rekap/laporan_baak_bauk ">Laporan Pekerjaan </a></li>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-folder-open"></i>  Rekap Laporan Pekerjaan Bag Unit dan Lembaga
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
								<div class="table-responsive">
									<table ui-jp="dataTable"  class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:65%" >Unit / Lembaga </th>
												<th style="width:15%" class="text-center">Detail</th>
											</tr>
										</thead>
										<tbody>
											<?php
$no = 1;
foreach ($fakultas['result'] as $value) {
    // print var_dump($value);
    ?>
											<!--   -->
											<tr>
												<td class="text-center">
													<?=$no?>
												</td>
												<td ><?php print $value['unit_bagian'];?></td>
												<td class="text-center">
													<a href="<?php print base_url()?>kepegawaian/rekap/laporan_baak_bauk/anggota/<?=enc_data($value['id_struktural'])?>">
														 <button class="btn btn-info btn-xs"><i class="fa fa-sitemap"></i> Anggota</button>
													</a>
												</td>
											</tr>
											<?php
$no++;
}
?>
										</tbody>
									</table>
								</div>

							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>