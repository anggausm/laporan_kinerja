<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kepegawaian/wfh/periode">Jadwal WFH </a></li>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-7">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-folder-open"></i>  Periode WFH
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table ui-jp="dataTable"  class="table table-striped b-t b-b table-bordered">
							<thead>
								<!--  id_piket_wfh, periode_wfh, keterangan, tgl_mulai, tgl_selesai, -->
								<tr>
									<th style="width:5%" class="text-center">No</th>
									<th style="width:35%" >Periode </th>
									<th style="width:15%" >Tgl Mulai </th>
									<th style="width:15%" >Tgl Selesai </th>
									<th style="width:15%" class="text-center"> </th>
								</tr>
							</thead>
							<tbody>
								<?php
$no = 1;
foreach ($piket['result'] as $value) {
    // print var_dump($value);
    ?>
								<!--   -->
								<tr>
									<td class="text-center">
										<?=$no?>
									</td>
									<td ><?php print $value['periode_wfh'];?></td>
									<td  class="text-center"><?php print $value['tgl_mulai'];?></td>
									<td  class="text-center"><?php print $value['tgl_selesai'];?></td>
									<td class="text-center">
										<a href="<?php print base_url()?>kepegawaian/wfh/periode/edit_data/<?=enc_data($value['id_piket_wfh'])?>">
											<button class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> </button>
										</a>
										<a href="<?php print base_url()?>kepegawaian/wfh/periode/hapus_data/<?=enc_data($value['id_piket_wfh'])?>"
											onclick="return confirm('Apakah anda akan menghapus data ini.');">
											<button class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> </button>
										</a>
									</td>
								</tr>
								<?php
$no++;
}
?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<?php
if ($crud['c'] == '1') {?>
			<div class="col-md-5">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-plus "></i> Tambah Periode WFH
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body">
					<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kepegawaian/wfh/periode/simpan" enctype="multipart/form-data">
						<div class="form-group ">
							<input type="hidden" name="id_task" value="<?php print $task['result']['id_task']?>">
							<label class="col-sm-4 control-label text-right">Periode WFH </label>
							<div class="col-sm-8">
								<input type="text" class="form-control"  name ="periode_wfh" value="" required=""  >
							</div>
						</div>
						<div class="form-group">
                                <label for="" class="col-sm-4 control-label">Tanggal Mulai</label>
                                <div class="col-sm-8">
                                    <input type='text' class="form-control" id='datetimepicker1' name="tgl_mulai" data-date-format="YYYY-MM-DD" placeholder="Format Tahun-Bulan-tanggal" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Tanggal Selesai</label>
                                <div class="col-sm-8">
                                    <input type='text' class="form-control" id='datetimepicker2' name="tgl_selesai" data-date-format="YYYY-MM-DD" placeholder="Format Tahun-Bulan-tanggal"/>
                                </div>
                            </div>

						<div class="form-group ">
							<label class="col-sm-4 control-label text-right">Keterangan </label>
							<div class="col-sm-8">
								<textarea name="keterangan" placeholder="Catatan" class="form-control" rows="5" ><?php print $task['result']['keterangan'];?></textarea>
							</div>
						</div>
						<hr>
						<div class="col-sm-4"></div> <div class="col-sm-4"><button class="btn btn-info btn-sm"><i class="fa fa-save"></i> Simpan Laporan </button></div>
					</form>
				</div>
			</div>
		</div>

		<?php }
?>

	</div>
</div>