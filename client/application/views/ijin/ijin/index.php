<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>biodata/tendik">Biodata DiriI</a></li>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa fa-user"></i> <b>Biodata Diri</b>
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body">
					<form  class="form-horizontal p-h-xs"   >
						<div class="row">
							<div class="col-md-6">
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">NIS </label>
									<label class="col-sm-9 control-label text-right" style="font-weight: bold;">: 
										<?php if ($rs_data['result']['rs']['nis'] == null): ?>
											<span class="text-danger"><?php echo "Anda belum memiliki NIS"; ?></span>
										<?php else: ?>
											<?=$rs_data['result']['rs']['nis'];?>
										<?php endif ?>
									</label>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Nama Lengkap </label>
									<label class="col-sm-9 control-label text-right" style="font-weight: bold;">: <?=$rs_data['result']['rs']['nama_doskar'];?> </label>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">NIDN </label>
									<label class="col-sm-9 control-label text-right" style="font-weight: bold;">: 
										<?php if ($rs_data['result']['rs']['nidn'] == null): ?>
											<span class="text-danger"><?php echo "Anda belum memiliki NIDN"; ?></span>
										<?php else: ?>
											<?=$rs_data['result']['rs']['nidn'];?>
										<?php endif ?>
									</label>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Nomor HP </label>
									<label class="col-sm-9 control-label text-right" style="font-weight: bold;">: <?=$rs_data['result']['rs']['handphone'];?> </label>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">E-mail </label>
									<label class="col-sm-9 control-label text-right" style="font-weight: bold;">: <?=$rs_data['result']['rs']['email'];?> </label>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Golongan Darah </label>
									<label class="col-sm-9 control-label text-right" style="font-weight: bold;">: <?=$rs_data['result']['rs']['gol_darah'];?> </label>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Alamat </label>
									<label class="col-sm-9 control-label text-right" style="font-weight: bold;">: <?=$rs_data['result']['rs']['alamat'];?> </label>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Kabupaten/ Kota </label>
									<label class="col-sm-9 control-label text-right" style="font-weight: bold;">: <?=$rs_data['result']['rs']['kota'];?> </label>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Kode Pos </label>
									<label class="col-sm-9 control-label text-right" style="font-weight: bold;">: <?=$rs_data['result']['rs']['kode_pos'];?> </label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Nomor KTP </label>
									<label class="col-sm-9 control-label text-right" style="font-weight: bold;">: <?=$rs_data['result']['rs']['no_ktp'];?>  </label>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Tanggal Lahir </label>
									<label class="col-sm-9 control-label text-right" style="font-weight: bold;">: <?=$rs_data['result']['rs']['tgl_lhr_format'];?>  </label>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Tempat Lahir </label>
									<label class="col-sm-9 control-label text-right" style="font-weight: bold;">: <?=$rs_data['result']['rs']['tmpt_lhr'];?>  </label>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Jenis Kelamin </label>
									<label class="col-sm-9 control-label text-right" style="font-weight: bold;">: <?=$rs_data['result']['rs']['jns_kelamin'];?>  </label>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Agama </label>
									<label class="col-sm-9 control-label text-right" style="font-weight: bold;">: <?=$rs_data['result']['rs']['agama'];?>  </label>
								</div>
							</div>
						</div>
					</form>
					<hr>
					<div class="row">
						<div class="col-md-12">
							<a href="<?php print base_url()?>biodata/tendik/edit"><button class="btn btn-info pull-right" style="margin-top: 10px; margin-bottom: 10px">Edit Biodata</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>