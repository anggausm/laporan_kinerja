$(function() {
    setNavigation();
});

function setNavigation() {
    var path = window.location.href;
    // Sidebar dengan Sub Menu
    $("nav ul.nav.nav-sub a").each(function() {
        var href = $(this).attr('href');
        if (typeof href !== typeof undefined && href !== false) {
        }else{
            href = '';
        }
        if (path.trim() == href.trim() || path.includes(href.trim())) {
            $(this).closest('li').addClass('active');
            $(this).closest('li').parents('li').addClass('active');
        } else {
            $(this).closest('li').removeClass('active');
        }
    });
}