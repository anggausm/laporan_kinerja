<?php
/**
 *  autor : Wahyu Budi Santosa
 *  email : wahyubudisantosa@gmail.com
 *  @dudoks
 */
defined('BASEPATH') or exit('No direct script access allowed');

class CI_Tema
{

    public function __construct($config = array())
    {
        $this->ddk = &get_instance();

    }
    public function index($value = '')
    {
        # code...
    }
    public function set($name, $value)
    {
        $this->template_data[$name] = $value;
    }

    /*
     * FROND TEMPLATE
     */

    public function frond($view = '', $view_data = array(), $return = false)
    {
        $menu_data = '';
        $this->set('header', $this->ddk->load->view('master/frondend/header', $menu_data, true));
        $this->set('sidebar', $this->ddk->load->view('master/frondend/sidebar', $view_data, true));
        $this->set('footer', $this->ddk->load->view('master/frondend/footer', '', true));
        $this->set('konten', $this->ddk->load->view($view, $view_data, true));
        return $this->ddk->load->view('master/frondend/index', $this->template_data, $return);
    }
    public function app($view = '', $view_data = array(), $return = false)
    {
        $data['server_file'] = $this->ddk->app->server_file();
        $data['user'] = $this->data_profil();
        $this->set('header', $this->ddk->load->view('master/backend/app/header', '', true));
        $this->set('footer', $this->ddk->load->view('master/backend/app/footer', '', true));
        $this->set('profil', $this->ddk->load->view('master/backend/app/profil', $data, true));
        $this->set('konten', $this->ddk->load->view($view, $view_data, true));
        return $this->ddk->load->view('master/backend/app/index', $this->template_data, $return);
    }

    public function backend($view = '', $view_data = array(), $return = false)
    {
        error_reporting('ALL');
        $data['nm_aplikasi'] = $this->ddk->session->userdata('nm_aplikasi');
        $data['nm_level'] = $this->ddk->session->userdata('nm_level');
        $data['menu'] = $this->ddk->session->userdata('menu');
        $data['user'] = $this->data_profil();
        $data['server_file'] = $this->ddk->app->server_file();
        $data['base_url'] = $this->ddk->app->sso_app();
        $this->set('header', $this->ddk->load->view('master/backend/page/header', $data, true));
        $this->set('sidebar', $this->ddk->load->view('master/backend/page/sidebar', $data, true));
        $this->set('konten', $this->ddk->load->view($view, $view_data, true));
        return $this->ddk->load->view('master/backend/page/index', $this->template_data, $return);
    }
    public function data_profil()
    {
        $base_url = $this->ddk->app->sso_app();
        return json_decode($this->ddk->curl->request('PUT', $base_url . 'api/profil', 'sso_token', ''), 'TRUE');
    }

}
